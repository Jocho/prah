<?php
/**
 * This is an essential file for _blank framework. It contains all the setup and decisions
 * that are needed for proper running of the framework. As far as you know what are you
 * doing, do whatever changes that you need, but for most of the time it won't be
 * neccessary. Have fun! :)
 *
 * @author Marek J. Kolcun
 * @since 2014-05-11
 * @version 1.3.1
 */
namespace Blank;
// session_start();
// session_destroy();
class Blank {
	public $database;
	public $post;
	public $session;
	public $cookie;
	public $router;
	public $models;
	public $settings;
	public $productionMode;

	public function __construct() {
		// path constants to basic directories
		define('ROOT', str_replace('\\', '/', dirname(dirname(__FILE__))) . '/');
		define('CONTROLLERS', ROOT . 'controllers/');
		define('MODELS', ROOT . 'models/');
		define('VIEWS', ROOT . 'views/');
		define('FRAMEWORK', ROOT . '_blank/');
		define('RESOURCES', ROOT . 'res/');
		
		// initializing objects used per whole framework
		$this->headers  = apache_request_headers();
		
		// asking whether this is an ajax request
		$this->isAjaxRequest = (isset($this->headers['X-Requested-With']) || isset($this->headers['is-ajax-request'])) ? TRUE : FALSE;
		$this->requestMethod = $_SERVER['REQUEST_METHOD'];

		if ($this->requestMethod == 'PUT') {
			parse_str(file_get_contents('php://input'), $_POST);
		}

		$this->router  = new Router();
		$this->session = new Session();
		$this->post    = new Post();
		$this->files   = new Files();
		$this->post->setFiles($this->files);
		$this->settings = new BlankObject();
		$this->models   = new BlankObject();
		$this->database = NULL;
		
		// initializing settings from file settings-local.json or settings-production.json
		$this->loadSettings();
		
		// initializing cookies
		$cookieDuration = ($this->settings->has('cookieDuration')) ? $this->settings->get('cookieDuration') : 86400;
		$this->cookie = new Cookie($cookieDuration);
		
		// strings.php is an essential library for this framework. Without this library framework cannot work properly.
		try {
			if (is_file(FRAMEWORK . 'strings.php')) {
				require_once(FRAMEWORK . 'strings.php');
				$this->strings = new Strings;
			} else
				throw new \Exception;
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			echo 'File strings.php does not exist.';
			die();
		}
		
		// if there is setup for database in settings, a database class is loaded.
		if ($this->settings->has('database')) {
			try {
				if (is_file(FRAMEWORK . 'database.php')) {
					require_once(FRAMEWORK . 'database.php');
					$this->database = new Database($this->settings->database);
				} else
					throw new \Exception;
			}
			catch (\Exception $e) {
				echo $e->getMessage();
				echo 'File database.php does not exist.';
				die();
			}
		}
		
		// load translation class
		try {
			if (is_file(FRAMEWORK . 'translation.php')) {
				require_once(FRAMEWORK . 'translation.php');
				$translationSelect = ($this->settings->translation->get('translationSelect') != 'auto')
					? $this->settings->translation->get('translationSelect')
					: NULL;
				$this->tran = new Translation($translationSelect, $this->settings->translation->useTranslation);
			} else
				throw new \Exception;
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			echo 'File translation.php does not exist.';
			die();
		}
		
		// let autoload 
		$this->autoload();
		
		// all model classes are loaded only if a blank->database object exists.
		
		if (is_object($this->database)) {
			$models   = $this->dirInspector(MODELS);
			$pathList = $this->pather($models);

			foreach ($pathList as $modelPath) {
				$this->additionalClassAutoload($modelPath, MODELS);
				include_once(MODELS . $modelPath);
				$pathinfo          = pathinfo($modelPath);
				$modelCallableName = $pathinfo['dirname'] . '/' . $pathinfo['filename'];
				$modelCallableName = $this->strings->changeSlashes($modelCallableName, '\\');
				$modelCallableName = $this->strings->dkr($modelCallableName, 'camelcase');
				$modelName         = str_replace('\\', '_', $modelCallableName);
				$modelName         = $this->strings->dkr($modelName, 'camelcase');
				$this->models->set($modelName, new $modelCallableName($this->settings->database));
			}
		}
		
		// a page instance is started and if there will be no error or redirection, new session data will be stored to $_SESSION array.
		if ($this->run()) {
			$this->session->save();
			
			try {
				if ($this->post->hasAny() && !$this->post->has('PROCESSED'))
					throw new \Exception;
			}
			catch (\Exception $e) {
				echo 'Post was not properly processed. Check relevant HTML template, controller and gear methods.';
				die();
			}
		}
		die();
	}
	
	/**
	 * Defines autoload function for autoloading classes extended in controllers/ folder.
	 * All classes must be situated in controllers/ site
	 */
	private function autoload() {
		spl_autoload_register(function($class) {
			$class = $this->strings->dkr($this->strings->changeSlashes($class, '/'), 'underscore');
			if (is_file(CONTROLLERS . $class . '.php'))
				include(CONTROLLERS . $class . '.php');
			else
				return FALSE;
		});
		// var_dump(spl_autoload_functions());
	}
	
	/**
	 * Method is used for loading the Settings file. File contains a JSON type object with various settings.
	 * Those settings are transformed into BlankObject object and they are available in Gear objects through 
	 * "$this->settings" notation. 
	 * For developing there is an option to use settings-local.json and settings-production.json file. 
	 * For using the settings-local.json file just create a file named "whitelist.txt" with a list of IP
	 * addressses, for which this settings file will be used. 
	 */
	private function loadSettings() {
		$remoteAddr    = $_SERVER["REMOTE_ADDR"];
		$addrWhiteList = (file_exists(FRAMEWORK . 'whitelist.txt')) ? file(FRAMEWORK . 'whitelist.txt') : array();
		
		try {
			if (count($addrWhiteList) && in_array($remoteAddr, $addrWhiteList) && file_exists(FRAMEWORK . '/settings-local.json')) {
				$settingsFilePath     = FRAMEWORK . 'settings-local.json';
				$this->productionMode = FALSE;
			}
			elseif (file_exists(FRAMEWORK . 'settings-production.json')) {
				$settingsFilePath     = FRAMEWORK . 'settings-production.json';
				$this->productionMode = TRUE;
			}
			else
				throw new \Exception;
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			die('Settings file does not exist.');
		}
		$settings = json_decode(implode("\n", file($settingsFilePath)), TRUE);
		try {
			if (is_array($settings)) {
				$this->settings->fromArray($settings);
				return TRUE;
			} else
				throw new \Exception;
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			echo 'Settings were not properly parsed. Is a JSON file "' . $settingsFilePath . '" correct?';
			die();
		}
	}
	
	/**
	 * This method returns array-based directory tree of given root directory
	 *
	 * @param $dir string - path to the root directory. Must exist!
	 * @return array - formatted directory tree
	 */
	private function dirInspector($dir) {
		$content = array_flip(scandir($dir));
		foreach ($content as $key => $c) {
			if (in_array($key, array(
				'.',
				'..'
			))) {
				unset($content[$key]);
			} elseif (is_dir(realpath($dir . $key))) {
				$content[$key] = $this->dirInspector($dir . $key . '/');
			} else {
				$content[$key] = NULL;
			}
		}
		return $content;
	}
	
	/**
	 * Returns a list of paths that are possible to build from array returned by "dirInspector" method.
	 *
	 * @param $list array - array returned by dirInspector.
	 * @param $initialDir string - path that will be added to the file that is going to be written.
	 * @param $pathList array - resulting list of paths.
	 * @return $pathList.
	 */
	private function pather($list, $initialDir = '', &$pathList = array()) {
		foreach ($list as $item => $type) {
			if (is_array($type)) {
				$this->pather($type, $initialDir . $item . '/', $pathList);
			} else
				$pathList[] = $initialDir . $item;
		}
		return $pathList;
	}
	
	/**
	 * Automatically tries to find declaration of class in soon-to-be included file and pre-includes every class that is extending this class.
	 *
	 * @param $entryName string - camel-cased name of class whether with or without backslashes. If this class is extended by any other class
	 * 							  and that class is not declared yet, a file with this class is being included. 
	 * @return BOOLEAN 			- if all classes were loaded successfully, TRUE will be returned, else Exception will be thrown and FALSE returned. 
	 */
	private function additionalClassAutoload($entryName, $path = NULL) {
		$path                 = ($path == NULL) ? CONTROLLERS : $path;
		$noBackslashEntryName = (strpos($entryName, '\\') === 0) ? substr($entryName, 1) : $entryName;
		if (in_array($noBackslashEntryName, get_declared_classes()))
			return TRUE;
		
		$filename = $this->strings->changeSlashes($this->strings->dkr($entryName, 'underscore'), '/');
		if (strpos($filename, '.php') === FALSE)
			$filename .= '.php';
		
		if (strpos($filename, '/') === 0)
			$filename = substr($filename, 1, strlen($filename));
		try {
			if (file_exists($path . $filename)) {
				$file        = fopen($path . $filename, 'r');
				$fileContent = fread($file, filesize($path . $filename));
				fclose($file);
				
				preg_match('/namespace (.+);/', $fileContent, $namespaceMatch);
				preg_match_all('/class [\w\S]+ extends ([\w\S]+)/', $fileContent, $matches);
				if (count($matches[1])) {
					foreach ($matches[1] as $match) {
						$exploded = explode('\\', $match);
						if (count($exploded) == 1 && count($namespaceMatch) == 2)
							$match = $namespaceMatch[1] . '\\' . $match;
						$this->additionalClassAutoload($match, $path);
					}
				}
				require_once($path . $filename);
			} else
				throw new \Exception;
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			// var_dump ($path);
			echo 'File ' . $path . $filename . ' does not exist.';
			die();
			return FALSE;
		}
	}
	
	/**
	 * Loads the Gear object based by given name. Name must containt full name of object, namespace included.
	 * Namespaces are neccessary for proper loading the gear file and initializing it's instance.
	 *
	 * @param $entryName string - full name of loaded gear object.
	 * @param $generation boolean - decides wether a Gear is loaded automatically by Blank itself or by 
	 * 							 developer as inline gear instance in main gear object.
	 * @return mixed - returns instance of new Gear object if no errors occured, otherwise returns FALSE.
	 */
	public function loadGear($entryName, $generation = 0) {
		$filename = $this->strings->changeSlashes($this->strings->dkr($entryName, 'underscore'), '/');
		$filename .= '.php';
		if (strpos($filename, '/') == 0)
			$filename = substr($filename, 1, strlen($filename));
		try {
			if (file_exists(CONTROLLERS . $filename))
				require_once(CONTROLLERS . $filename);
			else
				throw new \Exception;
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			echo 'File ' . $filename . ' does not exist.';
			return FALSE;
		}
		
		try {
			$controllerName = preg_replace('/\\\+/', '\\', $entryName);
			if (class_exists($controllerName)) {
				$controller = new $controllerName($this, $generation);
				return $controller;
			} else
				throw new \Exception;
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			echo 'Controller class not found (' . $controllerName . ').';
			return FALSE;
		}
	}
	
	
	/**
	 * Tries to find out, which Gear object is being created.
	 * For proper deciding there is need to create routes in settings file. 
	 * Routes should cover all the possible addresses a guest can visit in the web application.
	 * There are 5 prefixes used for better routing:
	 * - {home} - prefix is used for route when no path in URL is found. A.k.a. landing page.
	 * - {number} - prefix used when first part of path starts with a number. I.e.: test.com/4/hello
	 * - {render} - string found in the position of this prefix will be used as render method prefix.
	 * - {any} - this prefix mathes any part of the path.
	 * - [###] - when a string is set instead of "###", a route->query[###] variable will be set with
	 * 			 a value of matched URL part.
	 * 
	 * @return boolean - TRUE if a Gear object was successfully created, FALSE otherwise.
	 */
	public function run() {
		try {
			$path               = $this->router->path;
			$entryName          = '';
			$this->renderMethod = 'render';
			$routeList          = array_keys($this->settings->routes->toArray());
			$routeListValues    = array();
			
			$shadowGets       = array();
			$shadowGetsOffset = array();
			$shadowRenders    = array();
			
			$path             = array_reverse($path, TRUE);
			$pathSize         = count($path);

			foreach ($routeList as $key => $route) {
				$routeListValues[$key] = 0;
				$xedRoute = explode("/", $route);
				if ($pathSize && count($xedRoute) > $pathSize) 
					continue;
				$xedRoute = array_reverse($xedRoute, TRUE);

				foreach ($xedRoute as $level => $routePart) {
					if (!count($path) && $routePart == '{home}')
						$routeListValues[$key] += pow(10, $level);

					elseif (count($path)) {
						if (is_numeric($path[$level]) && $routePart == '{number}')
							$routeListValues[$key] += pow(10, $level);

						elseif (preg_match('/\[(\S+)\]/', $routePart, $match)) {
							$shadowGets[$key][$match[1]] = $path[$level];
							$shadowGetsOffset[$key][]    = $level;
							$routeListValues[$key] += pow(10, $level);
						}

						elseif ($routePart == '{render}') {
							$shadowRenders[$key] = $path[$level];
							$routeListValues[$key] += pow(10, $level);
						}

						elseif (strlen($path[$level]) && $routePart == '{any}')
							$routeListValues[$key] += pow(10, $level);

						if ($path[$level] == $routePart)
							$routeListValues[$key] += pow(10, $level) * 1.5;
					}
				}
			}
			arsort($routeListValues);

			$keys = array_keys($routeListValues);
			$firstKey = reset($keys);

			if (isset($shadowGets[$firstKey]) && count($shadowGets[$firstKey])) {
				$query = (is_array($this->router->query)) ? $this->router->query : array();
				$this->router->query = array_merge($query, $shadowGets[$firstKey]);
				// foreach ($shadowGetsOffset[$firstKey] as $level)
				// 	unset($this->router->path[$level]);
			}
			
			if (isset($shadowRenders[$firstKey]))
				$this->renderMethod = $shadowRenders[$firstKey];

			$this->renderMethod = $this->strings->dkr($this->renderMethod, 'camelcase');
			
			$entryKey  = $routeList[$firstKey];
			$entryName = $this->settings->routes->$entryKey;
			
			// var_dump($this->router);
			// var_dump($shadowGets);
			// var_dump($shadowGetsOffset);
			// var_dump($path);
			// var_dump($routeList);
			// var_dump($routeListValues);
			// var_dump($shadowRenders);
			// var_dump($firstKey);
			// var_dump($this->renderMethod);
			// die();

			$this->additionalClassAutoload($entryName);
			
			$this->loadGear($entryName);
			return TRUE;
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			return FALSE;
		}
	}
	
	public function log($calledClass) {
		$file = fopen('log.txt', 'a');
		fwrite($file, microtime(TRUE));
		fwrite($file, "\r\n");
		fwrite($file, "called class: " . $calledClass);
		fwrite($file, "\r\n");
		
		fwrite($file, json_encode($this->session));
		fwrite($file, "\r\n");
		fwrite($file, json_encode($this->post));
		fwrite($file, "\r\n");
		
		// fwrite($file, json_encode($_SESSION));
		// fwrite($file, "\r\n");
		// fwrite($file, json_encode($_POST));
		// fwrite($file, "\r\n");
		
		fwrite($file, '- - - - - - - - - -');
		fwrite($file, "\r\n");
		fclose($file);
	}
}

/**
 * A standard _blank framework object. 
 * Is quite simmilar to a standard PHP object (StdObject), but contains some additional methods.
 *
 * @author Marek J. Kolcun
 * @since 2014-05-11
 * @version 1.0.1
 */
class BlankObject {
	/**
	 * Creates a basic _blank framework object. When an non-empty array is used as initial parameter,
	 * a newly created object will be filled by values of that array by rules stated in "fromArray()" method.
	 *
	 * @param $data array - An array containing an initial data of the newly created BlankObject.
	 */
	public function __construct($data = array()) {
		if (count($data)) {
			$this->fromArray($data);
		}
	}
	/**
	 * Returns a value of given variable If a string is returned, a "stripslasshes()" function is applied.
	 *
	 * @param $variable string - name of variable whose value is going to be returned.
	 * @return mixed - value of demanded variable or NULL if this variable does not exist.
	 */
	public function get($variable) {
		$value = NULL;
		if (isset($this->$variable))
			$value = (is_string($this->$variable)) ? stripslashes($this->$variable) : $this->$variable;
		return $value;
	}
	
	/**
	 * Creates a new variable in object with a given value or resets a value of existing object variable.
	 * If a string variable is created an "addslashes()" function is applied.
	 *
	 * @param $variable string - name of variable
	 * @param $value mixed - value of variable
	 * @return boolean - TRUE if new variable was set, FALSE otherwise.
	 */
	public function set($variable, $value) {
		$value          = (is_string($value)) ? addslashes($value) : $value;
		$varname        = (is_numeric($variable)) ? '_' . $variable : $variable;
		$this->$varname = $value;
		return isset($this->$varname);
	}
	
	/**
	 * Deletes a variable with a certain name.
	 * 
	 * @param $variable string - name of soon to be deleted variable.
	 * @return boolean - TRUE if a variable was successfully deleted, FALSE otherwise.
	 */
	public function del($variable = NULL) {
		if ($variable === NULL) {
			foreach (get_object_vars($this) as $key => $value)
				$this->del($key);
			return TRUE;
		} elseif (isset($this->$variable)) {
			unset($this->$variable);
			return TRUE;
		}
		return FALSE;
	}
	
	/**
	 * Asks whether a variable with a given name exists in object.
	 *
	 * @param $variable string - name of asked variable.
	 * @return boolean - TRUE if searched variable exists, FALSE otherwise.
	 */
	public function has($variable) {
		$variables = get_object_vars($this);
		return array_key_exists($variable, $variables);
	}
	
	/**
	 * Asks if an object contains at least one variable. 
	 *
	 * @return boolean - TRUE if object contains at least one variable, FALSE otherwise.
	 */
	public function hasAny() {
		return (count(get_object_vars($this))) ? TRUE : FALSE;
	}
	
	/**
	 * Returns all the object's variables as an associative array.
	 *
	 * @return array - array of object's variables.
	 */
	public function toArray() {
		return get_object_vars($this);
	}
	
	public function toArrayRecursive($source = NULL) {
		$source = ($source === NULL) ? get_object_vars($this) : $source;
		$data   = array();
		
		foreach ($source as $key => $value) {
			$k = (preg_match('/_\d+/', $key)) ? substr($key, 1) : $key;
			if (is_object($value))
				$data[$k] = $this->toArrayRecursive(get_object_vars($value));
			elseif (is_array($value))
				$data[$k] = $this->toArrayRecursive($value);
			else
				$data[$k] = $value;
		}
		return $data;
	}
	
	/**
	 * sets values to multiple variables of the Object object
	 * @param array (associative)
	 * @return boolean
	 */
	public function fromArray($array = array()) {
		foreach ($array as $key => $value) {
			if (!is_array($value) && !is_object($value)) {
				// $this->set($key, $value);
				$this->$key = $value;
			} else {
				$data = (is_array($value)) ? $value : get_object_vars($value);
				$this->set($key, new BlankObject());
				$k = ($this->has($key)) ? $key : '_' . $key;
				$this->$k->fromArray($data);
			}
		}
		$array = (is_object($array)) ? get_object_vars($array) : $array;
		return (in_array(array_keys($array), array_keys(get_object_vars($this)))) ? TRUE : FALSE;
	}
	
	public function log($data) {
		$file = fopen('log.txt', 'a');
		fwrite($file, json_encode($data));
		fwrite($file, "\r\n");
		fclose($file);
	}
}


/**
 * Main object or controller (name it as you want), that generates and stores all the web content.
 * One Gear object is loaded automatically and is represented as Controller that solves all the
 * queries and tasks on a given URL page. 
 * Gear objec can contain various inline gear objects that can solve their own tasks per page.
 * 
 * @author Marek J. Kolcun
 * @since 2014-05-11
 * @version 1.0.1
 */
class Gear {
	private $generation;
	protected $router;
	protected $session;
	protected $post;
	protected $models;
	protected $strings;
	protected $database;
	protected $settings;
	protected $template;
	protected $blank;
	protected $gears;
	protected $tran;
	
	private $methodName;
	
	/**
	 * Sets up a gear object. Also automatically runs some of its methods in a precisous order:
	 * 1. init() - used for initialization of variables and other inline gear objects.
	 * 2. processPost() - if there was a post request, a related post-solving method is being found.
	 * 3. processHandler() - if there is a "do" variable in URL query, a related "get-solving" method is being found.
	 * 4. process() - a mid-time solving method used for solving any request that need to be solved after initialization
	 * 				  but before any render() method.
	 * 5. processRender() - if object contains a native render() method or #prefix#Render() method, it will be run.
	 */
	public function __construct($blank, $generation) {
		$this->generation = $generation;
		$this->blank      = $blank;
		$this->gears      = new BlankObject;
		$this->template   = new BlankObject;
		
		$this->router     = $blank->router;
		$this->session    = $blank->session;
		$this->post       = $blank->post;
		$this->files      = $blank->files;
		$this->models     = $blank->models;
		$this->strings    = $blank->strings;
		$this->database   = $blank->database;
		$this->settings   = $blank->settings;
		$this->methodName = '';

		$this->tran       = $blank->tran;
		
		$myClass              = get_class($this);
		$myClass              = $this->strings->dkr($myClass, 'underscore');
		$exploded             = explode("\\", $myClass);
		$this->template->name = array_pop($exploded);
		$this->template->path = VIEWS . implode('/', $exploded) . '/';
		
		$this->template->root = ($this->generation) ? 'gear_template' : 'frontend_template';
		$this->template->body = 'BODY';
		
		$this->init();
		
		if ($this->post->hasAny())
			$this->processPost();
		if (isset($blank->router->query['do']) && $blank->router->query['do'])
			$this->processHandler();
		$this->process();
		if (!$this->isChild())
			echo $this->processRender();
	}
	
	/**
	 * A pack of predefined methods that need to be present in every Gear object
	 */
	protected function init() {
	}
	protected function process() {
	}
	// protected function render() {}
	
	/**
	 * Tries to find and run an acceptable #prefix#Form() method. 
	 * To run the proper method there is need to correctly setup the "action" parameter in form
	 * and setup a "submit" type input element with exact name as related method.
	 */
	protected function processPost() {
		$processedPosts = array();
		$formMethods    = get_class_methods($this);
		foreach ($formMethods as $key => $method) {
			if (strpos($method, 'Form') === FALSE)
				unset($formMethods[$key]);
		}
		
		foreach ($this->post->toArray() as $item => $value) {
			$methodName = $item . 'Form';
			if (in_array($methodName, $formMethods)) {
				$this->methodName = $methodName;
				$this->$methodName($this->post);
				$this->post->set('PROCESSED', TRUE);
			}
		}
	}
	
	/**
	 * If there is a "do" variable in URL query a method with #prefix#Handler() name being found.
	 * #Prefix# stands for value of "do" variable in URL query. 
	 * If this method does not exist, no error value is returned. 
	 */
	protected function processHandler() {
		$methodName = trim($this->router->query['do']) . 'Handler';
		if (method_exists($this, $methodName)) {
			$this->methodName = $methodName;
			$this->$methodName($this->router);
		}
	}
	
	/**
	 * If no other #prefix#Render() is decided to be called (check "run()" method of Blank object),
	 * a classic render() method is being run.
	 */
	protected function processRender() {
		try {
			$methodName = (!$this->generation) ? $this->blank->renderMethod : 'render';
			if ($methodName != 'render')
				$methodName .= 'Render';
			$this->methodName = $methodName;
			
			if (method_exists($this, $methodName)) {
				if ($methodName != 'render')
					$this->template->name = $this->strings->dkr($this->blank->renderMethod, 'underscore');
				$this->$methodName();
				return $this->view();
			} elseif (!$this->isChild()) {
				throw new \Exception;
			}
		}
		catch (\Exception $e) {

			if ($this->blank->productionMode) {
				$this->redirect('!e404');
			}
			else {
				echo $e->getMessage();
				echo 'Gear ' . get_class($this) . ' does not have a render method ' . $methodName . '.';
				die();
			}
		}
	}
	
	/**
	 * A returns a result if a Gear controller is child or is it a root.
	 *
	 * @return boolean - TRUE if an asked Gear object is child any Gear, FALSE otherwise.
	 */
	public function isChild() {
		return ($this->generation) ? TRUE : FALSE;
	}
	
	/**
	 * A getter of $generation private variable.
	 *
	 * @return boolean - TRUE if an asked Gear object is child of automatically called Gear, FALSE otherwise.
	 */
	public function getGeneration() {
		return $this->generation;
	}
	
	
	/**
	 * Last method that Gear runs creates instances of its $template variables.
	 * Every variable is thrown in output buffer, then a correct HTML template is loaded and solved.
	 * Buffer is then sent to the $render variable. That variable is then echoed in template the
	 * non-child Gear object is extending (using for rendering its content).
	 *
	 * @return boolean - TRUE if view runs correctly, an Exception is thrown otherwise.
	 */
	protected function view() {
		$templateName = $this->template->name;
		try {
			$gearTemplate = $this->template->path . $templateName . '.html';
			
			if (is_file($gearTemplate)) {

				if ($this->blank->isAjaxRequest) {
					header('Content-Type', 'application/json');
					$this->disposeTemplateData();

					if ($this->gears->hasAny()) {
						$this->template->_gears = new BlankObject;
						foreach ($this->gears as $name => $gear) {
							$gear->disposeTemplateData();
							$this->template->_gears->$name = $gear->template;
						}
					}

					echo json_encode($this->template, JSON_UNESCAPED_UNICODE);
					die();
				}

				ob_start();
				$gears = array();

				foreach ($this->gears->toArray() as $gearName => $gear) {
					$gearName = strtoupper($this->strings->dkr($gearName, 'underscore'));
					if ($gear->getGeneration() == 1)
						$gears[$gearName] = $gear->processRender();
				}
				extract($gears);
				
				if (!$this->isChild() && !$this->template->has('metadata'))
					$this->template->set('metadata', $this->settings->metadata);
				
				extract($this->template->toArray());
				
				include($gearTemplate);

				$this->template->body = strtoupper($this->strings->dkr($this->template->body, 'underscore'));
				${$this->template->body} = ob_get_clean();
				ob_start();
				include(VIEWS . $this->template->root . '.html');
				return ob_get_clean();
			} else {
				throw new \Exception;
			}
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			echo 'Template ' . $gearTemplate . ' does not exist.';
			die();
		}
	}

	private function disposeTemplateData() {
		unset($this->template->body, $this->template->root, $this->template->path, $this->template->name);
	}
	
	/**
	 * Redirects current URL location to another
	 *
	 * @param $destination string    - a relative or absolute URL path (with query) to which a guest will be redirected.
	 * 								 - if path is set by string prepended by "!" character, a custom redirect
	 * 								   will be proceeded. For more info see a "getRedirection()" and "redirectionExists()" documentation.
	 * @param $resetFragment boolean - if set to TRUE, a fragment from URL will be replaced by pure "#" character.
	 */
	public function redirect($destination = '', $resetFragment = FALSE) {
		if (strlen($destination) && substr_compare($destination, '!', 0, 1) === 0 && $this->redirectionExists(substr($destination, 1)))
			$destination = $this->getRedirection(substr($destination, 1));
		
		array_unshift($this->router->path, "");
		$implodedPath = implode("/", $this->router->path);
		if (!strlen($implodedPath))
			$implodedPath = '/';

		try {
			if ($destination != $implodedPath) {
				$this->session->save();
				$destination = ($destination == '') ? $implodedPath : $destination;
				if ($resetFragment)
					$destination .= '#';
				// $this->blank->log('--> ' . $destination);
				header('Location: ' . $destination);
				die();
			}
			else
				throw new \Exception($destination);
		}
		catch (\Exception $e) {
			echo 'Cyclic redirection to "' . $e->getMessage() . '".';
			die();
		}
	}
	
	/**
	 * Gets a custom redirection string from "settings" file. 
	 * A "redirect()" method can perform a custom redirection based on string defined in "settings" file.
	 * In "settings" file there is need to create full path tree (class, method, redirection name) to
	 * proper functioning. 
	 * Custom redirection is set by exclamation mark defined before redirection name.
	 *
	 * Example of usage in gear method: "$this->redirect('!settingsConfirmed');"
	 * 
	 * @param $redirectionName string - redirection string name set in "settings" file.
	 * @return mixed string | NULL    - a string redirection is sent when redirection name exists, NULL sent instead.
	 */
	private function getRedirection($redirectionName = '') {
		$myClass = '\\' . get_class($this);
		if ($this->settings->has('redirections')) {
			foreach (array($myClass, 'GLOBAL') as $section) {
				var_dump ($this->settings->redirections->$section->$redirectionName);
				if ($this->settings->redirections->has($section) && $this->settings->redirections->$section->has($redirectionName)
				) {
					return $this->settings->redirections->$section->$redirectionName;
				}
			}
		}
		return NULL;
	}
	
	/**
	 * Returns a boolean value after check if a custom redirection exists.
	 *
	 * @param $redirectionName string - a custom redirection name to be checked.
	 * @return boolean                - TRUE if a custom redirection string exists, FALSE otherwise.
	 */
	private function redirectionExists($redirectionName = '') {
		return !is_null($this->getRedirection($redirectionName));
	}
	
	/**
	 * Sets current object's template variables, root and body. 
	 * - Root variable decides which global template will be used. 
	 * - Body variable decides which variable will contain default Gear's view() return. 
	 * 
	 * 
	 */
	public function useTemplate($template, $variable = 'BODY') {
		$this->template->root = $template;
		$this->template->body = $variable;
	}
	
	/**
	 * Method creates a \Blank\Gear -like object that belongs to main controller's template variable list.
	 * This micro-controller will be iniciated in "view" method.
	 *
	 * @param $gear string - name of the class the gear will be instance of. Name can vary: if full class name is given (namespace + class), the corresponding class is being used. 
	 *						 Otherwise, if given name of the class only, the namespace of current controller will be used. This form is good for recognizing whether the Gear is part
	 *						 of main controller's namespace group or if it is some external gear.
	 * @return BOOLEAN - TRUE if gear was created successfully, FALSE otherwise
	 */
	public function gear($gearName, $alias = '') {
		$gear         = $this->blank->loadGear($gearName, $this->generation + 1);
		$gearBasename = lcfirst(basename($gearName));
		
		try {
			if (is_object($gear)) {
				if (strlen($alias))
					$this->gears->set($alias, $gear);
				else
					$this->gears->set($gearBasename, $gear);
			} else
				throw new \Exception($gearName);
		}
		catch (\Exception $e) {
			echo 'No gear ' . $gearName;
			die();
		}
	}
}


/**
 * Object used for using parsed URL. 
 * Path of the URL is exploded into array. 
 * Query string is parsed into associative array where variable is key and value is, well... key's value.
 * 
 * @author Marek J. Kolcun
 * @since 2014-05-11
 * @version 1.0.1
 */
class Router {
	public $path;
	public $query;
	
	public function __construct() {
		$parsed     = parse_url($_SERVER["REQUEST_URI"]);
		$this->path = (isset($parsed['path'])) ? explode("/", $parsed['path']) : array();
		if (end($this->path) == '')
			array_pop($this->path);
		if (count($this->path))
			array_shift($this->path);
		$query = (isset($parsed['query'])) ? explode("&", $parsed['query']) : array();
		foreach ($query as $q) {
			$parsedQueryPart                  = explode("=", $q);
			$this->query[$parsedQueryPart[0]] = (count($parsedQueryPart) == 2) ? $parsedQueryPart[1] : NULL;
		}
	}
	
	/**
	 * Builds up and returns an usable URL address from available data stored in Router. Can be built in various forms, from relative path to absolute address.
	 * 
	 * @param $buildQuery bool - adds full query request behind built url path when set to TRUE, otherwise address will be built only by path. 
	 * @param $fullAddress bool - sets whether should be used full address or relative path only. 
	 *
	 * @return string - Returns built URL address. Full or relative, with/out query string after path.
	 */
	public function buildUrl($buildQuery = FALSE, $fullAddress = FALSE) {
		$builtUrl = '/' . implode("/", $this->path);
		
		if ($buildQuery && count($this->query)) {
			$queryData = array();
			foreach ($this->query as $key => $value) {
				$queryData[] = $key . '=' . $value;
			}
			$builtUrl .= '?' . implode("&", $queryData);
		}
		
		if ($fullAddress)
			$builtUrl = 'http://' . $_SERVER["SERVER_NAME"] . $builtUrl;
		return $builtUrl;
	}
	
}

/**
 * Class extending basic BlankObject. Sets up it's variables with a $_POST global variable.
 */
class Post extends \Blank\BlankObject {
	public function __construct() {
		// $postData = (count($_POST)) ? $_POST : json_decode(file_get_contents('php://input'), TRUE);
		$postData = $_POST;
		$postData = (is_null($postData)) ? array() : $postData;
		return $this->fromArray($postData);
	}
	
	public function set($variable, $value) {
		$result = parent::set($variable, $value);
		$this->save();
		return $result;
	}
	
	/**
	 * Saves its variables into $_SESSION global variable.
	 */
	public function save() {
		$_POST = get_object_vars($this);
	}
	
	/**
	 * If there was set a global variable $_FILES, an instance of BlankObject Files will be initiated into Post object for easier access.
	 *
	 * @param $files BlankObject - An instance of BlankObject holding all data from global $_FILES variable.
	 */
	public function setFiles($files) {
		if ($files->hasAny()) {
			$this->set('files', new BlankObject);
			foreach ($files->toArray() as $name => $data);
			$this->set($name, $data);
		}
	}
}

/**
 * Class for initializing Files data. The basic form of $_FILES variable is quite unpleasant, so this object holds all the files in their individual packages. 
 * Every file data is stored in its own array key-based array in case of multiple files, or as an object alone in case of one single uploaded file. 
 */
class Files extends \Blank\BlankObject {
	public function __construct() {
		foreach ($_FILES as $packName => $packData) {
			if (is_array($packData['name'])) {
				$c = count($packData['name']);
				for ($i = 0; $i < $c; $i++) {
					$obj = new BlankObject;
					$obj->set('name', $_FILES[$packName]['name'][$i]);
					$obj->set('tmp_name', $_FILES[$packName]['tmp_name'][$i]);
					$obj->set('type', $_FILES[$packName]['type'][$i]);
					$obj->set('size', $_FILES[$packName]['size'][$i]);
					$obj->set('error', $_FILES[$packName]['error'][$i]);
					
					if ($i == 0 && $c == 1) {
						$this->set($packName, new BlankObject);
						$this->$packName = $obj;
					} else {
						if (!$this->has($packName))
							$this->set($packName, array());
						array_push($this->$packName, $obj);
					}
				}
			} else {
				$this->$packName->set('name', $_FILES[$packName]['name'][$i]);
				$this->$packName->set('tmp_name', $_FILES[$packName]['tmp_name'][$i]);
				$this->$packName->set('type', $_FILES[$packName]['type'][$i]);
				$this->$packName->set('size', $_FILES[$packName]['size'][$i]);
				$this->$packName->set('error', $_FILES[$packName]['error'][$i]);
			}
		}
	}
}

/**
 * Session class extending basic BlankObject.
 */
class Session extends \Blank\BlankObject {
	/**
	 * Starts session instance and sets up object's variables.
	 */
	public function __construct() {
		// session_name(md5($_SERVER['REMOTE_ADDR']));
		// session_name($_SERVER['REMOTE_ADDR']);
		session_start();
		$this->fromArray($_SESSION);
	}
	
	// public function set($variable, $value) {
	// 	$result = parent::set($variable, $value);
	// 	$this->save();
	// 	return $result;
	// }
	
	/**
	 * Saves its variables into $_SESSION global variable.
	 */
	public function save() {
		$_SESSION = $this->toArrayRecursive();
		// session_write_close();
	}
}

/**
 * Cookie class extending basic BlankObject.
 */
class Cookie extends \Blank\BlankObject {
	/**
	 * Starts session instance and sets up object's variables.
	 */
	private $cookieDuration;
	
	public function __construct($cookieDuration = 86400) {
		$this->fromArray($_COOKIE);
		$this->cookieDuration = intval($cookieDuration);
	}
	
	public function set($variable, $value) {
		$result = parent::set($variable, $value);
		$this->save();
		return $result;
	}
	
	/**
	 * Saves its variables into $_SESSION global variable.
	 */
	public function save() {
		$cookies = $this->toArray();
		foreach ($cookies as $cookie => $value) {
			setcookie($cookie, $value, (time() + $this->cookieDuration), '/');
		}
	}
}
?>