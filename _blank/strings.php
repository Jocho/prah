<?php
	/**
 	 * This is the basic Mf class. 
 	 * The "MF" is acronym for "my functions". It's an fragmentn from past and does not have any
 	 * relation to anything this class provides now. For now, the classes' job is to handle the
 	 * string and address-related operations. It provides: 
 	 * - simple string parser and formatter
 	 * - nice address creator
 	 * 
	 * @version: 1.11.3
	 */
	namespace Blank;

	class Strings
	{
		/**
		 * Returns first paragraph of text, if exists
		 * @param string $text
		 * @return mixed
		 */
		public function firstp($text)
		{
			$reg='/^[<p>]{3}(.+?)[<\/p>]{4}/';
			preg_match($reg,$text,$match);
			if(empty($match))
				return substr($text,0,400).'...';
			elseif(strlen($match[1])>400)
				return substr($match[1],0,400).'...';
			else
				return $match[1];
		}
		
		/**
		 * Returns first n characters of given text
		 * @param string $text
		 * @param int $val
		 * @return string
		 */
		public function first_n($text='',$val=400)
		{
			if($val<1)
				$val = 400;
			return substr($text,0,$val).'...';
		}

		/**
		 * Returns first N words (limit given by user) followed by hellip character (...).
		 * @param string $text - given text from which we are going to strip first words.
		 * @param int $limit   - limit of words being stripped from text. 
		 * @return string      - if there is match for searched count of words, those words are being returned. 
		 *                     - On the other hand, if there is no match, whole string will be returned.
		 */
		public function firstNWords($text = '', $limit = 10) {
			preg_match('/^\s*(?:\S+\s*){' . $limit . '}/', $text, $match);
			$res = (count($match)) ? trim($match[0]) : $text;
			if (strlen($res) < strlen($text))
				$res .= '&hellip;';
			return $res;
		}
		
		public function plainText($text) {
			return strip_tags(nl2br($text));
		}
		
		/**
		 * Deletes any tags that are included in the given text
		 * @param string $text
		 * @return string
		 */
		public function notags($text)
		{
			$text = $this->no_iframe($text);
			$reg='/\<[^<>]+\>(.*)\<\/[^<>]+\>/';
			return addslashes(preg_replace($reg,'$1',$text));
			//return strip_tags(nl2br(addslashes($text)),'[>>],[<<],[<>],[==],[><]');
		}
		
		/**
		 * Deletes any iframe tag exclusively. Other tags will be left.
		 * @param string $text
		 * @return string
		 */
		public function no_iframe($text)
		{
			$reg='/\<i?frame[^<>]*\>(.*)\<\/i?frame\>/';
			return addslashes(preg_replace($reg,'',$text));
		}
		
		/**
		 * Dediacritizer returns URL-friendly text. 
		 * @param string $text
		 * @return string
		 */
		public function ddkr($text)
		{
			$string1 = array('á','Á','ä','Ä','é','É','ě','Ě','í','Í','ý','Ý','ó','Ó','ô','Ô','ö','Ö','ú','Ú','č','Č','ď','Ď','ĺ','Ĺ','ľ','Ľ','ň','Ň','ŕ','Ŕ','ř','Ř','š','Š','ś','Ś','ť','Ť','ž','Ž','ź','Ź',' ','!','?',',','(', '[', '{', ')', ']', '}');
			$string2 = array('a','A','a','A','e','E','e','E','i','I','y','Y','o','O','o','O','o','O','u','U','c','C','d','D','l','L','l','L','n','N','r','R','r','R','s','S','s','S','t','T','z','Z','z','Z','-','','','', '', '', '', '', '', '');
			$result = preg_replace('/-{2,}/','-',strtolower(str_replace($string1,$string2,$text)));
			$result = preg_replace('/^-|-$/', '', trim($result));
			return $result;
		}

		/**
		 * Diacritizer parses camel-cased text or underscoped text into user-friendly readable text
		 * @param string $text
		 * @return string
		 */
		public function dkr($text, $method)
		{
			$text = trim($text);
			$group = array(
				'dash' => '-',
				'underscore' => '_',
				'space' => ' '
			);

			$text = preg_replace('/([a-z\d])([A-Z])/', '$1 $2', $text);
			$text = preg_replace('/[_-]/', ' ', $text);
			$text = preg_replace('/\s+/', ' ', $text);
			$text = (in_array($method, array_keys($group))) ? strtolower(preg_replace('/ /', $group[$method], $text)) : lcfirst(preg_replace('/ /', '', ucwords($text)));
			return $text;
		}
		
		/**
		 * Date returning method. Returns date in formated string.
		 * @param string $text
		 * @param string $volba
		 * @return string
		 */
		public function datum($text,$volba='')
		{
			$preg='/^(\d{4})-(\d{1,})-(\d{1,}) (\d{1,}):(\d{1,}):(\d{1,})/';
			switch($volba)
			{
				default: $sub='$4:$5:$6 $3.$2.$1'; break;
				case 'cas': $sub='$4:$5:$6'; break;
				case 'datum': $sub='$3.$2.$1'; break;
			}
			$text=preg_replace($preg,$sub,$text);
			return $text;
		}

		/**
		 * Changes slash type in given string
		 * 
		 * @param $string string - string in which the slashes will be changed
		 * @param $slashType string - type of slash (/ | \) which will be changed to the other one
		 * @return string
		 */
		public function changeSlashes($string, $slashType = '/') {
			$searchedSlash = ($slashType == '/') ? "/\\\{1,}/" : '/\//';
			return preg_replace($searchedSlash, $slashType, $string);
		}

		/**
		 * formats given path string into camel-cased style - the format that this framework uses
		 *
		 * @param $path string - given path
		 * @return string - formatted path
		 */
		public function getFormattedPath($path) {
			$p = $this->changeSlashes($path, '/');
			$pa = explode('/', $p);
			foreach ($pa as $key => $part)
				$pa[$key] = $this->dkr($part, 'normal', 'camelcased');
			return implode('/', $pa);
		}
		
		/**
		 * Private formatter function that translates inner code to HTML friendly one.
		 * @param string $text
		 * @return string
		 */
		private function fmtr($text)
		{
			//$p1='//'; $p2='';
			//$p1='/([\r?\n]){2,}/';
			$p1='/(<br>){1}/';
			$p2='</p><p>';
			
			$postp1='/(<p><\/p>)/';
			$postp2='<br><br>';
			//$postp1='//'; $postp2='';
			
			$n1='/([^\S ]{1,2})/';
			//$n1='/([\r?\n]){1}/';
			$n2='<br>';
			//$n1='//'; $n2='';
			
			$t1='/\t/';
			$t2='<span class="tab"></span>';
			
			$hr1='/-{4,}/';
			$hr2='<hr>';
			
			$np1='/\[==\]/';
			$np2='</div><div class="kapitola">';
			
			$left1='/\[>{2}\](.+?)\[\]/s';
			$left2='<span class="tal fl">$1</span>';
			
			$right1='/\[[\<]{2}\](.+?)\[\]/s';
			$right2='<span class="tar fr">$1</span>';
			
			$center1='/\[<>\](.+?)\[\]/s';
			$center2='<span class="tac">$1</span>';
			
			$justify1='/\[><\](.+?)\[\]/s';
			$justify2='<span class="taj">$1</span>';
			
			$bold1='/\*{3}([^\*{3}]+)\*{3}/';
			$bold2='<strong>$1</strong>';
			
			$italic1='/\*{2}([^\*{2}]+)\*{2}/';
			$italic2='<em>$1</em>';
			
			$h2a='/={2}([^=]{2,})={2}/m';
			$h2b='<h2>$1</h2>';
			$h3a='/={3}([^=]{2,})={3}/m';
			$h3b='<h3 name="'.$this->ddkr('$1').'">$1</h3>';
			$h4a='/={4}([^=]{2,})={4}/m';
			$h4b='<h4>$1</h4>';
			$h5a='/={5}([^=]{2,})={5}/m';
			$h5b='<h5>$1</h5>';
			$h6a='/={6}([^=]{2,})={6}/m';
			$h6b='<h6>$1</h6>';
		
			$kot1='/\:{2}(.+)\:{2}/s';
			$kot2='<a href="#'.$this->ddkr('$1').'">$1</a> ';
			//$kot1='//';$kot2='';
		
			$link1='/[^\"\':](https?\:\/\/[www\.]?[^\s\"\']{4,})\s/';
			$link2='<a href="$1" target="_blank">$1</a> ';
			$a1='/\:{2}([^:\"\']+)\:((?:https?\:\/\/[www\.]?[^\s\"\']{4,})|(?:\/src\/[^\'\"]+\/[^\'\"]+-\d+\.\w{3,5}))\s/';
			$a2='<a href="$2" target="_blank">$1</a> ';
			$img1='/\[\]([^:\'\"]+)\:((?:https?\:\/\/[www\.]?[^\s\"\']{4,})|(?:\/src\/[^\'\"]+\/[^\'\"]+-\d+\.\w{3,5}))\s/';
			$img2='<img src="$2" alt="$1" title="$1"> ';
			
			$apos1="/(\\')/";
			$apos2="'";

			$vzor=array($a1,$img1,$link1,$kot1,$hr1,$bold1,$italic1,$h6a,$h5a,$h4a,$h3a,$h2a,$t1,$n1,$p1,$left1,$right1,$center1,$justify1,$np1,$postp1,$apos1);
			$obrz=array($a2,$img2,$link2,$kot2,$hr2,$bold2,$italic2,$h6b,$h5b,$h4b,$h3b,$h2b,$t2,$n2,$p2,$left2,$right2,$center2,$justify2,$np2,$postp2,$apos2);
			return '<p>'.preg_replace($vzor,$obrz,$text).'</p>';
			//return preg_replace($vzor,$obrz,$text);
		}
		
		public function formatText($text) {
			$urlRegEx   = '(?:https?:\/\/[^\s\"\'\\\]+[\/#\w])';
			$localRegEx = '(?:[#\w\/\.\-\_]+)';

			$linkRegEx  = '(' . $urlRegEx . '|' . $localRegEx . ')';

			// direct anchor and image
			preg_match_all('/(\!)?\[([^\[\]]+?)\]\(' . $linkRegEx . '(?:\s+"(.+?)")?\)/', $text, $matches, PREG_SET_ORDER);
			foreach ($matches as $match) {
				$title       = isset($match[4]) ? $match[4] : $match[2];
				$targetBlank = (strpos($match[3], '#') === 0) ? '' : 'target="_blank"';
				if ($match[1] == '!')
					$text = str_replace($match[0], '<img src="' . $match[3] . '" alt="' . $title . '" title="' . $title . '" />', $text);
				else
					$text = str_replace($match[0], '<a href="' . $match[3] . '" ' . $targetBlank . ' title="' . $title . '">' . $match[2] . '</a>', $text);
			}

			// reference anchor and image
			preg_match_all('/(?:^|[ \t]{0,3})(\!)?\[(.+?)\]:\s+' . $linkRegEx . '\s+(?:["(](.+?)[")])?/m', $text, $matches, PREG_SET_ORDER);
			foreach ($matches as $match) {
				$title       = isset($match[4]) ? $match[4] : $match[2];
				$targetBlank = (strpos($match[3], '#') === 0) ? '' : 'target="_blank"';
				$text        = str_replace($match[0], '', $text);
				if ($match[1] == '!') {
					$text = preg_replace('/\[(.+?)\]\s+\[' . $match[2] . '\]/', '<img src="' . $match[3] . '" title="' . $title . '" />', $text);
					$text = str_replace('/\[' . $match[0] . '\]/', '<img src="' . $match[3] . '" title="' . $title . '" />', $text);
				}
				else {
					$text = preg_replace('/\[(.+?)\]\s+\[' . $match[2] . '\]/', '<a href="' . $match[3] . '" ' . $targetBlank . ' title="' . $title . '">\1</a>', $text);
					$text = str_replace('[' . $match[2] . ']', '<a href="' . $match[3] . '" target="_blank" title="' . $title . '">' . $match[2] . '</a>', $text);
				}
			}

			// kotva
			// $text = preg_replace('/::(.*):' . $urlRegEx . '/', '<a href="#$2" title="$1">$1</a>', $text);
			// clean url anchor
			$text = preg_replace('/(^|\s)(?:' . $urlRegEx . ')/', '$1<a href="$2" target="_blank">$2</a>', $text);
			// bold
			$text = preg_replace('/([*_]{2})(.+?)\1/', '<strong>$2</strong>', $text);
			// emphasized
			$text = preg_replace('/([*_])(.+?)\1/', '<em>$2</em>', $text);
			// code
			$text = preg_replace('/`([\s\S]+?)`/', '<code>$1</code>', $text);
			// headers
			$text = preg_replace_callback('/^(#{6})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(6, $matches[2]); }, $text);
			$text = preg_replace_callback('/^(#{5})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(5, $matches[2]); }, $text);
			$text = preg_replace_callback('/^(#{4})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(4, $matches[2]); }, $text);
			$text = preg_replace_callback('/^(#{3})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(3, $matches[2]); }, $text);
			$text = preg_replace_callback('/^(#{2})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(2, $matches[2]); }, $text);
			$text = preg_replace_callback('/^(#{1})(.+)\1?/m', function($matches) {return $this->anchorizeHeader(1, $matches[2]); }, $text);
			// $text = preg_replace('/^(#{5})(.+)\1?/m', '<h5 id="anchor-' . $this->ddkr('$2') . '">$2</h5>', $text);
			// $text = preg_replace('/^(#{4})(.+)\1?/m', '<h4 id="anchor-' . $this->ddkr('$2') . '">$2</h4>', $text);
			// $text = preg_replace('/^(#{3})(.+)\1?/m', '<h3 id="anchor-' . $this->ddkr('$2') . '">$2</h3>', $text);
			// $text = preg_replace('/^(#{2})(.+)\1?/m', '<h2 id="anchor-' . $this->ddkr('$2') . '">$2</h2>', $text);
			// $text = preg_replace('/^#(.+)#?/m', '<h1 id="anchor-' . $this->ddkr('$1') . '">$1</h1>', $text);
			// hr
			$text = preg_replace('/^-{4,}/m', '<hr />', $text);
			// unordered/ordered lists
			$text = preg_replace('/((\r?\n|^)- [\s\S]+?)(?=(\r?\n){2,}|\d+\. |$)/', "<ul>\n$1\n</ul>", $text);
			$text = preg_replace('/((\r?\n|^)\d+\. [\s\S]+?)(?=(\r?\n){2,}|- |$)/', "<ol>\n$1\n</ol>", $text);
			$text = preg_replace('/((\r?\n|^)(-|\d+\.)) ([\S\s]*?)(?=\n|(- |\d+\.)|<\/(u|o))/', '<li>$4</li>', $text);
			
			// text aligns
			$text = preg_replace('/(\S.+)(?=(\r?\n){1,}|$)/', '<p>$1</p>', $text);
			$text = preg_replace('/<p>\[>{2}\]/', '<p class="tal">', $text);
			$text = preg_replace('/<p>\[<{2}\]/', '<p class="tar">', $text);
			$text = preg_replace('/<p>\[<>\]/', '<p class="tac">', $text);
			$text = preg_replace('/<p>\[\><\]/', '<p class="taj">', $text);
			$text = preg_replace('/<p>\[\<\/\>\](.*?)<\/p>/', '<pre>$1</pre>', $text);
			$text = preg_replace('/<p>(\[[=-]{4}\])(.*?)<\/p>/', '$2', $text);
			// $text = str_replace('/<p><\/p>/', '', $text);
			// blockquote
			$text = preg_replace('/<p( class\="[\w ]+")?>> *([\s\S]*?)<\/p>/', '<blockquote$1>$2</blockquote>', $text);
			// text blocks

			// 2-column
			$text = preg_replace('/(\[==\])?([\s\S]+?)(?=(\r?\n){2,}|$)/', '<section>$2</section>', $text);
			$text = preg_replace('/<section>\[=-\]/', '<section class="column5">', $text);
			$text = preg_replace('/<section>\[-=\]/', '<section class="column5 offset5">', $text);
			
			// 3-column
			$text = preg_replace('/(\[===\])?([\s\S]+?)(?=(\r?\n){2,}|$)/', '<section>$2</section>', $text);
			$text = preg_replace('/<section>\[=--\]/', '<section class="column33">', $text);
			$text = preg_replace('/<section>\[-=-\]/', '<section class="column33 offset33">', $text);
			$text = preg_replace('/<section>\[--=\]/', '<section class="column33 offset66">', $text);
			$text = preg_replace('/<section>\[==-\]/', '<section class="column66">', $text);
			$text = preg_replace('/<section>\[-==\]/', '<section class="column66 offset33">', $text);

			// 4-column
			$text = preg_replace('/(\[====\])?([\s\S]+?)(?=(\r?\n){2,}|$)/', '<section>$2</section>', $text);
			$text = preg_replace('/<section>\[=---\]/', '<section class="column25">', $text);
			$text = preg_replace('/<section>\[-=--\]/', '<section class="column25 offset25">', $text);
			$text = preg_replace('/<section>\[--=-\]/', '<section class="column25 offset5">', $text);
			$text = preg_replace('/<section>\[---=\]/', '<section class="column25 offset75">', $text);
			$text = preg_replace('/<section>\[==--\]/', '<section class="column5">', $text);
			$text = preg_replace('/<section>\[-==-\]/', '<section class="column5 offset25">', $text);
			$text = preg_replace('/<section>\[--==\]/', '<section class="column5 offset5">', $text);
			$text = preg_replace('/<section>\[===-\]/', '<section class="column75">', $text);
			$text = preg_replace('/<section>\[-===\]/', '<section class="column75 offset25">', $text);
			
			$text = preg_replace('/\[----\]/', '<div class="clear"></div>', $text);

			return $text;
		}

		private function anchorizeHeader($level, $text) {
			return '<h' . $level . ' id="' . trim($this->ddkr($text)) . '">' . $text . '</h' . $level . '>';
		}
	}
?>
