<?php
	/**
	 * @author Marek J. Kolcun
	 * @version: 1.3.5
	 */

	namespace Blank;

	class Database {
		public $database;
		protected $table;
		private $whereCalled = FALSE;
		private $request;
		
		public function __construct($database) {
			$this->database = new BlankObject;
			$this->database = $database;
			$debug = (strtolower($this->database->get('debug')) == 'true') ? TRUE : FALSE;
			$this->database->set('debug', $debug);
			$this->database->set('query', '');
			$this->database->set('connected', FALSE);
			$this->database->set('connection', NULL);
			$this->connect();
			if (!isset($this->table) || $this->table == '') {
				$parts = explode('\\', get_class($this));
				$this->table = strtolower(end($parts));
			}
		}

		public function __destruct() {
			$this->disconnect();
		}

		/**
		 * method that starts the connection to the database
		 * @return TRUE on success or FALSE on failure
		 */
		public function connect() {
			switch ($this->database->type) {
				case 'mysql': 
					$this->database->connection = mysql_connect($this->database->address, $this->database->user, $this->database->password);

					try {
						if ($this->database->connection != NULL) {
							$this->database->connected = TRUE;
							$db = mysql_select_db($this->database->name, $this->database->connection);
							try {
								if (!$db)
									throw new \DatabaseSelectionFailedException; 
							}
							catch (DatabaseSelectionFailedException $e) {
								$e->getMessage(); die();
							}
						}
						else throw new \DatabaseConnectionFailedException;
					}
					catch (DatabaseConnectionFailedException $e) {
						$e->getMessage(); die();
					}
					mysql_query('SET NAMES "utf8"');
					mysql_query('SET CHARACTER SET "utf8"');
					return $db;
				break;

				case 'mysqli': 
					$this->database->connection = new \mysqli($this->database->address, $this->database->user, $this->database->password);
					$this->database->connection->set_charset('utf8');
					if (!$this->database->connection->connect_errno) {
						$this->database->connected = TRUE;
						return TRUE;
					}
					throw new \DatabaseConnectionFailedException;

				break;

				case 'sqlite': 
					try {
						$this->database->connection = new \SQLite3(ROOT.'/db/'.$this->database->name.'.db');
					}
					catch (DatabaseUnknownException $e) {
						echo $e;
					}
					if (!empty($this->database->connection)) {
						$this->database->connected = TRUE;
						return TRUE;
					}
					throw new \DatabaseConnectionFailedException;
				break;
			}
		}

		/**
		 * method that ends the database connection
		 * @return TRUE on success or FALSE on failure
		 */
		public function disconnect() {
			switch($this->database->type)
			{
				case 'mysql':
				{
					$this->database->connected = FALSE;
					MySQL_Error();
					if ($this->database->connection)
						MySQL_Close($this->database->connection) or die (mysql_error());
					$this->database->connection = NULL;
					return $this->database->connected;
					// return (MySQL_Close($this->database->connection))? TRUE : FALSE;
				}
				case 'mysqli':
				{
					$this->database->connected = FALSE;
					return $this->database->connection->close();
				}
				case 'sqlite':
				{
					$this->database->connected = FALSE;
					try {
						$db = new \SQLite3(ROOT . '/db/' . $this->database->name . ".db");
						
					} catch(Exception $e) {
						var_dump ($e);
					}
					return $db->close();
				}
				break;
			}
		}

		/**
		 * method that sets up the table variable for single database query call
		 * use this method when non-objected database call eeded
		 * call it BEFORE ANY OTHER METHOD
		 * @param $table string
		 * @return object
		 */
		public function table($table) {
			$this->alternativeTable = $table;
			return $this;
		}

		private function getUsedTable() {
			$usedTable = (isset($this->alternativeTable) && strlen($this->alternativeTable)) 
				? $this->alternativeTable 
				: $this->table;

			unset($this->alternativeTable);
			return $usedTable;
		}

		/**
		 * basic select method. Used to open the database query
		 * @param $values string
		 * @return object on success or NULL on failure
		 */
		public function select($values) {
			$this->request = 'select';
			$vals = explode(',', $values);
			$escapedVals = array();
			foreach ($vals as $val) {
				$val = trim($val);
				$escapedVals[] = (strpos($val, '.') !== FALSE || $val == '*' || preg_match('/\w+ ?\(\w+\)/', $val)) ? $val : '`' . $val . '`';
			}

			$usedTable = $this->getUsedTable();
			
			$this->database->query = 'SELECT ' . implode(', ', $escapedVals) . ' FROM ' . $usedTable;
			return $this;
		}

		/**
		 * basic update method. Used to open the database query
		 * @return object on success or NULL on failure
		 */
		public function update($values) {
			$this->request = 'update';
			if (is_array($values)) {
				$assignments = $this->assigner($values);
				if (count($assignments)) {
					$usedTable = $this->getUsedTable();
					$this->database->query = 'UPDATE ' . $usedTable . ' SET ' . implode(',', $assignments);
					return $this;
				}
			}
			else die('update(input): input is not an array');
		}

		/**
		 * basic insert method. Used to open the database query
		 * @return object on success or NULL on failure
		 */
		public function insert($values) {
			$this->request = 'insert';
			if (is_array($values)) {
				$rows = '`' . implode('`,`', array_keys($values)) . '`';
				$vals = array();
				foreach (array_values($values) as $value) {
					$vals[] = "'" . $this->escapedString($value) . "'";
				}
				$usedTable = $this->getUsedTable();
				$this->database->query = 'INSERT INTO ' . $usedTable . '(' . $rows .') VALUES (' . implode(',', $vals) . ')';
				
				return $this;
			}
			else die('insert(input): input is not an array');
		}

		/**
		 * basic delete method. Used to open the database query
		 * @param mixed data
		 * @return object on success or NULL on failure
		 */
		public function delete() {
			$this->request = 'delete';
			$values = (func_num_args() == 2)? array(func_get_arg(0) => func_get_arg(1)) : func_get_arg(0);
			if (is_array($values)) {
				$assignments = $this->assigner($values, TRUE);
				if (count($assignments)) {
					$usedTable = $this->getUsedTable();
					$this->database->query = 'DELETE FROM ' . $usedTable . ' WHERE ' . implode(' AND ', $assignments);
					return $this;
				}
				else die('delete(input): input does not consist form two parameters nor array');
			}
		}

		/**
		 * where is dependency method. Used to send the assignments to database query
		 * @param mixed data
		 * @return object on success or NULL on failure
		 */
		public function where() {
			$values = (func_num_args() == 2)? array(func_get_arg(0) => func_get_arg(1)) : func_get_arg(0);
			if (is_array($values)) {
				$assignments = $this->assigner($values, TRUE);
				if (count($assignments)) {
					$whereOr = ($this->whereCalled) ? ' OR ' : ' WHERE ';
					if (!$this->whereCalled)
						$this->whereCalled = TRUE;
					$this->database->query .= $whereOr . '(' . implode(' AND ', $assignments) . ')';
					return $this;
				}
			}
		}

		/**
		 * order method. Used to set the order of the returned data. 
		 * @param $values string
		 * @return object on success or NULL on failure
		 */
		public function order($values) {
			$this->database->query .= ' ORDER BY ' . $values;
				return $this;
		}

		/**
		 * offset method sends the offset and limit of returned data from list
		 * check out that checkout is second parameter!!!
		 * @param $count int
		 * @param $offset int
		 * @return object on success or FALSE on failure
		 */
		public function limit($count, $offset = 0) {
			$this->database->query .= ($offset)? ' LIMIT ' . $offset . ', ' . $count : ' LIMIT ' . $count;
			return $this;
		}

		/**
		 * method that sends GROUP BY parameter to database query
		 * @param $values
		 * @return object on success or FALSE on failure
		 */
		public function group($values) {
			$this->database->query .= ' GROUP BY ' . $values;
			return $this;
		}

		/**
		 * joining method. Used to join more tables in database
		 * join requires 2 or 3 parameters: 
		 *	-	when two, the first one is name of second joined table and second one is related tables value
		 *	-	when three, the first one is JOIN parameter (left, right, inner, outer) and others are like in
		 *		two parameter case
		 * @return object on success or FALSE on failure
		 */
		public function join() {
			if (func_num_args() == 2)
				$this->database->query .= ' JOIN ' . func_get_arg(0) . ' ON ' . func_get_arg(1);
			elseif (func_num_args() == 3 && in_array(strtolower(func_get_arg(0)), array('inner', 'outer', 'left', 'right')))
				$this->database->query .= ' ' . strtoupper(func_get_arg(0)) . ' JOIN ' . func_get_arg(1) . ' ON ' . func_get_arg(2);
			else
				die('join(input): input does not consist from exactly 2 or 3 parameters');
			return $this;
		}

		/**
		 * method that supplies all the queries that you cannot call by predefined methods. 
		 * Simply write whole query there and do not forget to call run() method afterwards
		 * @param $value string
		 * @return object
		 */
		public function query($value, $add = TRUE) {
			if ($add === TRUE)
				$this->database->set('query', $this->database->query . ' ' . $value . ' ');
			else
				$this->database->set('query', $value);
			return $this;
		}

		/**
		 * method that runs whole query and returns the desired data
		 * @return array on success or boolean in BOOLEAN-type query FALSE on query failure
		 */
		public function run($form = '') {
			$result = array();
			if ($this->database->debug)
				var_dump('-> ' . $this->database->query);
			$this->database->query = trim($this->database->query);
			$this->whereCalled = FALSE;

			switch ($this->database->type)
			{
				case 'mysql':
					$rows = mysql_query($this->database->get('query'));
					$this->database->query = '';
					
					if (is_resource($rows)) {
						while ($row = mysql_fetch_array($rows)) {
							foreach ($row as $key => $value)
								if (is_numeric($key))
									unset($row[$key]);
							$result[] = $row;
						}
						return $this->dbReturn($result, $form);
					}
					elseif (is_bool($rows))
						return $rows;
					else
						return NULL;
				
				case 'mysqli':
					$db = $this->database->connection;
					$result = $db->query($this->database->get('query'));
					$this->database->query = '';
					if ($db->errno==0) {
						if (is_object($result)) {
							while ($row = $result->fetch_assoc()) {
								foreach ($row as $key => $value)
									if (is_numeric($key))
										unset($row[$key]);
								$result[] = $row;
							}
							$result->free();
							return $this->dbReturn($result, $form);
						}
						else
							return $result;
					}
					else
						return NULL;
				
				case 'sqlite':
					if ($this->request == 'select') {
						$rows = $this->database->connection->query($this->database->get('query'));
						$this->database->query = '';
						while ($row = $rows->fetchArray()) {
							foreach ($row as $key => $value)
								if(is_numeric($key))
									unset($row[$key]);
							$result[] = $row;
						}
						return $this->dbReturn($result, $form);
					}
					else
					{
						$req = $this->database->query;
						$this->database->query = '';

						$result = $this->database->connection->exec($req);
						if ($this->request == 'insert') {
							return $this->database->connection->lastInsertRowID();
						}
						return $result;
					}
			}
		}

		/**
		 * returns data based on return size
		 *
		 * @return mixed value
		*/
		private function dbReturn($result, $form = '') {
			$class = get_class($this);

			switch ($form) {
				case 'object': 
					$return = new BlankObject;
					if (count($result))
						foreach ($result[0] as $key => $value)
							$return->set($key, stripslashes($value));
					else
						foreach (get_class_vars(get_class($this)) as $variable => $value)
							if (!in_array($variable, array('database', 'table')))
								$return->set($variable, $value);
					break;

				case 'assoc':
					$return = $result;
					break;

				case 'result': 
					$return = count($result) ? reset($result[0]) : NULL;
					break;

				case 'list':
					$return = array();
					foreach ($result as $res)
						$return[] = reset($res);
					break;

				default:
					$return = array();
					foreach ($result as $res) {
						$rtn = new BlankObject;
						foreach ($res as $key => $value)
							$rtn->set($key, stripslashes($value));
						$return[] = $rtn;
					}
				break;
			}
			return $return;
		}

		private function escapedString($string) {
			switch($this->database->type) {
				case 'mysql':
					return mysql_real_escape_string($string);
				case 'mysqli': 
					return $this->database->connection->mysql_real_escape_string($string);
				case 'sqlite':
					return $this->database->connection->escapeString($string);
			}
		}
		
		/**
		 * showQuery can be called whenever in database query building chain
		 * It prints out the actual query database variable on the screen
		 * @return object
		 */
		public function showQuery() {
			var_dump($this->database->get('query'));
			return $this;
		}

		/**
		 * private method that is used to setup assignment values in queries
		 * whenever inputed variables are in an array or as two parameters
		 * @param $values array
		 * @return array
		 */
		private function assigner($values, $inWhere = FALSE) {
			$assignments = array();
			foreach ($values as $column => $value) {
				// preg_match('/(\S+)(?: ([\!\<\>\+\-\*\/])(?:=|>))?$/', $column, $matches);
				preg_match('/(\S+) *([\!\<\>\+\-\*\/\%]=?)?$/', $column, $matches);
				
				$rest = '= ';
				if (count($matches)) {
					$column = $matches[1];

					$rest = (isset($matches[2]) && strlen($matches[2])) ? $matches[2] : '=';
					if ($rest == '!')
						$rest = '!=';

					if (is_null($value)) {
						$rest   = ($rest == '!=') ? 'IS NOT' : 'IS';
						if (!$inWhere)
							$rest = '= ';
						$value .= ' NULL';
					}

					elseif (is_array($value)) {
						foreach ($value as $k => $val) {
							$value[$k] = (is_numeric($val))? $val : "'" . addslashes($val) . "'";
						}

						$rest  = ($rest == '!=') ? 'NOT IN' : 'IN';
						$value = '(' . implode(',', $value) . ')';
					}

					else {
						if (is_string($value)) {
							$value = "'" . $this->escapedString($value) . "'";
						}

						$operator = (isset($matches[2]) && preg_match('/[\+\-\*\/]/', $matches[2], $submatch)) ? $submatch[0] : '';

						if (strlen($operator)) {
							$rest = '= ';
							$value = $column . ' ' . $operator . ' ' . $value;
						}

						if ($inWhere && isset($matches[2]) && preg_match('/\%=?/', $matches[2]))
							$rest = 'LIKE ';
					}
				}
				$assignments[] = $column . ' ' . $rest . $value;
			}
			return $assignments;
		}
	}
?>