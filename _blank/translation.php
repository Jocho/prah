<?php
namespace Blank;

define('TRANSLATIONS', ROOT . 'translations/');

/**
 * This is basic translation class. It uses a well-formated JSON file located in "/translations/", whose name
 * corresponds to the first value in the HTTP_ACCEPT_LANGUAGE header, or to the value stored in Cookie.
 *
 * @author Marek J. Kolcun
 * @since 1.2.10
 * @version 1.0
 */

class Translation extends \Blank\BlankObject {
	private $useTranslation;
	public $info;
	public $data;

	/**
	 * Uses slightly different construct than basic Blank Object. It may content also a language file that is going to be loaded.
	 * @author Marek J. Kolcun
	 * @param $language string - a 2-character code of language name that is going to be loaded.
	 */
	public function __construct($language = NULL, $useTranslation) {
		parent::__construct();
		$this->useTranslation = (strtoupper($useTranslation) == 'TRUE') ? TRUE : FALSE;
		if ($this->useTranslation)
			$this->load($language);
	}

	/**
	 * Performs neccessary checks before it loades the data. 
	 * To load the language properly there must exist:
	 * 1. settings inside settings.json
	 * 2. language directory
	 * 3. proper language JSON file choosen by Cookie or SERVER header.
	 *
	 * @author Marek J. Kolcun
	 * @param $language string - code of language to be loaded
	 */
	public function load($language = NULL) {
		if (is_null($language)) {
			$language = (isset($_COOKIE["language"])) ? $_COOKIE["language"] : substr($_SERVER["HTTP_ACCEPT_LANGUAGE"], 0, 2);
		}
	
		try {
			if (is_dir(TRANSLATIONS) && is_readable(TRANSLATIONS)) {
				try {
					if (is_file(TRANSLATIONS . $language . '.json')) {
						$source     = file_get_contents(TRANSLATIONS . $language . '.json');
						$data       = json_decode($source, TRUE);
						$this->info = $data['info'];
						$this->data = $data['data'];
					}
					else
						throw new \Exception;
				}
				catch (\Exception $e) {
					echo $e->getMessage();
					echo 'Presumed file ' . TRANSLATIONS . $language . '.json does not exist.';
					die();
				}
			}
			else {
				throw new \Exception;
			}
		}
		catch (\Exception $e) {
			echo $e->getMessage();
			echo 'Presumed folder ' . TRANSLATIONS . ' does not exist.';
			die();
		}
	}



	/**
	 * Returns corresponding language translation of a given string keyword. 
	 * When use of translations is set to FALSE (in settings), only keyword will be returned.
	 * 
	 * @author Marek J. Kolcun
	 * @param $keyword string - a string, whose translation will be returned.
	 * @param $data           - when a string or whole indexed array is inserted, additionally a sprintf() function will be used.
	 * @param $tree array     - an array of translations taken from source file.
	 * @return string         - Whether given keyword or corresponding translation of this keyword.
	 */
	public function say($keyword, $data = NULL, $tree = NULL) {
		if (!$this->useTranslation)
			$result = $keyword;
		else {
			if ($tree === NULL)
				$tree = $this->data;
			$result = $keyword;

			if (is_array($tree)) {
				if (array_key_exists($keyword, $tree)) {
					$result = $tree[$keyword];
				}
				else {
					foreach ($tree as $branch) {
						$result = $this->say($keyword, $data, $branch);
						if (sprintf($keyword, $data) != $result)
							break;
					}
				}
			}
		}

		if (!is_null($data))
			$result = sprintf($result, $data);
		return $result;
	}
}
?>
