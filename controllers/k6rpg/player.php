<?php
namespace k6rpg;

class Player extends SignedK6Page {
	public function init() {
		parent::init();
		$this->gear('\k6rpg\Templates', 'TEMPLATES');
		$this->gear('\k6rpg\Modals', 'MODALS');
	}
	public function render() {
		// $locale = new Locale();
		parent::render();
	}

	public function stuffRender() {
		$this->gear('\\k6rpg\\Items', 'i');
		$items = $this->models->k6rpgDatabaseItems->getByGame(2);
		print_r($this->gears->i->getItemListHash($items));
	}
}
?>