<?php
namespace k6rpg;

class Items extends \k6rpg\SignedK6Gear {
	public function init() {
		parent::init();
	}

	public function render() {
		parent::render();
	}
	
	public function requestResult($data = NULL) {
		if (is_array($data))
			$this->post->fromArray($data);

		if ($this->blank->isAjaxRequest)
			$this->{strtolower($this->blank->requestMethod)}();
	}

	private function get() {
		if (isset($this->router->query['item_id'])) {
			$this->template->item = $this->models->k6rpgDatabaseItems->getById($this->router->query['item_id']);
		}
		elseif (isset($this->router->query['char_id'])) {
			$items = $this->models->k6rpgDatabaseItems->getByCharacter($this->router->query['char_id'], $this->character->game_id);
			$newHash = $this->getItemListHash($items);
			
			if ($this->blank->cookie->get('itemHash') != $newHash)
				$this->template->itemList = $this->indexItems($items);

			$this->blank->cookie->set('itemHash', $newHash);
		}
		elseif (isset($this->router->query['game_id'])) {
			$items = $this->models->k6rpgDatabaseItems->getByGame($this->router->query['game_id']);
			$this->template->itemList = $this->indexItems($items);
		}
		else {
			$this->template->itemList = array();
		}
	}

	private function put() {
		// var_dump($this->character, $this->game, $this->player);
		if ($this->models->k6rpgDatabaseItems->charHasItem($this->character->id, $this->post->id)
			|| $this->player->id == $this->game->user_id
		) {
			$item = $this->post;
			$item->character_id = $this->character->id;
			$item->game_id      = $this->character->game_id;
			$this->template->saved = $this->models->k6rpgDatabaseItems->set($item);
		}
		else {
			$this->template->infobar = "You don't own this item.";
		}
	}

	private function post() {
		$item = $this->post;
		unset($item->id);
		$item->character_id = $this->character->id;
		$item->game_id = $this->character->game_id;
		$this->template->item_id = $this->models->k6rpgDatabaseItems->add($item);
		$this->template->saved   = ($this->template->item_id) ? TRUE : FALSE;
	}

	private function delete() {
		if (!isset($this->router->path[3]) || !is_numeric($this->router->path[3]))
			return;

		$id = $this->router->path[3];

		if (!$this->character->id) {
			$this->template->deleted = $this->models->k6rpgDatabaseItems->del($id);
		}
		else {
			if ($this->models->k6rpgDatabaseItems->charHasItem($this->character->id, $id)) {
				$this->template->deleted = $this->models->k6rpgDatabaseItems->drop($this->character->id, $id);
			}
			else {
				$this->template->infobar = "You don't own this item.";
			}
		}
	}


	private function indexItems($items) {
		return array_map(function($item) use (&$index) {
			$item->index     = $index;
			$item->stackable = intval($item->stackable);
			$index++;
			return $item;
		}, $items);
	}

	public function getItemListHash($itemList = array(), $level = 0) {
		$result = array();
		$itemList = array_values($itemList);
		foreach ($itemList as $key => $value) {
			if (is_object($value))
				$value = $value->toArrayRecursive();

			if (is_array($value)) {
				$result[$key] = $this->getItemListHash($value, $level + 1);
			}
			elseif (is_numeric($value))
				$result[$key] = intval($value);
			elseif (is_string($value))
				$result[$key] = strlen($value);
		}

		return (!$level) ? md5(json_encode($result)) : $result;
	}
}
?>
