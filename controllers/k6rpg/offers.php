<?php
namespace k6rpg;

class Offers extends \k6rpg\SignedK6Gear {
	public function init() {
		parent::init();
	}

	public function render() {
		parent::render();
	}

	public function requestResult($data = NULL) {
		if (is_array($data))
			$this->post->fromArray($data);

		if ($this->blank->isAjaxRequest)
			$this->{strtolower($this->blank->requestMethod)}();
	}

	private function get() {
		$this->template->to = $this->models->k6rpgDatabaseCharacters->getWithSpaceInInventory($this->character->game_id, $this->character->id);
	}

	private function put() {
		$offer = $this->post;
		$this->template->saved = $this->models->k6rpgDatabaseItems->give($offer);
	}

}
?>