<?php
namespace k6rpg;

class Chars extends \k6rpg\SignedK6Page {
	public function init() {
		parent::init();
		$this->template->name = 'characters';

		// if ($this->session->has('character'))
		// 	$this->session->del('character');
	}
	public function render() {
		parent::render();

		$character = (isset($this->router->query['char_id']) && $this->models->k6rpgDatabaseCharacters->hasPlayerCharacter($this->player->id, $this->router->query['char_id']))
			? $this->models->k6rpgDatabaseCharacters->getById($this->router->query['char_id'])
			: new \Blank\BlankObject(array('id' => 0, 'game_id' => 0));

		if (isset($this->router->path[2])) {
			switch($this->router->path[2]) {
				case 'items':
					$this->gear('\\k6rpg\\Items', 'items');
					$this->gears->items->character = $character;
					$this->gears->items->requestResult();
					break;
				case 'skills':
					$this->gear('\\k6rpg\\Skills', 'skills');
					$this->gears->skills->character = $character;
					$this->gears->skills->requestResult();
					break;
				case 'talents':
					$this->gear('\\k6rpg\\Talents', 'talents');
					$this->gears->talents->character = $character;
					$this->gears->talents->requestResult();
					break;
				case 'offers':
					$this->gear('\\k6rpg\\Offers', 'offers');
					$this->gears->offers->character = $character;
					$this->gears->offers->requestResult();
					break;
				default: 
					$this->{strtolower($this->blank->requestMethod)}();
				break;
			}
			return;
		}

		$this->{strtolower($this->blank->requestMethod)}();
	}

	private function get() {
		if (isset($this->router->query['char_id'])) {
			$character = $this->models->k6rpgDatabaseCharacters->getById($this->router->query['char_id']);

			$this->template->character = ($character->user_id == $this->player->id) 
				? $this->prepareCharacter($character) 
				: new \Blank\BlankObject;
		}
		else {
			if (isset($this->router->path[1]) && $this->router->path[1] == 'light') {
				$characterList = $this->models->k6rpgDatabaseCharacters->getLightByPlayer($this->player->id);
			}
			else {
				$characterList = $this->models->k6rpgDatabaseCharacters->getByPlayer($this->player->id);
				foreach ($characterList as $k => $character) {
					$character = $this->prepareCharacter($character);
					$character->index = $k;
					$characterList[$k] = $character;
				}
			}

			$this->template->characterList = $characterList;
		}
	}

	private function post() {
		$this->post->user_id = $this->player->id;
		$result = $this->models->k6rpgDatabaseCharacters->addCharacter($this->post);
		if ($result)
			$this->saveCharacterData($result);

		$this->template->created = $result;
	}

	private function put() {
		$character = $this->models->k6rpgDatabaseCharacters->getById($this->router->query['char_id']);
		if ($this->models->k6rpgDatabaseCharacters->hasPlayerCharacter($this->player->id, $this->router->query['char_id'])) {
			$this->models->k6rpgDatabaseCharacters->saveCharacter($this->blank->post);
			$this->saveCharacterData($character->id);
		}
		else {
			$this->gears->info->say('CHAR_NOT_YOURS');
			$this->template->saved = FALSE;
		}
	}

	private function saveCharacterData($charId) {
		$character = $this->models->k6rpgDatabaseCharacters->getById($charId);

		$this->gear('\\k6rpg\\Items', 'items');
		$this->gear('\\k6rpg\\Talents', 'talents');
		$this->gear('\\k6rpg\\Skills', 'skills');

		$this->gears->items->character   = $character;
		$this->gears->talents->character = $character;
		$this->gears->skills->character  = $character;
		
		$actions = array();

		if ($this->blank->post->has('talents')) {
			foreach ($this->blank->post->talents->toArrayRecursive() as $talentGroup) {
				foreach ($talentGroup as $talent) {
					$this->gears->talents->requestResult($talent);
					$actions[] = $this->gears->talents->template->saved;
				}
			}
		}

		if ($this->blank->post->has('skills')) {
			foreach ($this->blank->post->skills->toArrayRecursive() as $skill) {
				$this->gears->skills->requestResult($skill);
				$actions[] = $this->gears->skills->template->saved;
			}
		}

		if ($this->blank->post->has('items')) {
			foreach ($this->blank->post->items->toArrayRecursive() as $item) {
				$this->gears->items->requestResult($item);
				$actions[] = $this->gears->items->template->saved;
			}
		}

		$this->template->saved = count($actions) == count(array_filter($actions));
	}

	private function delete() {
		$character = $this->models->k6rpgDatabaseCharacters->getById($this->router->query['char_id']);
		if (!$character->get('id')) {
			$this->gears->info->say($this->tran->say('CHAR_NOT_EXIST'), 'c-red');
			// $this->redirect('/characters');
		}
		else if ($character->user_id != $this->player->id) {
			$this->gears->info->say($this->tran->say('CHAR_NOT_YOURS'), 'c-red');
			// $this->redirect('/characters');
		}
		else {
			$this->template->deleted = $this->models->k6rpgDatabaseCharacters->del($this->router->query['char_id'], $this->player->id);
		}
	}

	private function prepareCharacter($character) {
		$attrs = array('pow', 'dex', 'knw', 'san', 'vit');
		$attributes = array();

		foreach ($attrs as $attr) {
			$attributes[] = array(
				'attr'  => $attr,
				'bonus' => 0,
				'max'   => $character->{'max_' . $attr},
				'value' => $character->$attr,
			);
			unset($character->{'max_' . $attr}, $character->$attr);
		}

		//items
		$result = $this->models->k6rpgDatabaseItems->getByCharacter($character->id, $character->game_id);
		$items  = array();
		foreach ($result as $k => $item) {
			// name, attributes [attr, value], amount, text_value, description, price, stackable, group
			$item->index     = $k;
			$item->stackable = intval($item->stackable);
			unset($item->game_id);
			$items[] = $item;
		}

		//skills
		$result = $this->models->k6rpgDatabaseSkills->getByCharacter($character->id);
		$skills = array();
		foreach ($result as $k => $skill) {
			$skill->index = $k;
			unset($skill->character_id);
			$skills[] = $skill;
		}

		//talents
		$result  = $this->models->k6rpgDatabaseTalents->getByCharacter($character->id);
		$talents = array('pow' => array(), 'dex' => array(), 'knw' => array(), 'san' => array());
		foreach ($result as $k => $talent) {
			$talent->relatedTo = $talent->related_to;
			$talent->index     = $k;
			unset($talent->character_id, $talent->related_to);
			$talents[$attrs[$talent->relatedTo]][] = $talent;
		}

		if ($character->game_id) {
			$game  = $this->models->k6rpgDatabaseGames->getById($character->game_id);
			$guild = $this->models->k6rpgDatabaseCharacters->getByGame($character->game_id, $character->id);

			$adventure = array(
				'id'          => $game->id,
				'search'      => '',
				'guild'       => array_values($guild),
				'title'       => $game->title,
				'description' => $game->description,
				'ruleset'     => $game->ruleset,
				'list'        => array()
			);
		}
		else {
			$adventure = array(
				'id'          => 0,
				'search'      => '',
				'guild'       => array(),
				'title'       => '',
				'description' => '',
				'ruleset'     => '',
				'list'        => array()
			);
		}

		$character->adventure  = $adventure;
		$character->attributes = $attributes;
		$character->items      = $items;
		$character->talents    = $talents;
		$character->skills     = $skills;
		$character->offers     = array();

		return $character;
	}

	public function addCharacterForm($form) {
		$form->set('user_id', $this->player->id);
		$form->del('addCharacter');
		$result = $this->models->k6rpgDatabaseCharacters->addCharacter($form);
		
		if ($result)
			$this->gears->info->say($this->tran->say('CHAR_CREATED_OK', $form->name), 'b-green');
		else
			$this->gears->info->say($this->tran->say('CHAR_CREATED_NO', $form->name), 'b-red');

		$this->redirect('', TRUE);
	}

	public function delCharacterHandler($router) {
		$result = $this->models->k6rpgDatabaseCharacters->delCharacter($router->query['id'], $this->player->id);
		if ($result)
			$this->gears->info->say($this->tran->say('CHAR_DELETED_OK'), 'b-green');
		else
			$this->gears->info->say($this->tran->say('CHAR_DELETED_NO'), 'b-red');
		$this->redirect('', TRUE);
	}
}
