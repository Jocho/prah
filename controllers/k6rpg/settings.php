<?php
namespace k6rpg;

class Settings extends \k6rpg\SignedK6Page {
	
	public function init() {
		parent::init();
	}

	public function render() {
		
	}

	public function saveSettingsForm($form) {
		$user = $this->models->blankUsers->getUserById($this->player->id);

		if ($form->has('new_password') && strlen($form->get('new_password'))) {
			if (!$form->has('old_password') || !strlen($form->get('old_password')))
				$this->gears->info->say($this->tran->say('SETTINGS_OLD_PASS_NOT_SET'), 'c-red');

			elseif ($user->password != $this->models->blankUsers->makeHash($form->get('old_password')))
				$this->gears->info->say($this->tran->say('SETTINGS_OLD_PASS_NO'), 'c-red');
			elseif (strlen($form->get('new_password')) < 5)
				$this->gears->info->say($this->tran->say('SETTINGS_NEW_PASS_SHORT'), 'c-red');
			elseif (!$form->has('rep_password') 
				|| $form->get('new_password') != $form->get('rep_password')
			)
				$this->gears->info->say($this->tran->say('SETTINGS_PASS_MATCH_NO'), 'c-red');
			else {
				$data = new \Blank\BlankObject;
				$data->set('password', $form->get('new_password'));
				$result = $this->models->blankUsers->editUser($this->player->id, $data);

				if ($result)
					$this->gears->info->say($this->tran->say('SETTINGS_SAVED_OK'), 'c-green');
				else
					$this->gears->info->say($this->tran->say('SETTINGS_SAVED_NO'), 'c-red');
			}
		}
		$this->redirect();
	}
}
?>