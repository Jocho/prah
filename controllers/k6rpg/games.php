<?php
namespace k6rpg;
class Games extends \k6rpg\SignedK6Page {
	// public function render() {
	// 	parent::render();

	// 	$this->{strtolower($this->blank->requestMethod)}();
	// }

	public function render() {
		if (isset($this->router->path[2]) && is_numeric($this->router->path[1])) {
			if (!$this->models->k6rpgDatabaseGames->hasPlayerGame($this->player->id, $this->router->query['game_id']))
				return;

			$game      = $this->models->k6rpgDatabaseGames->getById($this->router->query['game_id']);
			$character = new \Blank\BlankObject(array('id' => 0, 'game_id' => $game->id));
			
			switch ($this->router->path[2]) {
				case 'characters':
					$this->template->guild = $this->models->k6rpgDatabaseCharacters->getFullByGame($game->id);
					// $this->gear('\\k6rpg\\Chars', 'chars');
					// $this->gears->chars->
					break;
				case 'items':
					$this->gear('\\k6rpg\\Items', 'items');
					$this->gears->items->character = $character;
					$this->gears->items->game      = $game;
					$this->gears->items->requestResult();
					break;
				case 'offers':
					$this->gear('\\k6rpg\\Offers', 'offers');
					$this->gears->offers->character = $character;
					$this->gears->offers->requestResult();
					break;
				case 'npcs':
					$this->gear('\\k6rpg\\Npcs', 'npcs');
					$this->gears->npcs->game = $game;
					$this->gears->npcs->requestResult();
					break;
				case 'npc_groups':
					$this->gear('\\k6rpg\\NpcGroups', 'npcGroups');
					$this->gears->npcGroups->game = $game;
					$this->gears->npcGroups->requestResult();
					break;
				default: break;
			}
			return;
		}

		$this->{strtolower($this->blank->requestMethod)}();
	}

	private function get() {
		if (isset($this->router->query['game_id'])) {
			$game = $this->models->k6rpgDatabaseGames->getById($this->router->query['game_id']);
			$this->template->game = $this->prepareGame($game);
		}
		elseif (isset($this->router->path[1]) && $this->router->path[1] == 'search') {
			$this->template->results = $this->models->k6rpgDatabaseGames->getByTitle(urldecode($this->router->path[2]));
		}
		else {
			if (isset($this->router->path[1]) && $this->router->path[1] == 'light') {
				$gameList = $this->models->k6rpgDatabaseGames->getLightByGM($this->player->id);
			}
			else {
				$gameList = $this->models->k6rpgDatabaseGames->getByGM($this->player->id);

				foreach ($gameList as $k => $game) {
					$game = $this->prepareGame($game);
					$game->index = $k;
					$gameList[$k] = $game;
				}
			}

			$this->template->gameList = $gameList;
		}
	}

	private function post() {
		$result = $this->models->k6rpgDatabaseGames->addGame($this->blank->post);
		$this->template->created = $result;
	}

	private function put() {
		if (isset($this->router->query['game_id']) && $this->models->k6rpgDatabaseGames->hasPlayerGame($this->player->id, $this->router->query['game_id']))
			$this->template->saved = $this->models->k6rpgDatabaseGames->setGame($this->blank->post);
		else
			$this->template->saved = NULL;
	}

	private function delete() {
		$game = $this->models->k6rpgDatabaseGames->getById($this->router->query['game_id']);
		if (!$this->models->k6rpgDatabaseGames->hasPlayerGame($this->player->id, $game->id)) {
			$this->template->deleted = FALSE;
			return;
		}

		if (!$game->get('id')) {
			$this->gears->info->say($this->tran->say('GAME_NOT_FOUND'), 'c-red');
			// $this->redirect('/games');
		}
		else if ($game->user_id != $this->player->id) {
			$this->gears->info->say($this->tran->say('GAME_NOT_YOURS'), 'c-red');
			// $this->redirect('/characters');
		}
		else {
			$this->template->deleted = $this->models->k6rpgDatabaseGames->delGame($game->id, $this->player->id);
		}
	}

	private function prepareGame($game) {
		$game->guild     = array_values($this->models->k6rpgDatabaseCharacters->getExtendedByGame($game->id));
		$game->npcGroups = $this->models->k6rpgDatabaseNpcGroups->getByGame($game->id);
		$game->npcs      = $this->models->k6rpgDatabaseNpcs->getByGame($game->id);
		$game->items     = $this->getItems($game);
		return $game;
	}

	private function getItems($game) {
		$index = 0;
		return array_map(function($item) use (&$index) {
			$item->index = $index;
			$index++;
			return $item;
		}, $this->models->k6rpgDatabaseItems->getByGame($game->id));
	}

	public function addGameForm($form) {
		$form->set('user_id', $this->player->id);
		$form->set('created', time());
		$form->set('tstamp', time());

		$result = $this->models->k6rpgDatabaseGames->addGame($form);

		if ($result)
			$this->gears->info->say($this->tran->say('GAME_CREATED_OK', $form->set('title')), 'c-green');
		else
			$this->gears->info->say($this->tran->say('GAME_CREATED_NO', $form->set('title')), 'c-red');
		$this->redirect();
	}
}
?>