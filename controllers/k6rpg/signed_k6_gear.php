<?php
namespace k6rpg;

class SignedK6Gear extends \Blank\Gear {
	protected $player;
	protected $character;
	protected $game;

	public function init() {
		$this->player    = new \Blank\BlankObject;
		$this->character = new \Blank\BlankObject;
		$this->game      = new \Blank\BlankObject;

		$this->gear('\Blank\Sign', 'sign');
		$this->gear('\Blank\Info', 'info');
		if (!$this->gears->sign->isLogged())
			$this->gears->sign->logoutHandler();

		$this->player = $this->session->get('user');
	}

	public function render() {
		$this->template->player = $this->player;
	}
}
?>