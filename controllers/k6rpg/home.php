<?php
namespace k6rpg;
class Home extends \Blank\Gear {
	public function init() {
		$this->gear('\Blank\info', 'info');
		$this->gear('\Blank\menu', 'menu');
		$this->gear('\Blank\search', 'search');
		$this->gear('\Blank\sign', 'sign');
		$this->gear('\Blank\footer', 'footer');

		$this->template->SEARCH = '';
	}

	public function render() {
		$backgrounds = array('fantasy', 'horror', 'scifi');
		$random = rand(0, 2);
		$this->template->randomBackground = $backgrounds[$random];
	}

	public function aboutStuffRender() {}
	public function rulesRender() {

		$rules = file_get_contents(RESOURCES . 'prah-fantasy.txt');
		$this->template->rules = $this->strings->formatText($rules);
	}
}
?>