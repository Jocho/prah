<?php
namespace k6rpg;

class Npcs extends \k6rpg\SignedK6Gear {
	public function init() {
		parent::init();
	}

	public function render() {
		parent::render();
	}
	
	public function requestResult($data = NULL) {
		if (is_array($data))
			$this->post->fromArray($data);

		if ($this->blank->isAjaxRequest)
			$this->{strtolower($this->blank->requestMethod)}();
	}

	private function get() {
		if (isset($this->request->query['npc_id'])) {
			$this->template->npc = $this->models->k6rpgDatabaseNpcs->getById($this->request->query['npc_id']);
		}
		else {
			$this->template->npc = array();
		}
	}

	private function put() {
		if ($this->models->k6rpgDatabaseNpcs->gameHasNpc($this->game->id, $this->post->id)) {
			$this->template->saved = $this->models->k6rpgDatabaseNpcs->set($this->post);
		}
		else {
			$this->template->infobar = "You don't own this npc.";
		}
	}

	private function post() {
		$npc = $this->post;
		$npc->game_id = $this->game->id;
		unset($npc->id);
		$result = $this->models->k6rpgDatabaseNpcs->add($npc);
		$this->template->npc_id = (is_numeric($result)) ? $result : 0;
	}

	private function delete() {
		if (!isset($this->router->query['npc_id']) || !is_numeric($this->router->query['npc_id']))
			return;

		$id = $this->router->query['npc_id'];

		if ($this->models->k6rpgDatabaseNpcs->gameHasNpc($this->game->id, $id)) {
			$this->template->deleted = $this->models->k6rpgDatabaseNpcs->del($id);
		}
		else {
			$this->template->infobar = "You don't own this npc.";
		}
	}
}
?>
