<?php
namespace k6rpg;

class Skills extends \k6rpg\SignedK6Gear {
	public function init() {
		parent::init();
	}

	public function render() {
		parent::render();
	}
	
	public function requestResult($data = NULL) {
		if (is_array($data))
			$this->post->fromArray($data);

		if ($this->blank->isAjaxRequest)
			$this->{strtolower($this->blank->requestMethod)}();
	}

	private function get() {
		if (isset($this->request->query['skill_id'])) {
			$this->template->skill = $this->models->k6rpgDatabaseSkills->getById($this->request->query['skill_id']);
		}
		else {
			$this->template->skill = array();
		}
	}

	private function put() {
		if ($this->models->k6rpgDatabaseSkills->charHasSkill($this->character->id, $this->post->id)) {
			$this->template->saved = $this->models->k6rpgDatabaseSkills->set($this->post);
		}
		else {
			$this->template->infobar = "You don't own this skill.";
		}
	}

	private function post() {
		$skill = $this->post;
		unset($skill->id);
		$skill->character_id = $this->character->id;
		$result = $this->models->k6rpgDatabaseSkills->add($skill);
		$this->template->skill_id = (is_numeric($result)) ? $result : 0;
		$this->template->saved    = (is_numeric($result)) ? TRUE : FALSE;
	}

	private function delete() {
		if (!isset($this->router->path[3]) || !is_numeric($this->router->path[3]))
			return;
		$id = $this->router->path[3];

		if ($this->models->k6rpgDatabaseSkills->charHasSkill($this->character->id, $id)) {
			$this->template->deleted = $this->models->k6rpgDatabaseSkills->del($id);
		}
		else {
			$this->template->infobar = "You don't own this skill.";
		}
	}
}
?>
