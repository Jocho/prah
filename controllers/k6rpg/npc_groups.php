<?php
namespace k6rpg;

class NpcGroups extends \k6rpg\SignedK6Gear {
	public function init() {
		parent::init();
	}

	public function render() {
		parent::render();
	}
	
	public function requestResult($data = NULL) {
		if (is_array($data))
			$this->post->fromArray($data);

		if ($this->blank->isAjaxRequest)
			$this->{strtolower($this->blank->requestMethod)}();
	}

	private function get() {
		if (isset($this->request->query['npc_group_id'])) {
			$this->template->npcGroup = $this->models->k6rpgDatabaseNpcGroups->getById($this->request->query['npc_group_id']);
		}
		else {
			$this->template->npcGroups = $this->models->k6rpgDatabaseNpcGroups->getByGame($this->game->id);
		}
	}

	private function put() {
		if ($this->models->k6rpgDatabaseNpcGroups->gameHasNpcGroup($this->game->id, $this->post->id)) {
			$this->post->game_id   = $this->game->id;
			$this->template->saved = $this->models->k6rpgDatabaseNpcGroups->set($this->post);
		}
		else {
			$this->template->infobar = "You don't own this npc group.";
		}
	}

	private function post() {
		$npcGroup = $this->post;
		$npcGroup->game_id = $this->game->id;
		unset($npcGroup->id);
		$result = $this->models->k6rpgDatabaseNpcGroups->add($npcGroup);
		$this->template->npc_group_id = (is_numeric($result)) ? $result : 0;
	}

	private function delete() {
		if (!isset($this->router->query['npc_group_id']) || !is_numeric($this->router->query['npc_group_id']))
			return;
		$id = $this->router->query['npc_group_id'];

		if ($this->models->k6rpgDatabaseNpcGroups->gameHasNpcGroup($this->game->id, $id)) {
			$this->template->deleted = $this->models->k6rpgDatabaseNpcGroups->del($id);
		}
		else {
			$this->template->infobar = "You don't own this npc group.";
		}
	}
}
?>
