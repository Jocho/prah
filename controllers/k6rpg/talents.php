<?php
namespace k6rpg;

class Talents extends \k6rpg\SignedK6Gear {
	public function init() {
		parent::init();
	}

	public function render() {
		parent::render();
	}

	public function requestResult($data = NULL) {
		if (is_array($data))
			$this->post->fromArray($data);

		if ($this->blank->isAjaxRequest)
			$this->{strtolower($this->blank->requestMethod)}();
	}

	private function get() {
		if (isset($this->router->query['talent_id'])) {
			$this->template->talent = $this->models->k6rpgDatabaseTalents->getById($this->router->query['talent_id']);
		}
		else {
			$this->template->talent = array();
		}
	}

	private function put() {
		if ($this->models->k6rpgDatabaseTalents->charHasTalent($this->character->id, $this->post->id)) {
			$talent = $this->post;
			$talent->related_to = $talent->relatedTo;
			unset($talent->relatedTo);
			$this->template->saved = $this->models->k6rpgDatabaseTalents->set($talent);
		}
		else {
			$this->template->infobar = "You don't own this talent.";
		}
	}

	private function post() {
		$talent = $this->post;
		$talent->related_to = $talent->relatedTo;
		$talent->character_id = $this->character->id;
		unset($talent->relatedTo, $talent->id);
		$result = $this->models->k6rpgDatabaseTalents->add($talent);
		$this->template->talent_id = (is_numeric($result)) ? $result : 0;
		$this->template->saved     = (is_numeric($result)) ? TRUE : FALSE;
	}

	private function delete() {
		if (!isset($this->router->path[3]) || !is_numeric($this->router->path[3]))
			return;
		$id = $this->router->path[3];

		if ($this->models->k6rpgDatabaseTalents->charHasTalent($this->character->id, $id)) {
			$this->template->deleted = $this->models->k6rpgDatabaseTalents->del($id);
		}
		else {
			$this->template->infobar = "You don't own this talent.";
		}
	}
}
?>