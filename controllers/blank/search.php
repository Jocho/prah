<?php
namespace Blank;
class Search extends \Blank\Gear {
	private $keywords;
	private $found;

	public function init() {}

	public function render() {
		$this->template->keywords = ($this->session->has('search')) ? $this->session->search->get('keywords') : '';
		$this->template->searchActive = strlen($this->template->keywords);
	}

	public function clearSearchForm() {
		if ($this->session->has('search'))
			$this->session->del('search');
		$this->redirect();
	}
}
?>