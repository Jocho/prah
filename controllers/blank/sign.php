<?php
namespace Blank;
class Sign extends Gear {
	public $data;
	private $userModel;

	public function init() {
		$this->data = new BlankObject;
		$this->gear('\Blank\Info', 'info');
		$this->userModel = $this->models->blankUsers;
		
		// die();

		// $cookie = new BlankObject;
		// $cookie->set('id', $this->cookie->get('user_id'));
		// $cookie->set('hash', $this->cookie->get('user_hash'));

		// if ($this->cookie->hasAny() && $this->userModel->checkHash($cookie)) {
		// 	$this->data = $this->getUserById($cookie->id);
		// }
		// else
		if ($this->session->has('user') && $this->userModel->checkHash($this->session->user)) {
			$this->data = $this->session->user;
		}
	}

	public function signinForm($form) {
		$this->data = $this->userModel->getUser($form->email, $form->password);

		$this->data->del('password');

		if ($this->data->get('id') !== NULL) {
			$this->session->set('user', $this->data);
			// $this->cookie->set('user_hash', $this->data-get('hash'));
			// $this->cookie->set('user_id', $this->data-get('id'));
			$this->gears->info->say('Welcome, ' . $this->data->email, 'b-green');
			$this->redirect('!accepted', TRUE);
		}
		else {
			$this->gears->info->say('Cannot login user. Check email and password.', 'b-red');
			$this->redirect('!refused');
		}
	}

	public function signupForm($form) {
		if ($this->userModel->userExists($form->email)) {
			$this->gears->info->say('User creation failed. User ' . $form->email . ' already exists.', 'b-red');
			$this->redirect('!userAlreadyExists');
		}

		if (!$this->checkPassword($form))
			$this->redirect('!incorrectPassword');

		$result = $this->userModel->addUser($form->email, $form->password);
		if ($result)
			$this->gears->info->say('User ' . $form->email . ' was successfully created.', 'b-green');
		else
			$this->gears->info->say('User with name ' . $form->email . ' could not be created.', 'b-red');
		$this->redirect('!userRegistered');
	}

	public function isLogged() {
		return $this->data->hasAny();
	}

	public function refreshUser() {
		$this->data = $this->userModel->getUserById($this->data->id);
		$this->session->set('user', $this->data);
	}

	private function checkPassword($form) {
		if (isset($form->password) && isset($form->password2) && strlen($form->password)) {

			if ($form->has('old_password') && !strlen($form->get('old_password'))) {
				$this->gears->info->say('Old password was not filled.', 'b-orange');
				return FALSE;
			}

			if ($form->has('old_password') && $this->userModel->makeHash($form->get('old_password')) != $this->data->password) {
				$this->gears->info->say('Old password does not match', 'b-red');
				return FALSE;
			}

			if(strlen($form->password) < 5) {
				$this->gears->info->say('Password must be at least 5 characters long.', 'b-red');
				return FALSE;
			}

			if($form->password != $form->password2) {
				$this->gears->info->say('Passwords do not match.', 'b-red');
				return FALSE;
			}
			return TRUE;
		}
		return NULL;
	}

	public function editUser($data) {
		$chp = $this->checkPassword($data);
		if ($chp === FALSE)
			$this->redirect();
		elseif ($chp === NULL) {
			$data->del('old_password');
			$data->del('password');
			$data->del('password2');
		}
		
		$result = $this->userModel->editUser($this->data->id, $data);
		if ($result === TRUE) {
			$this->refreshUser();
			$this->gears->info->say('User settings have been successfully edited.', 'b-green');
		}
		elseif ($result === FALSE)
			$this->gears->info->say('User settings could not be edited.', 'b-red');
		else
			$this->gears->info->say('User settings were not changed.', 'b-orange');
	}

	public function logoutHandler() {
		$this->session->del();
		$this->gears->info->say('You have been successfully logged out.', 'b-green');
		$this->redirect('!signedOut');
	}

	public function render() {}
}
?>