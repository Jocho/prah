<?php
namespace Blank;
class Info extends Gear {
	protected $info;

	public function init() {
		$this->useTemplate('blank/info', 'info');
		if (!$this->blank->session->has('info')) {
			$this->blank->session->set('info', new BlankObject);
		}

		$this->info = (is_array($this->blank->session->info)) ? $this->blank->session->info : $this->blank->session->info->toArray();
		// $this->session->set('hello', array(1, 2));
	}

	protected function process() {
		$this->template->infoList = array();
		if ($this->getGeneration() == 1) {
			if (count($this->info)) {
				$this->template->infoList = $this->info;
				$this->blank->session->set('info', new BlankObject);
			}
		}
	}

	public function render() {}

	public function say($message, $class = '') {
		if (strlen(trim($message))) {
			$this->info[] = new BlankObject(array('message' => $message, 'class' => $class));
			$this->blank->session->set('info', $this->info);
			$this->blank->session->save();
		}
	}
}
?>