<?php
namespace Blank;
class Menu extends Gear {
	public function render() {
		$this->template->activePage = (isset($this->router->path[0])) ? $this->router->path[0] : '';
		$this->template->signedIn   = $this->session->has('user');
	}
}
?>