// var app = new Later({useLocalStorage: false});
var app = new Later({useLocalStorage: true});
var menu = app.section('menu');

var mainMenu = app.section('mainMenu');

var character = app.section('character');
var game = app.section('game');


var item = app.section('item');
var talent = app.section('talent');
var skill = app.section('skill');
var dice = app.section('dice');
var offers = app.section('offers');
var npc = app.section('npc');
var npcGroup = app.section('npcGroup');

var main = (function() {
	var _m = {};
	var ATTRIBUTES = ['pow', 'dex', 'knw', 'san'];
	var tabClasses = ['one', 'two', 'three', 'four', 'five', 'six'];

	_m.init = function() {
		if (typeof menu.name === 'undefined')
			return;

		app.ajaxer.get('/characters/light', function(res) {
			var list = JSON.parse(res);
			
			if (typeof list.characterList !== 'undefined' && list.characterList instanceof Array)
				mainMenu.ctrl.set('characterList', list.characterList);
			else
				mainMenu.ctrl.restore();

			app.ajaxer.get('/games/light', function(res) {
				var list = JSON.parse(res);
				
				if (typeof list.gameList !== 'undefined' && list.gameList instanceof Array) {
					mainMenu.ctrl.set('gameList', list.gameList);
					toggleGameLock(list.gameList);
				}

				else
					mainMenu.ctrl.restore();
			});
		});

		disableContextMenu(document.querySelector('.menu .user'));
		menu.view.hide();
		character.view.hide();
		game.view.hide();

		touchGames();
		touchCharacters();
	}
	
	_m.registers = function() {
		if (typeof menu.name === 'undefined')
			return;
		// MENU
		menu.call.register('open-dice', '_', function() {
			dice.call.run('load', '_');
		});

		menu.call.register('open-offers', '_', function() {
			offers.call.run('load', '_');
		});

		menu.call.register('toggleNavigation', '_', function() {
			if (game.view.isHidden())
				toggleNavigation('character');
			else
				toggleNavigation('game');
		});

		// CHARACTERS
		mainMenu.call.register('add-char', '_', function() {
			_m.showCharacter();
		});

		mainMenu.call.register('+select-char', '_', function(x) {
			var xed = x.split('.');
			_m.showCharacter(xed[1]);
		});

		mainMenu.call.register('+delete-char', '_', function(x) {
			var xed = x.split('.');
			if (confirm('Do you want to delete character "' + mainMenu.ctrl.get('characterList.' + xed[1] + '.name') + '"?')) {
				var list = mainMenu.ctrl.get('characterList');
				var charId = list[xed[1]].id;

				app.ajaxer.delete('/characters/' + charId, function(res) {
					var result = JSON.parse(res);
					if (result.deleted) {
						list.splice(xed[1], 1);
						mainMenu.ctrl.set('characterList', list);
						mainMenu.view.refresh();
					}
				});
			}
		});

		mainMenu.call.register('+delete-game', '_', function(x) {
			var xed = x.split('.');
			if (confirm('Do you want to delete game "' + mainMenu.ctrl.get('gameList.' + xed[1] + '.title') + '"?')) {
				var list = mainMenu.ctrl.get('gameList');
				var gameId = list[xed[1]].id;

				app.ajaxer.delete('/games/' + gameId, function(res) {
					var result = JSON.parse(res);
					if (result.deleted) {
						list.splice(xed[1], 1);
						mainMenu.ctrl.set('gameList', list);
						mainMenu.view.refresh();
					}
				});
			}
		});


		// CHARACTER
		character.call.register('back', '_', function() {
			saveCharacter();
		});

		character.call.register('add-item', '_', function() {
			item.call.run('load', '_');
		});

		character.call.register('add-talent', '_', function() {
			talent.call.run('load', '_');
		});

		character.call.register('add-skill', '_', function() {
			skill.call.run('load', '_');
		});

		character.call.register('tab-select+', '_', function(x) {
			_m.tabSelect('character', x);
		});

		// ITEMS, TALENTS AND SKILLS
		character.call.register('+edit', '_', function(x) {
			var xed = x.split('.');
			var list = character.ctrl.get(xed[0]);

			switch(xed[0]) {
				case 'items':
					item.call.run('load', '_', list[xed[1]]);
					break;

				case 'talents':
					var t = list[xed[1]][xed[2]];
					talent.call.run('load', '_', t);
					break;

				case 'skills':
					skill.call.run('load', '_', list[xed[1]]);
					break;
			}
		});

		character.call.register('+details', '_', function(x) {
			var xed = x.split('.');
	
			switch(xed[0]) {
				case 'items':
					listGroup = '.items li';
					break;
				case 'talents':
					listGroup = '.talents .talent-group-' + xed[1] + ' li';
					break;
				case 'skills':
					listGroup = '.skills li';
					break;
				case 'adventure':
					listGroup = '.settings li';
					break;
			}
			
			var list = document.querySelectorAll('#character ' + listGroup);
			var listItem = (['talents', 'adventure'].indexOf(xed[0]) > -1) ? list[xed[2]] : list[xed[1]];
			var isActive = listItem.classList.contains('active');

			if (!isActive)
				listItem.classList.add('active');
			else
				listItem.classList.remove('active');
		});

		character.call.register('+remove', '_', function(x) {
			var xed = x.split('.');
			
			if (isNaN(xed[1])) {
				var listIndex = xed.slice(0, 2).join('.');
				var itemIndex = xed[2];
			}
			else {
				var listIndex = xed[0];
				itemIndex = xed[1];
			}
			
			var list = character.ctrl.get(listIndex);
			var item = list.splice(itemIndex, 1);
			
			character.ctrl.set(listIndex, list);

			app.ajaxer.delete('/characters/' + character.ctrl.get('id') + '/' + xed[0] + '/' + item[0].id, item[0]);

			switch(xed[0]) {
				case 'items':
					toggleAddItem();
					break;

				case 'talents':
					toggleMasterButton();
					toggleAddTalent();
					break;

				case 'skills':
					var xp = parseInt(character.ctrl.get('xp'));
					character.ctrl.set('xp', xp + 1);
					toggleAddSkill();
					break;
			}
		});

		character.call.register('add-xp', '_', function(x) {
			var xp = character.ctrl.get('xp');
			character.ctrl.set('xp', parseInt(xp) + 1);

			var xpHtml = document.querySelector('.add-attr-button');
			
			toggleAddSkill();

			if (character.ctrl.get('setter.active') != -1 && xpHtml.classList.contains('hidden'))
				xpHtml.classList.remove('hidden');
		});

		character.call.register('+master', '_', function(x) {
			var xed = x.split('.');
			var master = xed.slice(0, 3);
			master.push('mastered');
			master = master.join('.');
			var mastered = parseInt(character.ctrl.get(master));
			var newMaster = (mastered) ? 0 : 1;

			character.ctrl.set(master, newMaster, false);
			toggleMasterButton();
		});

		character.call.register('+equip+', '_', function(x) {
			var xed = x.split('.');
			var equipped = (xed[3] == 'true') ? 1 : 0;
			character.ctrl.set('items.' + xed[1] + '.equipped', equipped, false);
			var li = document.querySelectorAll('.items li')[xed[1]];
			
			calculateBonuses();
			app.ajaxer.put('/characters/' + character.ctrl.get('id'), character.ctrl.get());
		});

		character.call.register('+raise-amount', '_', function(x) {
			var xed = x.split('.');
			var item = character.ctrl.get('items.' + xed[1]);
			if (parseInt(item.stackable)) {
				item.amount = parseInt(item.amount) + 1;
				character.ctrl.set('items.' + xed[1] + '.amount', item.amount);
				app.ajaxer.put('/characters/' + character.ctrl.get('id'), character.ctrl.get());
			}

		});

		character.call.register('+lower-amount', '_', function(x) {
			var xed = x.split('.');
			var item = character.ctrl.get('items.' + xed[1]);
			if (parseInt(item.stackable) && item.amount > 1) {
				item.amount = parseInt(item.amount) - 1;
				character.ctrl.set('items.' + xed[1] + '.amount', item.amount);
				app.ajaxer.put('/characters/' + character.ctrl.get('id'), character.ctrl.get());
			}
		});

		character.call.register('toggleNavigation', '_', function() {
			toggleNavigation('character');
		});

		// ATTRIBUTES
		character.call.register('raise-attribute', '_', function() {
			var exp = parseInt(character.ctrl.get('xp'));
			var attr = ['attributes', character.ctrl.get('setter.active'), 'max'].join('.');
			var max = parseInt(character.ctrl.get(attr));

			if (exp > 0) {
				character.ctrl.set('xp', exp - 1);
				character.ctrl.set(attr, max + 1);
				character.ctrl.set('setter.max', max + 1);
			}
			toggleAddTalent();
			toggleAddSkill();
			toggleMasterButton();
		});

		character.call.register('+lower-attribute', '_', function(x) {
			var active = character.ctrl.get('setter.active');
			var exp = parseInt(character.ctrl.get('xp'));
			var atM = ['attributes', active, 'max'].join('.');
			var atV = ['attributes', active, 'value'].join('.');
			var max = parseInt(character.ctrl.get(atM));
			var val = parseInt(character.ctrl.get(atV));

			if (max) {
				character.ctrl.set('xp', exp + 1);
				character.ctrl.set(atM, max - 1);
				character.ctrl.set('setter.max', max - 1);

				if (val > max - 1) {
					character.ctrl.set(atV, max - 1);
					character.ctrl.set('setter.value', max - 1);
				}
			}
			toggleAddTalent();
			toggleAddSkill();
			toggleMasterButton();
		});

		character.call.register('attributes+', '_', function(x) {
			setAttributeLimit();
		});

		menu.call.register('rest+', '_', function(x) {
			var xed = x.split('.');
			var type = xed[1];
			var attrs = character.ctrl.get('attributes');

			for (var i = 0; i < 4; i++) {
				attrs[i].value = (type == 'good') ? parseInt(attrs[i].max) : parseInt(attrs[i].value) + 2;
			}

			character.ctrl.set('attributes', attrs, true);
			setAttributeLimit();
			app.ajaxer.put('/characters/' + character.ctrl.get('id'), character.ctrl.get());
		});

		character.call.register('+choose', '_', function(x) {
			var xed = x.split('.');
			var chosenAttribute = parseInt(xed[1]);
			var setter = character.ctrl.get('setter');
			var setterElement = document.querySelector('#character .navigation .setter');
			if (typeof setter.active === 'undefined')
				setter.active = -1;
			
			var maxVal = character.ctrl.get('attributes.' + chosenAttribute + '.max') < character.ctrl.get('attributes.4.value')
				? character.ctrl.get('attributes.' + chosenAttribute + '.max')
				: character.ctrl.get('attributes.4.value');
			var maxVal = (chosenAttribute != 4) ? maxVal : character.ctrl.get('attributes.4.max');

			var attributes = document.querySelectorAll('#character .attributes .attribute');
			for (var i = 0; i < attributes.length; i++)
				attributes[i].classList.remove('active');

			if (setter.active != chosenAttribute) {
				character.ctrl.set('setter', {
					value: character.ctrl.get('attributes.' + chosenAttribute + '.value'),
					max: maxVal,
					active: chosenAttribute
				}, true);
				
				document.querySelector('#character .navigation .setter').classList.remove('hidden')
				attributes[chosenAttribute].classList.add('active');

				if (parseInt(character.ctrl.get('xp')) && chosenAttribute != 4)
					document.querySelector('.add-attr-button').classList.remove('hidden');
				else
					document.querySelector('.add-attr-button').classList.add('hidden');
			}
			else {
				character.ctrl.set('setter.active', -1);
				document.querySelector('#character .navigation .setter').classList.add('hidden')				
				document.querySelector('.add-attr-button').classList.add('hidden');
			}
		});

		character.call.register('setter.value', '_', function(x) {
			var xed = x.split('.');
			var setter = character.ctrl.get('setter');
			character.ctrl.set('attributes.' + setter.active + '.value', character.ctrl.get('setter.value'));
			var elem = document.querySelectorAll('#character .attribute')[setter.active];
			app.ajaxer.put('/characters/' + character.ctrl.get('id'), character.ctrl.get());
		});

		// character.call.register('search', '_', function(x) {
		// 	character.
		// });

		// CHARACTER SETTINGS
		var searchTim = null;
		character.call.register('adventure.search', '_', function(x) {
			var searchFor = character.ctrl.get('adventure.search');

			if (searchTim != null) {
				clearTimeout(searchTim);
			}

			if (!searchFor.length)
				character.ctrl.set('adventure.list', []);

			if (searchFor.length < 3)
				return;
			
			searchTim = setTimeout(function() {
				app.ajaxer.get('/games/search/' + searchFor, function(res) {
					var result = JSON.parse(res);
					character.ctrl.set('adventure.list', result.results);
				});
			}, 1500);			
		});

		character.call.register('+adventure.join', '_', function(x) {
			var xed = x.split('.');
			var gameList = character.ctrl.get('adventure.list');

			if (!gameList.length)
				return;

			var game = gameList[xed[2]];
			
			character.ctrl.set('game_id', game.id);
			var ch = character.ctrl.get();
			app.ajaxer.put('/characters/' + ch.id, ch, function(res) {
				var result = JSON.parse(res);
				
				if (!result.saved)
					return;
				
				app.ajaxer.get('/games/' + game.id, function(res) {
					var result = JSON.parse(res);
					var game = result.game;
					character.ctrl.set('adventure', {
						id: game.id,
						title: game.title,
						description: game.description,
						ruleset: game.ruleset
					});

					app.ajaxer.get('/characters/' + ch.id, function(res) {
						var result = JSON.parse(res);
						
						if (typeof result.character !== 'undefined') {
							character.ctrl.set({
								'items': result.character.items,
								'adventure': result.character.adventure
							});

							toggleAddItem();
							calculateBonuses();
							showItemAmount();
							touchItems();
						}
					});

					toggleAdventurePage();
				});
			});
		});

		character.call.register('adventure.leave', '_', function(x) {
			var xed = x.split('.');
			var ch = character.ctrl.get();
			
			character.ctrl.set('game_id', 0);
			app.ajaxer.put('/characters/' + ch.id, ch, function(res) {
				toggleAdventurePage();

				app.ajaxer.get('/characters/' + ch.id, function(res) {
					var result = JSON.parse(res);
					if (typeof result.character !== 'undefined')
						character.ctrl.set({
							'items': result.character.items,
							'adventure': result.character.adventure
						});

						toggleAddItem();
						calculateBonuses();
				});

			});
		});

		// ITEMS
		item.call.register('load', '_', function(data) {
			if (typeof data !== 'undefined') {
				item.ctrl.set({
					id: data.id,
					index: data.index,
					setter: {active: -1, value: 0},
					name: data.name,
					attributes: data.attributes,
					text_value: data.text_value,
					description: data.description,
					price: data.price,
					amount: data.amount,
					stackable: parseInt(data.stackable),
					item_group: data.item_group
				});
			}
			else {
				item.ctrl.set({
					index: -1,
					id: 0,
					setter: {active: -1, value: 0},
					name: '',
					attributes: [
						{attr: 'pow', value: 0}, 
						{attr: 'dex', value: 0}, 
						{attr: 'knw', value: 0}, 
						{attr: 'san', value: 0}
					],
					text_value: '',
					description: '',
					price: 0,
					amount: 1,
					stackable: 0,
					equipped: 0,
					item_group: 0
				});
			}

			item.call.run('stackable', '_');

			document.getElementById('modal-item').classList.add('active');
		});

		item.call.register('stackable', '_', function() {
			if (item.ctrl.get('stackable'))
				document.querySelector('#modal-item .amount-input').classList.remove('hidden');
			else
				document.querySelector('#modal-item .amount-input').classList.add('hidden');
		});

		item.call.register('save', '_', function() {
			var data = item.ctrl.get();

			var elem, link;

			if (game.view.isHidden()) {
				elem = character;
				link = '/characters/';
			}
			else {
				elem = game;
				link = '/games/';
			}

			var list = elem.ctrl.get('items');
			var itemEdited = {
				id: data.id,
				index: data.index,
				name: data.name,
				attributes: data.attributes,
				amount: 1,
				text_value: data.text_value,
				description: data.description,
				price: data.price,
				amount: data.amount,
				stackable: parseInt(data.stackable),
				item_group: data.item_group
			};

			if (!parseInt(data.stackable))
				itemEdited.amount = 1;

			modalSaveCheck('skill', itemEdited, function(canSave) {
				if (canSave || !game.view.isHidden()) {
					if (data.index > -1) {
						itemEdited.index = data.index;
					}
					else {
						itemEdited.index = list.length;
						
						if (game.view.isHidden())
							itemEdited.equipped = 0;
					}

					itemEdited.index = (data.index > -1) ? data.index : list.length;
					elem.ctrl.set('items.' + itemEdited.index, itemEdited, true);

					if (data.index == -1) {
						_m.touchItem(document.querySelectorAll('.items li')[itemEdited.index]);
						app.ajaxer.post(link + elem.ctrl.get('id') + '/items/', itemEdited);
					}
					else {
						app.ajaxer.put(link + elem.ctrl.get('id') + '/items/' + itemEdited.id, itemEdited);
					}

					if (game.view.isHidden()) {
						toggleAddItem();
						showItemAmount();
						calculateBonuses();
					}
					
					var activeSetter = item.ctrl.get('setter.active');
					if (parseInt(activeSetter) != -1)
						item.call.run('+choose', '_', 'attributes.' + activeSetter + '.choose');

					document.getElementById('modal-item').classList.remove('active');
				}
			});
		});

		item.call.register('cancel', '_', function() {
			var activeSetter = item.ctrl.get('setter.active');
			if (parseInt(activeSetter) != -1)
				item.call.run('+choose', '_', 'attributes.' + activeSetter + '.choose');
			document.getElementById('modal-item').classList.remove('active');
		});

		item.call.register('+choose', '_', function(x) {
			var xed = x.split('.');
			var chosenAttribute = xed[1];
			var setter = item.ctrl.get('setter');
			var setterElement = document.querySelector('#modal-item .bar .setter');
			if (typeof setter.active === 'undefined')
				setter.active = -1;
			
			var attributes = document.querySelectorAll('#modal-item .attributes .attribute');
			for (var i = 0; i < attributes.length; i++) {
				attributes[i].classList.remove('active');
			}

			if (setter.active != chosenAttribute) {
				item.ctrl.set('setter', {
					value: item.ctrl.get('attributes.' + chosenAttribute + '.value'),
					active: chosenAttribute
				});
				document.querySelector('#modal-item .bar .setter').classList.remove('hidden')
				attributes[chosenAttribute].classList.add('active');
			}
			else {
				item.ctrl.set('setter.active', -1);
				document.querySelector('#modal-item .bar .setter').classList.add('hidden')
			}
		});

		item.call.register('setter.value', '_', function(x) {
			var xed = x.split('.');
			var setter = item.ctrl.get('setter');
			item.ctrl.set('attributes.' + setter.active + '.value', item.ctrl.get('setter.value'));

			document.querySelector('#modal-item .bar .setter').classList.remove('hidden');
			document.querySelectorAll('#modal-item .attributes .attribute')[setter.active].classList.add('active');
			var elem = document.querySelectorAll('#modal-item .attribute')[setter.active];
		});


		// TALENTS
		talent.call.register('load', '_', function(data) {
			if (typeof data !== 'undefined') {
				talent.ctrl.set({
					id: data.id,
					index: data.index,
					name: data.name,
					description: data.description,
					relatedTo: data.relatedTo,
					mastered: (data.mastered) ? 1 : 0
				});
				toggleAddTalent(data.relatedTo);
			}
			else {
				talent.ctrl.set({
					id: 0,
					index: -1,
					name: '',
					description: '',
					mastered: 0,
					relatedTo: -1
				});
				toggleAddTalent();
			}

			document.querySelector('#modal-talent .attributes').className = 'attributes';
			document.querySelector('#modal-talent .name').className = 'name';
			document.getElementById('modal-talent').classList.add('active');
		});

		talent.call.register('save', '_', function() {
			var data = talent.ctrl.get();
			var talentEdited = {
				id: data.id,
				name: data.name,
				description: data.description,
				mastered: (data.mastered) ? 1 : 0,
				relatedTo: data.relatedTo
			};

			modalSaveCheck('talent', talentEdited, function(canSave) {
				if (canSave) {
					var talentGroup = 'talents.' + ATTRIBUTES[data.relatedTo];
					var list = character.ctrl.get(talentGroup);
					talentEdited.index = (data.index > -1) ? data.index : list.length;
					
					character.ctrl.set([talentGroup, talentEdited.index].join('.'), talentEdited);
					toggleAddTalent();
					toggleMasterButton();

					if (data.index == -1) {
						_m.touchTalent(document.querySelectorAll('.talents .talent-group-' + ATTRIBUTES[talentEdited.relatedTo] + ' li')[talentEdited.index]);
						app.ajaxer.post('/characters/' + character.ctrl.get('id') + '/talents/', talentEdited, function(res) {
							var result = JSON.parse(res);

							if (typeof result._gears.talents !== 'undefined' && result._gears.talents.talent_id !== 'undefined')
								character.ctrl.set([talentGroup, talentEdited.index, 'id'].join('.'), result._gears.talents.talent_id);
						});
					}
					else {
						app.ajaxer.put('/characters/' + character.ctrl.get('id') + '/talents/' + talentEdited.id, talentEdited);
					}

					document.getElementById('modal-talent').classList.remove('active');
				}
			});
		});

		talent.call.register('cancel', '_', function() {
			document.getElementById('modal-talent').classList.remove('active');
		});

		talent.call.register('relateTo+', '_', function(x) {
			var xed = x.split('.');
			var attrs = ['pow', 'dex', 'knw', 'san'];
			var index = attrs.indexOf(xed[1]);

			talent.ctrl.set('relatedTo', index, false);
			highlightSelectedTalentRelation(index);
		});


		// SKILLS
		skill.call.register('load', '_', function(data) {
			if (typeof data !== 'undefined') {
				skill.ctrl.set({
					id: data.id,
					index: data.index,
					name: data.name,
					description: data.description,
					difficulty: data.difficulty
				});
			}
			else {
				skill.ctrl.set({
					id: 0,
					index: -1,
					name: '',
					description: '',
					difficulty: 1
				});
			}

			document.getElementById('modal-skill').classList.add('active');
		});

		skill.call.register('save', '_', function() {
			var data = skill.ctrl.get();
			var list = character.ctrl.get('skills');
			var skillEdited = {
				id: data.id,
				index: data.index,
				name: data.name,
				description: data.description,
				difficulty: data.difficulty
			};

			modalSaveCheck('skill', skillEdited, function(canSave) {
				if (canSave) {
					skillEdited.index = (data.index > -1) ? data.index : list.length;
					character.ctrl.set('skills.' + skillEdited.index, skillEdited);
					
					if (data.index == -1) {
						var xp = parseInt(character.ctrl.get('xp'));
						character.ctrl.set('xp', xp - 1);
						_m.touchTalent(document.querySelectorAll('.skills li')[skillEdited.index]);

						app.ajaxer.post('/characters/' + character.ctrl.get('id') + '/skills/', skillEdited, function(res) {
							var result = JSON.parse(res);

							if (typeof result._gears.skills !== 'undefined' && result._gears.skills.skill_id !== 'undefined')
								character.ctrl.set('skills.' + skillEdited.index + '.id', result._gears.skills.skill_id);
						});
					}
					else {
						app.ajaxer.put('/characters/' + character.ctrl.get('id') + '/skills/' + skillEdited.id, skillEdited, function(res) {});
					}

					toggleAddSkill();
					document.getElementById('modal-skill').classList.remove('active');
				}
			});
		});


		skill.call.register('cancel', '_', function() {
			document.getElementById('modal-skill').classList.remove('active');
		});


		// DICE
		dice.call.register('load', '_', function() {
			dice.ctrl.set({
				'result': '...',
				'sum': ''
			});

			menu.ctrl.set('diceResult', '');

			document.getElementById('modal-dice').classList.add('active');
		});

		dice.call.register('cancel', '_', function() {
			document.getElementById('modal-dice').classList.remove('active');
		});

		dice.call.register('throw+', '_', function(x) {
			var xed = x.split('.');
			var throwCount = parseInt(xed[1]);
			var result = '';
			var sum = 0;
			var throwResults = [];
			var isSuccess = false;
			var isFailure = false;

			for (var i = 0; i < throwCount; i++) {
				throwResults.push(throwDice());
			}

			result = (throwCount > 1) ? throwResults.join(' + ') + ' = ' : '';
			sum = throwResults.reduce(function(a, b) {return a + b;});

			if ((throwCount == 1 && sum == 6)
				|| (throwCount == 2 && sum >= 11)
				|| (throwCount == 3 && sum >= 16)
			) {
				isSuccess = true;
			}

			if ((throwCount == 1 && sum == 1)
				|| (throwCount == 2 && sum <= 3)
				|| (throwCount == 3 && sum <= 5)
			) {
				isFailure = true;
			}

			document.querySelector('#modal-dice .h2').className = 'h2';
			document.querySelector('#modal-dice .sum').className = 'sum';
			document.querySelector('.menu .dice-result').className = 'dice-result';

			setTimeout(function() {
				if (isSuccess) {
					document.querySelector('#modal-dice .sum').classList.add('c-green');
					document.querySelector('.menu .dice-result').classList.add('c-green');
				}

				if (isFailure) {
					document.querySelector('#modal-dice .sum').classList.add('c-red');
					document.querySelector('.menu .dice-result').classList.add('c-red');
				}

				dice.ctrl.set({
					'result': result,
					'sum': sum
				});

				menu.ctrl.set('diceResult', sum);
				document.querySelector('#modal-dice .h2').classList.add('pop');
			}, 250);
		});


		//OFFERS
		offers.call.register('load', '_', function(x) {
			var inCharacter = game.view.isHidden();
			if (inCharacter) {
				var oi = character.ctrl.get('items.' + x);
				var link = '/characters/' + character.ctrl.get('id') + '/offers';
			}
			else {
				var oi = game.ctrl.get('items.' + x);
				oi.amount = 0;
				var link = '/games/' + game.ctrl.get('id') + '/offers';
			}

			app.ajaxer.get(link, function(res) {
				var result = JSON.parse(res);
				var guild = (typeof result._gears.offers.to !== 'undefined') ? result._gears.offers.to : [];
				var max = (oi.equipped) ? oi.amount - 1 : oi.amount;

				offers.ctrl.set({
					'guild': guild,
					'id': oi.id,
					'index': oi.index,
					'name': oi.name,
					'item_group': oi.item_group,
					'max': max,
					'amount': 1
				}, true);

				if (max == 1)
					document.querySelector('#modal-offers .amount').classList.add('hidden');
				else
					document.querySelector('#modal-offers .amount').classList.remove('hidden');

				document.getElementById('modal-offers').classList.add('active');
			})

			document.querySelector('#modal-offers .amount.character').classList.add('hidden');
			document.querySelector('#modal-offers .amount.admin').classList.add('hidden');
			if (parseInt(oi.stackable)) {
				if (inCharacter)
					document.querySelector('#modal-offers .amount.character').classList.remove('hidden');
				else
					document.querySelector('#modal-offers .amount.admin').classList.remove('hidden');
			}
			else {
				document.querySelector('#modal-offers .amount.character').classList.add('hidden');
				document.querySelector('#modal-offers .amount.admin').classList.add('hidden');
			}
		});

		offers.call.register('cancel', '_', function() {
			document.getElementById('modal-offers').classList.remove('active');
		});

		offers.call.register('+give', '_', function(x) {
			if (game.view.isHidden()) {
				var charId = character.ctrl.get('id');
				var link = '/characters/' + charId + '/offers';
			}
			else {
				var charId = 0;
				var link = '/games/' + game.ctrl.get('id') + '/offers';
			}

			var xed = x.split('.');
			var o = offers.ctrl.get();
			var amount = parseInt(o.amount);
			var offer = {
				from: charId,
				to: o.guild[xed[1]].id,
				item_id: o.id,
				amount: amount,
				all: (o.max - amount == 0) ? 1 : 0
			}

			character.ctrl.set('items.' + o.index + '.amount', o.max - amount);
			document.getElementById('modal-offers').classList.remove('active');

			app.ajaxer.put(link, offer, function(res) {
				var result = JSON.parse(res);

				if (offer.all)
					character.call.run('+remove', '_', 'items.' + o.index + '.remove');

			});
		});


		// GAMES
		mainMenu.call.register('add-game', '_', function() {
			_m.showGame();
		});

		mainMenu.call.register('+manage', '_', function(x) {
			var xed = x.split('.');
			_m.showGame(xed[1]);
		});

		game.call.register('back', '_', function() {
			saveGame();
		});

		game.call.register('toggleNavigation', '_', function() {
			toggleNavigation('game');
		});

		game.call.register('tab-select+', '_', function(x) {
			_m.tabSelect('game', x);
		});

		game.call.register('+details', '_', function(x) {
			var xed = x.split('.');

			switch(xed[0]) {
				case 'items':
					listGroup = '.items li';
					break;
				case 'npcs':
					listGroup = '.npcs li';
					break;
				case 'guild':
					listGroup = '.guild li';
					break;
				case 'npcGroups':
					listGroup = '.npc-group';
					break;
			}
			
			var list = document.querySelectorAll('#game ' + listGroup);
			var listItem = list[xed[1]];
			var isActive = listItem.classList.contains('active');

			if (!isActive)
				listItem.classList.add('active');
			else {
				listItem.classList.remove('active');

				if (xed[0] == 'npcGroups') {
					var npcGroup = game.ctrl.get('npcGroups.' + xed[1]);
					app.ajaxer.put('/games/' + game.ctrl.get('id') + '/npc_groups/' + npcGroup.id, npcGroup, function(res) {
						var result = JSON.parse(res);
					});
				}
			}
		});

		game.call.register('itemSearch', '_', function() {
			provideSearch(game.ctrl.get('itemSearch'), 'items');
		});

		game.call.register('npcSearch', '_', function() {
			provideSearch(game.ctrl.get('npcSearch'), 'npcs');
		});

		game.call.register('+edit', '_', function(x) {
			var xed = x.split('.');
			var list = game.ctrl.get(xed[0]);
			switch (xed[0]) {
				case 'items':
					item.call.run('load', '_', list[xed[1]]);
					break;
				case 'npcs':
					npc.call.run('load', '_', list[xed[1]]);
					break;
			}
		});

		game.call.register('add+', '_', function(x) {
			var xed = x.split('.');

			switch (xed[1]) {
				case 'item':
					item.call.run('load', '_');
					break;
				case 'npc':
					npc.call.run('load', '_');
					break;
				default: break;
			}
			
		});

		game.call.register('+remove', '_', function(x) {
			var xed = x.split('.');
			
			if (isNaN(xed[1])) {
				var listIndex = xed.slice(0, 2).join('.');
				var itemIndex = xed[2];
			}
			else {
				var listIndex = xed[0];
				itemIndex = xed[1];
			}
			
			var list = game.ctrl.get(listIndex);
			var item = list.splice(itemIndex, 1);
			
			if (listIndex !== 'items' || confirm('Do you really want to delete ' + item[0].name + '? All item instances will be lost!')) {
				game.ctrl.set(listIndex, list);
				var subsection = (xed[0] == 'npcGroups') ? 'npc_groups' : xed[0];
				app.ajaxer.delete('/games/' + game.ctrl.get('id') + '/' + subsection + '/' + item[0].id);
			}
		});

		game.call.register('+npcHealth', '_', function(x) {
			var xed = x.split('.');
			var npcToSet = 'npcGroups.' + xed[1] + '.npcs.' + xed[3];
			var npcGroupId = game.ctrl.get('npcGroups.' + xed[1] + '.id');
			var enemy = game.ctrl.get(npcToSet);
			var vit = enemy.attributes[4].value;
			
			for (var i = 0; i < 4; i++) {
				enemy.attributes[i].value = (enemy.attributes[i].max < vit) ? enemy.attributes[i].max : vit;
			}

			game.ctrl.set(npcToSet, enemy);
		});

		game.call.register('+removeNpc', '_', function(x) {
			var xed = x.split('.');
			var npcListToChange = 'npcGroups.' + xed[1] + '.npcs';
			var npcs = game.ctrl.get(npcListToChange);

			npcs.splice(xed[3], 1);
			game.ctrl.set(npcListToChange, npcs);
		});

		// NPCS
		npc.call.register('load', '_', function(enpc) {
			// add
			if (typeof enpc === 'undefined') {
				npc.ctrl.set({
					id: 0,
					index: -1,
					setter: {active: -1, value: 0},
					name: '',
					description: '',
					attrSpan: 10,
					attributes: [
						{attr: 'pow', value: 0}, 
						{attr: 'dex', value: 0}, 
						{attr: 'knw', value: 0}, 
						{attr: 'san', value: 0},
						{attr: 'vit', value: 0, max: 0}
					],
				});
			}
			// update
			else {
				npc.ctrl.set({
					id: enpc.id,
					index: enpc.index,
					setter: {active: -1, value: 0},
					name: enpc.name,
					description: enpc.description,
					attributes: enpc.attributes,
				});
				calculateAttrSpan();
			}
			document.querySelector('#modal-npc').classList.add('active');
		});

		npc.call.register('+choose', '_', function(x) {
			var xed = x.split('.');
			var chosenAttribute = xed[1];

			if (chosenAttribute == 4)
				return;

			var setter = npc.ctrl.get('setter');
			var setterElement = document.querySelector('#modal-npc .bar .setter');
			if (typeof setter.active === 'undefined')
				setter.active = -1;
			
			var attributes = document.querySelectorAll('#modal-npc .attributes .attribute');
			for (var i = 0; i < attributes.length; i++) {
				attributes[i].classList.remove('active');
			}

			if (setter.active != chosenAttribute) {
				npc.ctrl.set('setter', {
					value: npc.ctrl.get('attributes.' + chosenAttribute + '.value'),
					active: chosenAttribute
				});
				document.querySelector('#modal-npc .bar .setter').classList.remove('hidden')
				attributes[chosenAttribute].classList.add('active');
			}
			else {
				npc.ctrl.set('setter.active', -1);
				document.querySelector('#modal-npc .bar .setter').classList.add('hidden')
			}
		});

		npc.call.register('setter.value', '_', function(x) {
			var xed = x.split('.');
			var setter = npc.ctrl.get('setter');
			npc.ctrl.set('attributes.' + setter.active + '.value', npc.ctrl.get('setter.value'));

			document.querySelector('#modal-npc .bar .setter').classList.remove('hidden');
			document.querySelectorAll('#modal-npc .attributes .attribute')[setter.active].classList.add('active');
			var elem = document.querySelectorAll('#modal-npc .attribute')[setter.active];
		});

		npc.call.register('setter.value', 'setNpcVitality', function(x) {
			var attrs = npc.ctrl.get('attributes');
			var maxAttrValue = 0;
			for (var i = 0; i < 4; i++) {
				if (attrs[i].value > maxAttrValue)
					maxAttrValue = attrs[i].value;
			}

			npc.ctrl.set({
				'attributes.4.value': maxAttrValue,
				'attributes.4.max': maxAttrValue
			});

			calculateAttrSpan();
		});

		npc.call.register('save', '_', function() {
			var data = npc.ctrl.get();
			var list = game.ctrl.get('npcs');
			var npcEdited = {
				id: data.id,
				index: data.index,
				name: data.name,
				attributes: data.attributes,
				description: data.description
			};

			modalSaveCheck('npc', npcEdited, function(canSave) {
				if (canSave) {
					if (data.index > -1) {
						npcEdited.index = data.index;
					}
					else {
						npcEdited.index = list.length;
					}

					npcEdited.index = (data.index > -1) ? data.index : list.length;
					game.ctrl.set('npcs.' + npcEdited.index, npcEdited, true);

					if (data.index == -1) {
						_m.touchNpc(document.querySelectorAll('.npcs li')[npcEdited.index]);
						app.ajaxer.post('/games/' + game.ctrl.get('id') + '/npcs/', npcEdited, function(res) {
							var result = JSON.parse(res);
							if (typeof result._gears.npcs !== 'undefined' && result._gears.npcs.npc_id !== 'undefined') {
								game.ctrl.set('npcs.' + npcEdited.index + '.id', result._gears.npcs.npc_id);
							}
						});
					}
					else {
						app.ajaxer.put('/games/' + game.ctrl.get('id') + '/npcs/' + npcEdited.id, npcEdited);
					}

					var activeSetter = npc.ctrl.get('setter.active');
					if (parseInt(activeSetter) != -1)
						npc.call.run('+choose', '_', 'attributes.' + activeSetter + '.choose');

					document.getElementById('modal-npc').classList.remove('active');
				}
			});
		});

		npc.call.register('cancel', '_', function() {
			var activeSetter = npc.ctrl.get('setter.active');
			if (parseInt(activeSetter) != -1)
				npc.call.run('+choose', '_', 'attributes.' + activeSetter + '.choose');
			document.querySelector('#modal-npc').classList.remove('active');
		});

		npc.call.register('randomize-attrs', '_', function() {
			var attrSpan = parseInt(npc.ctrl.get('attrSpan'));
			var attrVals = [0, 0, 0, 0];

			for (var i = 0; i < 3; i++) {
				var rand = (attrSpan > 0) ? 1 + Math.floor(Math.random() * attrSpan) : 0;
				attrVals[i] = rand;
				attrSpan -= rand;
			}
			attrVals[3] = attrSpan;
			attrVals = shuffleArray(attrVals);

			for (var i = 0; i < attrVals.length; i++) {
				npc.ctrl.set('attributes.' + i + '.value', attrVals[i]);
			}
		});

		// npc groups
		npcGroup.call.register('load', '_', function(x) {
			var xed = x.split('.');
			var groups = game.ctrl.get('npcGroups');
			var nextIndex = groups.length;

			npcGroup.ctrl.set({
				selectedNpc: game.ctrl.get('npcs.' + xed[1]),
				npcGroups: game.ctrl.get('npcGroups')
			});

			document.getElementById('modal-npc-group').classList.add('active');
		});

		npcGroup.call.register('cancel', '_', function() {
			npcGroup.ctrl.set({
				selectedNpc: {},
				title: ''
			});
			document.getElementById('modal-npc-group').classList.remove('active');
		});

		npcGroup.call.register('add', '_', function() {
			npcGroup.call.run('+add', '_', 'add');
		});

		npcGroup.call.register('+add', '_', function(x) {
			var xed = x.split('.');
			var npcgData = npcGroup.ctrl.get();
			var selectedNpc = npcgData.selectedNpc;

			for (var i = 0; i < selectedNpc.attributes.length; i++) {
				selectedNpc.attributes[i].max = selectedNpc.attributes[i].value;
			}
			
			if (xed.length == 1) {
				var nextIndex = npcgData.npcGroups.length;
				var nextNpcGroup = {
					id: 0, 
					index: nextIndex, 
					title: npcgData.title, 
					npcs: [selectedNpc]
				};
				
				npcgData.npcGroups.push(nextNpcGroup);
				game.ctrl.set('npcGroups', npcgData.npcGroups);
				_m.touchNpcGroup(document.querySelectorAll('.npc-groups li')[nextIndex]);

				app.ajaxer.post('/games/' + game.ctrl.get('id') + '/npc_groups', nextNpcGroup, function(res) {
					var result = JSON.parse(res);
					if (typeof result._gears.npcGroups !== 'undefined' && result._gears.npcGroups.npc_group_id !== 'undefined')
						game.ctrl.set('npcGroups.' + nextIndex + '.id', result._gears.npcGroups.npc_group_id);
				});
			}
			else {
				var npcListToChange = 'npcGroups.' + xed[1] + '.npcs';
				var npcList = game.ctrl.get(npcListToChange);
				npcList.push(selectedNpc);
				game.ctrl.set(npcListToChange, npcList);
				app.ajaxer.put('/games/' + game.ctrl.get('id') + '/npc_groups', nextNpcGroup, function(res) {});
			}
			npcGroup.call.run('cancel', '_');
		});
	};

	var calculateAttrSpan = function() {
		var attrs = npc.ctrl.get('attributes');
		var attrSpan = 0;
		for (var i = 0; i < 4; i++) {
			attrSpan += parseInt(attrs[i].value);
		}

		npc.ctrl.set('attrSpan', attrSpan);
	}

	var shuffleArray = function(array) {
		for (var i = array.length - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var temp = array[i];
			array[i] = array[j];
			array[j] = temp;
		}
		return array;
	}


	var throwDice = function() {
		return Math.floor(1 + Math.random() * 6);
	}

	var modalSaveCheck = function(type, data, callback) {
		var modal = document.querySelector('#modal-' + type);
		var nameElement = modal.querySelector('.name');
		var isTalent = typeof data.relatedTo !== 'undefined';

		nameElement.className = 'name';

		if (isTalent) {
			var talentElement = modal.querySelector('.attributes');
			talentElement.className = 'attributes';
		}

		setTimeout(function() {
			var result = true;
			if (!data.name.length) {
				nameElement.classList.add('flash');
				result = false;
			}

			if (isTalent && data.relatedTo == -1) {
				talentElement.classList.add('flash');
				result = false;
			}
			callback(result);
		}, 100);
	}

	var setAttributeLimit = function() {
		var attrs = character.ctrl.get('attributes');

		for (var i = 0; i < 4; i++) {
			if (attrs[i].value > attrs[i].max)
				attrs[i].value = attrs[i].max;

			if (attrs[i].max > attrs[4].max)
				attrs[4].max = attrs[i].max;

			if (attrs[i].value > attrs[4].value)
				attrs[i].value = attrs[4].value;
		}

		character.ctrl.set('attributes', attrs, true);
	};

	var highlightSelectedTalentRelation = function(index) {
		var relates = document.getElementsByClassName('relate_to');
		for (var i = 0; i < relates.length; i++) {
			if (i == index)
				relates[i].classList.add('selected');
			else
				relates[i].classList.remove('selected');
		}
	}

	var getTalentCounts = function() {
		var talents = character.ctrl.get('talents');
		var talentCounts = [0, 0, 0, 0];

		for (var i in talents) {
			if (talents.hasOwnProperty(i)) {
				for (var j in talents[i]) {
					if (talents[i].hasOwnProperty(j) && typeof talentCounts[talents[i][j].relatedTo] !== 'undefined')
						talentCounts[talents[i][j].relatedTo]++;
				}
			}
		}
		
		return talentCounts;
	}

	var toggleAddTalent = function(allowed) {
		allowed = typeof allowed !== 'undefined' ? allowed : -1;
		var can = [0, 0, 0, 0];
		var talentCounts = getTalentCounts();
		var attributes = character.ctrl.get('attributes');

		for (var i = 0; i < 4; i++) {
			can[i] = (Math.floor(attributes[i].max / 2) - talentCounts[i] > 0) ? true : false;
		}

		var showAddTalent = can.reduce(function(x, y) {
			if (y)
				x++;
			return x;
		}, 0);

		if (typeof can[allowed] !== 'undefined')
			can[allowed] = true;

		highlightSelectedTalentRelation(allowed);

		if (showAddTalent) {
			document.querySelector('.talents .add').classList.remove('hidden');
		}
		else {
			document.querySelector('.talents .add').classList.add('hidden');

			var modal = document.getElementById('modal-talent').classList;
			if (modal.contains('active'))
				modal.remove('active');
		}

		var options = document.querySelectorAll('#modal-talent .relate_to');

		for (var i = 0; i < options.length; i++) {
			if (can[i]) {
				options[i].classList.remove('disabled');
				options[i].disabled = false;
			}
			else {
				options[i].classList.add('disabled');
				options[i].disabled = true;
			}
		}

		return can;
	};

	var toggleAddItem = function() {
		var items = character.ctrl.get('items');
		var gameId = character.ctrl.get('game_id');

		if (gameId && items.length < 10)
			document.querySelector('.items .add').classList.remove('hidden');
		else
			document.querySelector('.items .add').classList.add('hidden');
	};

	var toggleAddSkill = function() {
		var xp = parseInt(character.ctrl.get('xp'));

		if (xp) {
			document.querySelector('.skills .add').classList.remove('hidden');
		}
		else {
			document.querySelector('.skills .add').classList.add('hidden');
		}
	};

	var toggleGameLock = function(list) {
		if (typeof list === 'undefined')
			list = mainMenu.ctrl.get('gameList');

		var gamesHtml = document.querySelectorAll('#games li');

		for (var i = 0; i < list.length; i++) {
			gamesHtml[i].querySelector('i').className = (parseInt(list[i].open)) ? 'fa fa-unlock-alt' : 'fa fa-lock';
		}
	}

	var characterMasterIndexes = function() {
		var talents = character.ctrl.get('talents');
		for (var i in talents) {
			if (!talents.hasOwnProperty(i))
				continue;

			for (var j = 0; j < talents[i].length; j++) {
				if (parseInt(talents[i][j].mastered))
					return [i, j];
			}
		}
		return [-1, -1];
	}

	var characterCanMaster = function() {
		var attributesAllow = [false, false, false, false];
		var attributes = character.ctrl.get('attributes');
		for (var i = 0; i < 4; i++) {
			if (attributes[i].max >= 6) {
				attributesAllow[i] = true;
			}
		}

		return attributesAllow;
	}

	var toggleMasterButton = function(x) {
		var masterButtons = document.querySelectorAll('.talents .mastered');
		var talents = character.ctrl.get('talents');
		var attributes = character.ctrl.get('attributes');
		var masterIndexes = characterMasterIndexes();
		var canMaster = characterCanMaster();
		var idx = 0;
		var jdx = 0;

		var z = masterIndexes.reduce(function(x, y) {
			return x + y;
		}, 0);

		var isMaster = (z == -2) ? false : true;

		for (var i in talents) {
			if (!talents.hasOwnProperty(i))
				continue;

			var groupMasterable = canMaster[idx];

			for (var j = 0, jj = talents[i].length; j < jj; j++) {
				var talentMastered = app.compare(masterIndexes, [i, j]);
				
				masterButtons[jdx].classList.add('hidden');

				if ((groupMasterable && !isMaster) || talentMastered)
					masterButtons[jdx].classList.remove('hidden');
				
				masterButtons[jdx].innerHTML = (talentMastered) ? "&#9733;" : "&#9734";
				jdx++;
			}
			idx++;
		}
	};

	var toggleAdventurePage = function() {
		if (character.ctrl.get('id') != 0) {
			var search = document.querySelector('.settings .search');
			var adventure = document.querySelector('.settings .adventure');
			var isInAdventure = parseInt(character.ctrl.get('game_id'));

			if (isInAdventure) {
				search.classList.add('hidden');
				adventure.classList.remove('hidden');
			}
			else {
				search.classList.remove('hidden');
				adventure.classList.add('hidden');
			}

			document.querySelector('.char-saved').classList.remove('hidden');
			document.querySelector('.char-unsaved').classList.add('hidden');
		}
		else {
			document.querySelector('.char-saved').classList.add('hidden');
			document.querySelector('.char-unsaved').classList.remove('hidden');
		}
	}

	var clearedCharacter = function() {
		return {
			id: 0,
			game_id: 0,
			index: -1,
			name: '',
			xp: 10,
			attributes: [
				{attr: 'pow', value: 0, max: 0, bonus: 0},
				{attr: 'dex', value: 0, max: 0, bonus: 0},
				{attr: 'knw', value: 0, max: 0, bonus: 0},
				{attr: 'san', value: 0, max: 0, bonus: 0},
				{attr: 'vit', value: 0, max: 0, bonus:0}
			],
			notes: '',
			description: '',
			goal: '',
			fear: '',
			items: [],
			talents: {'pow': [], 'dex': [], 'knw': [], 'san': []},
			skills: [],
			adventure: {
				id: 0,
				title: '',
				description: '',
				ruleset: '',
				search: '',
				guild: [],
				list: []
			},
			offers: []
		};
	}

	var getTime = function() {
		var d = new Date();
		return Math.round(d.getTime() / 1000);
	}

	var clearedGame = function() {
		return {
			index: -1,
			triumphs: 0,
			notes: "",
			itemSearch: '',
			title: '',
			description: '',
			ruleset: 'Prah Fantasy',
			open: 0,
			id: 0,
			created: getTime(),
			tstamp: getTime(),
			guild: [],
			npcGroups: {},
			npcs: [],
			items: []
		}
	}

	_m.showCharacter = function(selectedCharacter) {
		showLoad(true, function() {
			selectedCharacter = (typeof selectedCharacter !== 'undefined') ? parseInt(selectedCharacter) : -1;
			mainMenu.view.hide();
			menu.view.show('block');
			character.view.show('block');

			if (selectedCharacter != -1) {
				var selectedId = mainMenu.ctrl.get('characterList.' + selectedCharacter + '.id');
				app.ajaxer.get('/characters/' + selectedId, function(res) {
					var result = JSON.parse(res);
					
					if (typeof result.character !== 'undefined') {
						character.ctrl.set(result.character);
						character.ctrl.set('setter.active', -1);
						character.ctrl.set('index', selectedCharacter);
						showCharacterInit();
					}
					else {
						getToMenu();
					}
				});
			}
			else {
				character.ctrl.set(clearedCharacter());
				showCharacterInit();
			}
		});
	};

	_m.showGame = function(selectedGame) {
		showLoad(true, function() {
			selectedGame = (typeof selectedGame !== 'undefined') ? parseInt(selectedGame) : -1;
			mainMenu.view.hide();
			character.view.hide();
			
			menu.view.show('block');
			game.view.show('block');

			if (selectedGame != -1) {
				var selectedId = mainMenu.ctrl.get('gameList.' + selectedGame + '.id');
				app.ajaxer.get('/games/' + selectedId, function(res) {
					var result = JSON.parse(res);
					
					if (typeof result.game !== 'undefined') {
						game.ctrl.set(result.game);
						game.ctrl.set('index', selectedGame);
						game.ctrl.set('tstamp', getTime());
						showGameInit();
					}
					else {
						getToMenu();
					}
				});

				game.ctrl.set(mainMenu.ctrl.get('gameList.' + selectedGame));
			}
			else {
				game.ctrl.set(clearedGame());
				showGameInit();
			}
		});
	};

	var showCharacterInit = function() {
		mainMenu.ctrl.set('diceResult', '');

		document.querySelector('.tab-nav a').click();

		toggleAddTalent();
		setAttributeLimit();
		toggleAddItem();
		toggleAddSkill();
		calculateBonuses();
		showItemAmount();
		toggleMasterButton();
		toggleAdventurePage();

		touchItems();
		touchTalents();
		touchSkills();
		touchPagers();

		refresher(true);
		showRest(true);
		showLoad(false);
	};

	var showGameInit = function() {
		mainMenu.ctrl.set('diceResult', '');
		touchItems();
		touchPagers();
		touchNpcs();
		touchNpcGroups();

		refresher(true);
		showRest(false);
		showLoad(false);
	};

	var refreshers = {};
	var refresher = function(start) {
		// return;
		start = start || false;

		if (start) {
			if (!game.view.isHidden()) {
				refreshers.guild = setInterval(refreshGuild, 10000);
			}
			refreshers.itemTimeout = setTimeout(function() {
				refreshers.items = setInterval(refreshItems, 10000);
			}, 5000);
		}
		else {
			clearInterval(refreshers.guild);
			clearInterval(refreshers.items);
			clearTimeout(refreshers.itemTimeout);
		}
	}

	var refreshGuild = function() {
		app.ajaxer.get('/games/' + game.ctrl.get('id') + '/characters', function(res) {
			var result = JSON.parse(res);
			if (typeof result.guild === 'undefined')
				return;

			var guild = game.ctrl.get('guild');
			
			for (var k in result.guild) {
				var found = false;
				if (!result.guild.hasOwnProperty(k))
					continue;

				for (var i = 0; i < guild.length; i++) {
					if (result.guild[k].id == guild[i].id) {
						guild[i] = result.guild[k];
						found = true;
						break;
					}
				}
				if (!found) {
					guild.push(result.guild[k]);
				}
			}
			game.ctrl.set('guild', guild, true);
			
		});
	};

	var refreshItems = function() {
		if (game.view.isHidden()) {
			var sect = character;
			var link = '/characters/';
		}
		else {
			var sect = game;
			var link = '/games/';
		}

		var id = sect.ctrl.get('id');

		app.ajaxer.get(link + id + '/items', function(res) {
			var result = JSON.parse(res);
			if (typeof result._gears !== 'undefined' 
				&& typeof result._gears.items !== 'undefined'
				&& typeof result._gears.items.itemList !== 'undefined'
			) {
				var items = (game.view.isHidden()) ? character.ctrl.get('items') : game.ctrl.get('items');
				var identical = false;

				if (items.length == result._gears.items.itemList.length) {
					var identical = true;
					for (var i = 0, ii = result._gears.items.itemList.length; i < ii; i++) {
						identical = app.compare(items[i], result._gears.items.itemList[i]);
						if (!identical)
							break;
					}
				}
				
				if (!identical) {
					if (game.view.isHidden()) {
						character.ctrl.set('items', result._gears.items.itemList);
						calculateBonuses();
						showItemAmount();
					}
					else
						game.ctrl.set('items', result._gears.items.itemList);

					var items = document.querySelectorAll('.tab.items li');
					for (var i = 0, ii = items.length; i < ii; i++) {
						if (typeof items[i]._hasTouchEvents === 'undefined')
							_m.touchItem(items[i]);
					}
				}
			}
		});
	}

	var saveCharacter = function() {
		var ch = character.ctrl.get();
		var index = parseInt(ch.index);

		showLoad(true);
		refresher(false);

		if (!ch.name.length) {
			getToMenu();
			return;
		}

		if (index > -1) {
			app.ajaxer.put('/characters/' + ch.id, ch, function(res) {
				character.ctrl.set(clearedCharacter());
				mainMenu.ctrl.set('characterList.' + index, {
					id: ch.id,
					name: ch.name,
					title: ch.adventure.title
				});
				getToMenu();
			});

		}
		else {
			var charList = mainMenu.ctrl.get('characterList');
			var saveIndex = (typeof charList !== 'undefined') ? charList.length : 0;

			ch.index = saveIndex;

			app.ajaxer.post('/characters/', ch, function(res) {
				var result = JSON.parse(res);
				ch.id = result.created;
				mainMenu.ctrl.set('characterList.' + saveIndex, {
					id: ch.id,
					name: ch.name,
					title: ch.adventure.title,
				});

				character.ctrl.set(clearedCharacter());
				getToMenu();

				var li = document.querySelectorAll('#characters li')[saveIndex];
				_m.touchCharacter(li);
			});
		}
	};


	var saveGame = function() {
		game.ctrl.set('item-search', '');

		var gm = game.ctrl.get();
		var index = parseInt(gm.index);

		showLoad(true);
		refresher(false);

		if (!gm.title.length) {
			getToMenu();
			return;
		}

		if (!gm.open)
			gm.open = 0;

		if (index > -1) {
			app.ajaxer.put('/games/' + gm.id, gm, function(res) {
				game.ctrl.set(clearedCharacter());
				mainMenu.ctrl.set('gameList.' + index, {
					id: gm.id,
					title: gm.title,
					description: gm.description
				});
				getToMenu();
			});

		}
		else {
			var gameList = mainMenu.ctrl.get('gameList');
			var saveIndex = (typeof gameList !== 'undefined') ? gameList.length : 0;

			gm.index = saveIndex;

			app.ajaxer.post('/games/', gm, function(res) {
				var result = JSON.parse(res);
				gm.id = result.created;
				mainMenu.ctrl.set('gameList.' + saveIndex, {
					id: gm.id,
					title: gm.title,
					description: gm.description
				});

				var li = document.querySelectorAll('#games li')[saveIndex];
				_m.touchGame(li);

				game.ctrl.set(clearedCharacter());
				getToMenu();
			})
		}

		getToMenu();
	};

	var getToMenu = function() {
		mainMenu.view.show('block');
		menu.call.run('toggleNavigation', '_');
		character.view.hide();
		game.view.hide();
		menu.view.hide();
		mainMenu.view.refresh();
		removePagers();
		toggleGameLock();
		showLoad(false);
	};

	_m.tabSelect = function(section, x) {
		var xed = x.split('.');
		var elem = document.getElementById(section);
		var tabs = elem.querySelectorAll('.tab-nav a');
		var tabsHTML = elem.getElementsByClassName('tabs')[0];

		for (var i = 0; i < tabs.length; i++) {
			tabs[i].classList.remove('active');
			tabsHTML.classList.remove(tabClasses[i]);
		}
		
		tabs[xed[1]].classList.add('active');
		tabsHTML.classList.add(tabClasses[xed[1]]);
		if (typeof xed[2] === 'undefined' || xed[2] != 'false')
			menu.call.run('toggleNavigation', '_');
	}

	var toggleNavigation = function(x) {
		if (x == 'character') {
			var chosenAttr = character.ctrl.get('setter.active');
			if (chosenAttr > -1) {
				character.call.run('+choose', '_', 'attributes.' + chosenAttr + '.choose');
			}
		}

		var elem = document.getElementById(x);
		var nav = elem.querySelector('.navigation');
		var bck = elem.querySelector('.navigation-toggler');
		
		if (nav.classList.contains('active')) {
			nav.classList.remove('active');
			bck.classList.remove('active');
		}
		else {
			nav.classList.add('active');
			bck.classList.add('active');
		}
	};

	var provideSearch = function(searchFor, searchSection) {
		var items = game.ctrl.get(searchSection);
		var itemHtmls = document.querySelectorAll('#game .' + searchSection + ' li');
		var regexp = new RegExp(searchFor, 'gi');

		for (var i = 0, ii = items.length; i < ii; i++) {
			if (!searchFor.length || items[i].name.match(regexp))
				itemHtmls[i].classList.remove('hidden');
			else
				itemHtmls[i].classList.add('hidden');
		}
	};

	var showRest = function(show) {
		show = show || false;
		var dropdown = document.querySelector('.menu .dropdown');

		if (show)
			dropdown.classList.remove('hidden');
		else
			dropdown.classList.add('hidden');
	}

	var getActiveTabId = function() {
		var tabs = document.getElementsByClassName('tabs')[0];
		var classes = ['one', 'two', 'three', 'four', 'five', 'six'];

		var active = 0;
		for (var i = 0; i < classes.length; i++) {
			if (tabs.classList.contains(classes[i])) {
				active = i;
				break;
			}
		}
		return parseInt(active);
	};

	var showItemAmount = function() {
		if (game.view.isHidden()) {
			var div = 'character';
			var elem = character;

		}
		else {
			var div = 'game';
			var elem = game;
		}
		
		var items = elem.ctrl.get('items');
		var itemsHtml = document.querySelectorAll('#' + div + ' .items li');

		if (!itemsHtml.length)
			return;

		for (var i = 0; i < items.length; i++) {
			var amounts = itemsHtml[i].querySelectorAll('.amount-value, .amount');
			if (!parseInt(items[i].stackable)) {
				for (var j = 0; j < amounts.length; j++)
					amounts[j].classList.add('hidden');
			}
			else {
				for (var j = 0; j < amounts.length; j++)
					amounts[j].classList.remove('hidden');
			}
		}
	}

	var calculateBonuses = function() {
		var items = character.ctrl.get('items');
		var itemsHtml = document.querySelectorAll('.items li');
		var attrsHtml = document.querySelectorAll('#character .attributes .attribute');
		var bonuses = [0, 0, 0, 0];
		for (var i = 0; i < items.length; i++) {
			if (items[i].equipped) {
				itemsHtml[i].classList.add('equipped');
				for (var j = 0; j < items[i].attributes.length; j++) {
					bonuses[j] += parseInt(items[i].attributes[j].value);
				}
			}
		}

		var attributes = character.ctrl.get('attributes');

		for (var i = 0; i < bonuses.length; i++) {
			if (attributes[i].bonus != bonuses[i])
				_m.flash(attrsHtml[i]);

			attributes[i].bonus = bonuses[i];
		}
		character.ctrl.set('attributes', attributes, true);
	};

	_m.flash = function(elem) {
		elem.classList.remove('flash');
		setTimeout(function() {
			elem.classList.add('flash');
		}, 100);
	}

	_m.bounce = function(elem, direction) {
		elem.classList.remove('bounce','lft', 'rgt');
		setTimeout(function() {
			elem.classList.add('bounce');
			elem.classList.add(direction);
		}, 100);
	}

	_m.touchItem = function(elem) {
		disableContextMenu(elem);
		var gii = character.view.getIterationIndexes(elem);
		var hmmr = new Hammer(elem);
		
		if (game.view.isHidden()) {
			hmmr.on('swipeleft', function(z) {
				_m.bounce(z.target, 'lft');
				character.call.run('+equip+', '_', gii + '.equip.true');
				elem.classList.add('equipped');
			});

			hmmr.on('swiperight', function(z) {
				_m.bounce(z.target, 'rgt');
				var item = character.ctrl.get(gii);
				if (item.equipped) {
					character.call.run('+equip+', '_', gii + '.equip.false');
					elem.classList.remove('equipped');
				}
				else if (confirm('Do you want to drop ' + item.name + '?')) {
					character.call.run('+remove', '_', gii);
				}
			});
		}
		else {
			hmmr.on('swiperight', function(z) {
				_m.bounce(z.target, 'rgt');
				var item = game.ctrl.get(gii);
				if (confirm('Do you want to drop ' + item.name + '?')) {
					game.call.run('+remove', '_', gii);
				}
			});
		}

		hmmr.on('press', function(z) {
			disableContextMenu(z.target);
			var xed = gii.split('.');
			offers.call.run('load', '_', xed[1]);
		});

		elem._hasTouchEvents = true;
	}

	_m.touchTalent = function(elem) {
		var gii = character.view.getIterationIndexes(elem);
		var hmmr = new Hammer(elem);
		
		hmmr.on('swiperight', function(z) {
			_m.bounce(z.target, 'rgt');
			var item = character.ctrl.get(gii);
			if (confirm('Do you want to unlearn ' + item.name + '?')) {
				character.call.run('+remove', '_', gii);
			}
		});

		disableContextMenu(elem);
	}

	_m.touchCharacter = function(elem) {
		var gii = mainMenu.view.getIterationIndexes(elem);
		var hmmr = new Hammer(elem);
		
		hmmr.on('swiperight', function(z) {
			_m.bounce(z.target, 'rgt');

			var item = mainMenu.ctrl.get(gii);
			mainMenu.call.run('+delete-char', '_', gii);
		});

		disableContextMenu(elem);
	}

	_m.touchGame = function(elem) {
		var gii = mainMenu.view.getIterationIndexes(elem);
		var hmmr = new Hammer(elem);
		
		hmmr.on('swiperight', function(z) {
			_m.bounce(z.target, 'rgt');
			var item = mainMenu.ctrl.get(gii);
			mainMenu.call.run('+delete-game', '_', gii);
		});

		disableContextMenu(elem);
	}

	_m.touchNpc = function(elem) {
		var gii = game.view.getIterationIndexes(elem);
		var hmmr = new Hammer(elem);
		
		hmmr.on('swiperight', function(z) {
			_m.bounce(z.target, 'rgt');
			var npc = game.ctrl.get(gii);
			if (confirm('Do you want to delete ' + npc.name + '?')) {
				game.call.run('+remove', '_', gii);
			}
		});

		hmmr.on('press', function(z) {
			var xed = gii.split('.');
			npcGroup.call.run('load', '_', gii);
		});

		disableContextMenu(elem);
	}

	_m.touchNpcGroup = function(elem) {
		var gii = game.view.getIterationIndexes(elem);
		var hmmr = new Hammer(elem);
		
		hmmr.on('swiperight', function(z) {
			_m.bounce(z.target, 'rgt');
			var npcGroup = game.ctrl.get(gii);
			if (confirm('Do you want to delete "' + npcGroup.title + '" NPC group?')) {
				game.call.run('+remove', '_', gii);
			}
		});
	}

	var touchGames = function() {
		var lis = document.querySelectorAll('#games li');

		for (var i = 0; i < lis.length; i++)
			_m.touchGame(lis[i]);
	}

	var touchCharacters = function() {
		var lis = document.querySelectorAll('#characters li');

		for (var i = 0; i < lis.length; i++)
			_m.touchCharacter(lis[i]);
	}

	var touchItems = function() {
		var type = (game.view.isHidden()) ? 'character' : 'game';
		var lis = document.querySelectorAll('#' + type + ' .tab.items li');
		
		for (var i = 0; i < lis.length; i++) {
			_m.touchItem(lis[i]);
		}
	};

	var touchNpcs = function() {
		var lis = document.querySelectorAll('.tab .npcs li');

		for (var i = 0; i < lis.length; i++) {
			_m.touchNpc(lis[i]);
		}
	}

	var touchNpcGroups = function() {
		var lis = document.querySelectorAll('.tab .npc-groups .npc-group');

		for (var i = 0; i < lis.length; i++) {
			_m.touchNpcGroup(lis[i]);
		}
	}

	var touchTalents = function() {
		var lis = document.querySelectorAll('.tab.talents li');
		
		for (var i = 0; i < lis.length; i++) {
			_m.touchTalent(lis[i]);
		}
	};

	var touchSkills = function() {
		var lis = document.querySelectorAll('.tab.skills li');
		
		for (var i = 0; i < lis.length; i++) {
			_m.touchTalent(lis[i]);
		}
	};

	var touchPagers = function() {
		if (game.view.isHidden()) {
			var root = document.getElementById('character');
			var elem = character;
		}
		else {
			var root = document.getElementById('game');
			var elem = game;
		}

		var leftPager = document.createElement('div');
		leftPager.setAttribute('class', 'pager l');
		var rightPager = document.createElement('div');
		rightPager.setAttribute('class', 'pager r');

		root.appendChild(leftPager);
		root.appendChild(rightPager);
		
		var list = root.querySelectorAll('.tab-nav a');
		var pl = root.querySelector('.pager.l');
		var pr = root.querySelector('.pager.r');
		var hmmrL = new Hammer(pl);
		var hmmrR = new Hammer(pr);

		hmmrL.on('swiperight', function(z) {
			var result = calculateNextTabIndex(list, false);

			if (result[0]) {
				elem.call.run('toggleNavigation', '_');
			}
			elem.call.run('tab-select+', '_', result[1]);
		});

		hmmrR.on('swipeleft', function(z) {
			var result = calculateNextTabIndex(list, true);
			elem.call.run('tab-select+', '_', result[1]);
		});
	}

	var removePagers = function() {
		var blocks = [document.getElementById('character'), document.getElementById('game')];

		for (var i = 0; i < blocks.length; i++) {
			if (blocks[i].querySelector('.pager')) {
				blocks[i].removeChild(blocks[i].querySelector('.pager.l'));
				blocks[i].removeChild(blocks[i].querySelector('.pager.r'));
			}
		}

		return;
	}

	var calculateNextTabIndex = function(list, gotoRight) {
		var selectedTab = 0;
		var openMenu = false;
		for (var i = 0; i < list.length - 1; i++) {
			if (list[i].classList.contains('active')) {
				selectedTab = i;
				break;
			}
		}

		if (gotoRight) {
			selectedTab = (selectedTab + 1 == list.length - 1) ? selectedTab : selectedTab + 1;
		}
		else {
			openMenu = (!selectedTab) ? true : false;
			selectedTab = (!selectedTab) ? selectedTab : selectedTab - 1;
		}

		return [openMenu, 'tab-select.' + selectedTab + '.false'];
	}

	var disableContextMenu = function(elem) {
		elem.oncontextmenu = function(event) {
			event.preventDefault();
			event.stopPropagation();
			return false;
		};
	};

	var showLoad = function(show, callback) {
		show = show || false;
		var bars = document.querySelectorAll('.loading-bar');
		for (var i = 0; i < bars.length; i++) {
			if (show)
				bars[i].classList.remove('loaded');
			else
				bars[i].classList.add('loaded');
		}

		if (typeof callback === 'function') {
			setTimeout(function() {
				callback();
			}, 1000);
		}
	}

	_m.load = function() {
		_m.init();
		_m.registers();

		showLoad(false);
	}

	return _m;
})();


autosize(document.querySelectorAll('.tab textarea'));