/**
 * A simple modal tool that handles... well... modal windows.
 * The tool has multiple customizable values, from showing close button or preseting close actions
 * to the initial modal dimensions.
 *
 * @author Marek J. Kolcun
 * @version 0.1.4
 * @since 2015-05-04
 */
var SimpleModal = (function(setup) {
	var information = {
		version: '0.1.3',
		author: 'Marek J. Kolcun'
	};

	// Initial settings of the every modal. Can be changed via .setup() method
	var settings = {
		autoShow: true,
		autoHide: false,
		autoResize: true,
		classes: '',
		showExitButton: true,
		exitByBackground: true,
		exitByKeyboard: true,
		width: 'auto',
		height: 'auto',
		toggleSpeed: 250
	};

	var content = {};
	var visible = false;
	var myId = 0;
	var myModal = null;
	var _sm = {};

	/**
	 * Returns an "information" object with info about the current version of this tool.
	 * @return object
	 */
	_sm.info = function() {
		console.log(information);
	}

	/**
	 * Sets up the inner settings object.
	 *
	 * @param setup (object) - an object with keys that are existing in inner settings object and their new values
	 * @return void
	 */
	_sm.setup = function(setup) {
		if (typeof setup === 'undefined')
			return settings;
		
		for (var i in settings) {
			if (settings.hasOwnProperty(i) && typeof setup[i] !== 'undefined') {
				settings[i] = setup[i];
			}
		}

		if (typeof _sm.resize !== 'undefined')
			_sm.resize();
	};

	_sm.settings = function() {
		return settings;
	}

	// if there is object setup inserted inside class constructor, the .setup() method will be run immediately
	if (typeof setup !== 'undefined')
		_sm.setup(setup);

	/**
	 * Initial method that creates all the neccessary dom elements and appends them into HTML body.
	 * Every modal has its own unique generated random identifier, so there can co-exist multiple modal objects. 
	 *
	 * @param content (mixed) - this argument can contain whether a complete HTMLElement or string.
	 						  - if HTMLElement is inserted, it will be appended into simple-modal-content as is
	 						  - if general string is inserted, it will be set as innerHTML content of simple-modal-content
	 * @param setup (object) - object containing data that will be sent to the .setup() method and inner settings object
	 * @return void
	 */
	_sm.init = function(content, setup) {
		if (typeof setup === 'object')
			_sm.setup(setup);
		
		var classes = ['simple-modal'];
		if (settings.classes.length)
			classes.push(settings.classes);

		if (this.myId)
			delete(document.getElementById('simple_modal_' + this.myId));
		this.myId = Math.floor(Math.random() * 10000);

		var modal = document.createElement('div');
		modal.setAttribute('class', classes.join(' '));
		modal.setAttribute('id', 'simple_modal_' + this.myId);
		modal.style.display = 'block';

		var modalWindow = document.createElement('div');
		modalWindow.classList.add('simple-modal-window');
		modalWindow.style.width = settings.width;
		modalWindow.style.height = settings.height;

		var modalContent = document.createElement('div');
		modalContent.classList.add('simple-modal-content');

		myModal = modal;
		
		if (['HTMLElement', 'object'].indexOf(typeof content) != -1)
			modalContent.appendChild(content);
		else
			modalContent.innerHTML = content;
	
		if (settings.exitByBackground)
			modal.click(function(e) {
				_sm.hide(e, false);
			});
		
		if (settings.showExitButton) {
			var exit = document.createElement('a')
			exit.innerHTML = '&times;';
			exit.setAttribute('href', 'javascript:void(0);');
			exit.classList.add('simple-modal-exit');
			exit.addEventListener('click', function(e) {
				_sm.hide(e);
			});
			modalWindow.appendChild(exit);
		}

		if (settings.exitByBackground)
			modal.addEventListener('click', function(e) {
				_sm.hide(e, false);
			});

		if (settings.exitByKeyboard) {
			document.addEventListener('keydown', function(e) {
				_sm.hide(e, false);
			});
		}
		
		modalWindow.appendChild(modalContent);
		modal.appendChild(modalWindow);

		document.getElementsByTagName('body')[0].appendChild(modal);
		
		if (settings.autoResize)
			_sm.resize();
		
		if (settings.autoShow) {
			setTimeout(function() {
				_sm.show();
			}, 10);
		}
		else
			modal.style.display = 'none';
	};

	/**
	 * An inner method that animates modal opacity when toggled to show or hide itself.
	 * 
	 * @param show (boolean) - sets whether an animation is going to show or hide the modal. If true, modal will show.
	 * @param duration (int) - time in miliseconds that sets up how long will animation last
	 * @return void
	 */
	var toggleAnimation = function(show, duration) {
		duration = (typeof duration !== 'undefined') ? duration : settings.toggleSpeed;
		var speed = duration / 100;
		var process = (show) ? 0: duration;

		myModal.style.opacity = process / duration;

		myModal.style.display = 'block';
		var interval = setInterval(function() {
			myModal.style.opacity = process / duration;
			process += (show) ? speed : -speed;
			var processed = (show) ? process >= duration : process <= 0;
			if (processed) {
				clearInterval(interval);
				myModal.style.opacity = (show) ? 1 : 0;
				myModal.style.display = (show) ? 'block' : 'none';
			}
		}, speed);
	};

	/** 
	 * Shows the modal window.
	 *
	 * @param duration (int) - time in miliseconds, how long will show last
	 * @return void
	 */
	_sm.show = function(duration) {
		visible = true;
		toggleAnimation(true, duration);

		if (settings.autoHide !== false) {
			setTimeout(function() {
				_sm.hide(null, true);
			}, settings.autoHide);
		}
	};
	
	/**
	 * Hides the modal.
	 *
	 * @param e (event) - an event object whose various values are going to be inspected.
	 *					- there can be inspected pressed key value (looking for ESC key code - 27)
	 *					- or there can be a click and target will be whether exit button or modal background itself
	 * @param direct (boolean) - when true, a modal will be closed immediately
	 * @return void
	 */
	_sm.hide = function(e, direct) {
		direct = (typeof direct !== 'undefined') ? direct : true;

		if (myModal.style.display != 'none' && (direct || e.target == myModal || e.keyCode == 27)) {
			visible = false;
			toggleAnimation(false);
		}
	};


	/**
	 * When called, will resize the modal and centers its position based on its actual dimensions.
	 * @return void
	 */
	_sm.resize = function() {
		if (myModal == null)
			return;

		var modalWindow = myModal.children[0];
		var windowWidth = window.innerWidth;
		var windowHeight = window.innerHeight;

		var widthParsed = settings.width.match(/(\d+)(\D+)?/);
		var heightParsed = settings.height.match(/(\d+)(\D+)?/);

		if (settings.width != 'auto' && widthParsed != null) {
			modalWidth = (typeof widthParsed[2] !== 'undefined' && widthParsed[2] == '%') 
			? (parseFloat(widthParsed) / 100) * windowWidth
			: parseFloat(settings.width);
		}
		else
			modalWidth = modalWindow.clientWidth;
		
		if (settings.height != 'auto' && heightParsed != null) {
			modalHeight = (typeof heightParsed[2] !== 'undefined' && heightParsed[2] == '%') 
			? (parseFloat(heightParsed) / 100) * windowHeight
			: parseFloat(settings.height);
		}
		else
			modalHeight = modalWindow.clientHeight;

		var paddingLeft = parseFloat(modalWindow.clientLeft);
		var paddingTop = parseFloat(modalWindow.clientTop);
		
		if (settings.height === 'auto' && modalHeight > windowHeight)
			modalHeight = windowHeight * 0.9;
		
		if (settings.width === 'auto' && modalWidth > windowWidth)
			modalWidth = windowWidth * 0.89;
		
		topOffset  = -1 * (modalHeight + 2 * paddingTop) / 2;
		leftOffset = -1 * (modalWidth + 2 * paddingLeft) / 2;
		
		var sizes = {
			'width': modalWidth + 'px',
			'height': modalHeight + 'px'
		}
		
		var margins = {
			'margin-top': topOffset + 'px',
			'margin-left': leftOffset + 'px'
		};
		
		for (var i in sizes) {
			if (sizes.hasOwnProperty(i)) {
				modalWindow.style[i] = sizes[i];
			}
		}

		for (var i in margins) {
			if (margins.hasOwnProperty(i)) {
				modalWindow.style[i] = margins[i];
			}
		}
	};

	/**
	 * Returns a current value of variable isVisible, that keeps information whether modal is visible or not.
	 * @return void
	 */
	_sm.isVisible = function() {
		return visible;
	};

	/**
	 * Sets a content to an existing modal given via attribute.
	 * @param newContent (HTMLElement|node) - content to be inserted into modal
	 */
	_sm.content = function(newContent) {
		var modal = document.getElementById('simple_modal_' + this.myId);
		var content = modal.querySelector('.simple-modal-content');
		if (typeof newContent === 'object' && newContent instanceof HTMLElement) {
			while (content.firstChild)
				content.removeChild(content.firstChild);
			content.appendChild(newContent);
		}
		else {
			content.innerHTML = newContent;
		}
		_sm.resize();
	};

	return _sm;
});