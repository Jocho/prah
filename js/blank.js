var Blank = {
	previewArticle: function(me) {
		var header = $('<h2 />');
		var perex = $('<p />');
		var text = $('<div />');

		var articleId = $(me).siblings('input[name="article_id"]').val();
		$.get('/admin/editor', {'do': 'preview', 'id': articleId}, function(res) {
			resource = JSON.parse(res);
			header.text(resource.header);
			perex.text(resource.perex);
			text.html(resource.text);
			$('#preview .body').html("").append(header).append(perex).append(text);
			window.location.href = '/admin/editor/' + articleId + '#preview';
		});
		return false;
	},

	closeModal: function() {
		$('.modal').click(function(e) {
			if (e.target !== this || $(this).attr('id') == 'loader')
				return;
			var address = window.location.href;
			address = address.replace(/#\S+$/, '#');
			window.location.href = address;
		});
	},

	confirmDelete: function(item) {
		$('.deleteButton').click(function() {
			return confirm('Do you want to delete it?', 'Yes', 'No');
		});
	},

	showLoader: function(item) {
		$("#loader").target;
	},

	listFilesForUpload: function(item) {
		var files = $(item).get(0).files;
		var list = $(item).parent().siblings('.upload_list');

		$(list).slideUp(500);

		setTimeout(function() {
			$(list).html("");
			$.each(files, function(i) {
				$(list).append('<li>' + files[i].name + ' (' + this.formatBytes(files[i].size) + ')' + '</li>');
			});
			$(list).slideDown(500);
		}, 500);
	},

	formatBytes: function(value) {
		var sizes = ['B', 'kB', 'MB', 'GB', 'TB', 'PB'];
		var i = 0;
		while (value > 1024 && i < sizes.length) {
			value = value / 1024;
			i++;
		}
		return (Math.round(value * 100) / 100) + sizes[i];
	},

	previewFile: function(me) {
		var address = $(me).data('source');

		if (this.fileIsImage(address)) {
			var image = $('<img />').attr('src', address).attr('title', $(me).text());
			var title = $('<h3 />').text($(me).text());

			$("#previewFile .body").html(image);
			$("#previewFile .footer").html(title);
		}
	},

	fileIsImage: function(name) {
		var parsedName = name.split('.');
		var extensionList = ['jpg', 'bmp', 'png', 'gif', 'tiff'];
		var result = false;
		if (parsedName.length > 1) {
			var extension = new RegExp(parsedName.pop(), 'i');
			$.each(extensionList, function(i) {
				if (extensionList[i].match(extension)) {
					result = true;
					return false;
				}
			});
		}
		return result;
	}, 

	cloneTarget: function(target, before, callback) {
		before = (typeof before !== 'undefined') ? before : false;
		filled = (typeof filled !== 'undefined') ? filled : false;

		if ($(target).length) {
			var clone = $(target).clone();
			if (before)
				$(target).before(clone);
			else
				$(target).after(clone);
			if (typeof callback === 'function') {
				(callback)();
			}
		}
		return false;
	}, 

	deleteTarget: function(me, target, callback) {
		$target = $(target);
		if ($target.length > 1) {
			$(me).parents(target).hide('slow', function() {
				this.remove();
				if (typeof callback === 'function') {
					(callback)();
				}
			});
		}
		return false;
	}, 

	exclusiveSelectOptions: function(targets) {
		var targets         = $(targets);
		var selectedOptions = [];

		$.each(targets, function(i) {
			selectedOptions.push($(this).val());
		});

		console.log(selectedOptions);

		$.each(targets, function(i) {
			var target  = $(this);
			var options = $(this).children('option');

			options.removeClass('hidden');

			$.each(options, function(j) {
				var option = $(this);
				
				if (
					// option.val() != target.val() && 
					selectedOptions.indexOf(option.val()) != -1
				) {
					option.addClass('hidden');
				}
			});
		});
	},

	saveLastHash: function() {
		$(document).ready(function() {
			$('body').data('last-hashes', '["' + location.hash + '"]');
		});
		$('a[href^="#"]').click(function(e) {
			var lastHashes = JSON.parse($('body').data('last-hashes'));
			
			setTimeout(function() {
				var newHash = location.hash;

				if (newHash.length) {
					var found = false;
					for (var i = 0; i < lastHashes.length; i++) {
						if (lastHashes[i] == newHash) {
							found = i;
						}
					}
					if (found === false) {
						lastHashes.push(newHash);
						if (lastHashes.length > 2)
							lastHashes.shift();
					}
					else if (found === 0)
						lastHashes.reverse();
					$('body').data('last-hashes', JSON.stringify(lastHashes));
				}
				else {
					location.hash = (lastHashes.length == 2) ? lastHashes[0] : '#';
				}

			}, 100);
		});
	},

	toggleTarget: function(me, toggleClass) {
		toggleClass = (typeof toggleClass !== 'undefined') ? toggleClass : 'toggled';
		var dataTarget = $(me).data('target');
		if ($(dataTarget).hasClass(toggleClass))
			$(dataTarget).removeClass(toggleClass);
		else
			$(dataTarget).addClass(toggleClass);
	}
}



var Shower = {
	controllerElementSelector: 'blankjs',

	setControllerElementSelector: function(selector) {
		var selector = (typeof selector === 'string') ? selector : 'blankjs';
		this.controllerElementSelector = selector;
	},

	init: function() {
		var controllerElementList = document.getElementsByClassName(this.controllerElementSelector);

		console.log(controllerElementList[0]);

		var controllerList = {};
		for (i in controllerElementList) {
			var elem = new Element(controllerElementList[i]);
			controllerList[elem.getAttribute('data-controller')] = elem;
		}

		console.log(controllerList);
	}
}