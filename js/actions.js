var actions = (function() {
	_a = {};
	_a.modalSections = {};
	_a.talents = (function() {
		var _t = {};
		
		var getAvailables = function(forceAdd) {
			forceAdd = forceAdd || '';
			var list = attributes.ctrl.get('list');
			var availables = {};
			for (var i in list) {
				if (list.hasOwnProperty(i) && (list[i].canAdd || forceAdd == i)) {
					availables[i] = list[i];
				}
			}

			return availables;
		};

		_t.selectTalent = function(value) {
			$('.talent-relation').removeClass('selected');
			$('.talent-relation[value="' + value + '"]').addClass('selected');
		};

		_t.checkMasterAdd = function() {
			var attrs = attributes.ctrl.get('list');
			var groups = talents.ctrl.get('groups');
			var mastered = 0;

			for (var i in groups) {
				if (groups.hasOwnProperty(i)) {
					var jj = groups[i].list.length;

					attrs[i].canAdd = jj * 2 + 1 < parseInt(attrs[i].max);

					for (var j = 0; j < jj; j++) {
						if (groups[i].list[j].mastered)
							mastered++;
					}
				}
			}

			for (var i in groups) {
				if (groups.hasOwnProperty(i)) {
					attrs[i].canMaster = groups[i].list.length >= 6 && !mastered;
				}
			}
			attributes.ctrl.set('list', attrs, false);

			_t.toggleAddButton();
		};
		
		_t.clearModal = function(forceAdd) {
			forceAdd = forceAdd || '';
			_a.modalSections.talent.ctrl.set({
				index: '',
				talent: '',
				related_to: '',
				availables: getAvailables(forceAdd)
			});
		};

		_t.toggleAddButton = function() {
			var ga = getAvailables();
			var canAdd = false;
			for (var i in ga)
				if (ga.hasOwnProperty(i)) {
					canAdd = true;
					break;
				}

			var showAdd = (canAdd) ? '' : 'hidden';
			talents.ctrl.set('showAdd', showAdd);
		}

		_t.init = function() {
			_a.modalSections.talent = app.register('modal-talent');
			_t.clearModal();
			_a.modalSections.talent.ctrl.set({'availables': {}});
			
			_a.modalSections.talent.call.register('+related_to', '_', function(x) {
				var xed = x.split('.');
				_a.modalSections.talent.ctrl.set('related_to', xed[1]);
				_t.selectTalent(xed[1]);
			});

			_a.modalSections.talent.call.register('saveTalent', '_', function() {
				var data = _a.modalSections.talent.ctrl.get();
				var list = talents.ctrl.get('groups.' + data.related_to + '.list');
				var index = data.index.length ? data.index : list.length;
				var toJoin = ['groups', data.related_to, 'list', index, 'talent'];
				
				talents.ctrl.set(toJoin.join('.'), data.talent);
				_t.checkMasterAdd();
				modals.talent.hide();
			});
		};

		_t.init();

		return _t;
	})();

	_a.skills = (function() {
		var _s = {};

		_s.toggleAddButton = function() {
			var xp = parseInt(main.ctrl.get('character.xp'));
			var showAdd = (xp) ? '' : 'hidden';
			skills.ctrl.set('showAdd', showAdd);
		};

		_s.clearModal = function() {
			_a.modalSections.skill.ctrl.set({
				index: '',
				name: '',
				description: ''
			});
		};

		_s.init = function() {
			_a.modalSections.skill = app.register('modal-skill');
			_s.clearModal();
			_a.modalSections.skill.call.register('saveSkill', '_', function() {
				var data = _a.modalSections.skill.ctrl.get();
				var list = skills.ctrl.get('skills');
				var index = data.index.length ? data.index : list.length;
				var toJoin = ['skills', index];
				
				skills.ctrl.set(toJoin.join('.'), {
					name: data.name,
					description: data.description,
					character_id: main.ctrl.get('character.id')
				});

				if (!data.index.length) {
					var xp = parseInt(main.ctrl.get('character.xp'));
					main.ctrl.set('character.xp', xp - 1);
				}
				_s.toggleAddButton();
				modals.skill.hide();
			});
		};

		_s.init();
		
		return _s;
	})();

	_a.items = (function() {
		var _i = {};

		var getIdentifier = function() {
			return Math.round(new Date().getTime() / 1000);
		};

		_i.clearModal = function() {
			_a.modalSections.item.ctrl.set({
				index: -1,
				id: 0,
				name: '',
				text_value: '',
				numeric_values: {
					pow: {attr: 'pow', value: 0},
					dex: {attr: 'dex', value: 0},
					knw: {attr: 'knw', value: 0},
					san: {attr: 'san', value: 0}
				},
				description: '',
				group: '',
				price: 0,
				stackable: 0,
				identifier: getIdentifier()
			});
		};

		_i.checkInventoryLimit = function() {
			var items = inventory.ctrl.get('items');
			return items.length < 10;
		};

		_i.init = function() {
			_a.modalSections.item = app.register('modal-item');
			_i.clearModal();
			
			_a.modalSections.item.call.register('saveItem', '_', function() {
				var modItem = _a.modalSections.item.ctrl.get();
				var list = inventory.ctrl.get('items');

				var index = modItem.index > -1 ? modItem.index : list.length;
				var toJoin = ['items', index];
				
				inventory.ctrl.set(toJoin.join('.'), modItem);
				actions.items.rebindItemTouchEvents(index);
				modals.item.hide();
			});
		};

		var recalculateAttributeBonuses = function() {
			var items = inventory.ctrl.get('items');
			var attrs = attributes.ctrl.get('list');

			var bonuses = {
				pow: 0,
				dex: 0,
				knw: 0,
				san: 0
			};

			for (var i in items) {
				if (items.hasOwnProperty(i) && items[i].used) {
					for (var j in bonuses) {
						if (bonuses.hasOwnProperty(j)) {
							bonuses[j] += parseInt(items[i]['numeric_values'][j]['value']);
						}
					}
				}
			}

			for (var i in attrs) {
				if (attrs.hasOwnProperty(i) && ['pow', 'dex', 'knw', 'san'].indexOf(i) > -1) {
					attrs[i].bonus = bonuses[i];
				}
			}
			
			attributes.ctrl.set('list', attrs, false);
			_a.attributes.refreshMain();
			attributes.view.refresh();
		};

		_i.rebindItemTouchEvents = function(index) {
			index = index || -1;
			console.log('tada');
			var items = inventory.ctrl.get('items');

			recalculateAttributeBonuses();

			$('.item').each(function(i) {
				if (index > -1 && i != index)
					return;

				if (items[i].used)
					$(this).addClass('equipped');

				var me = this;
				var h = new Hammer($(this)[0], {velocity: 0.5, treshold: 50});
				
				h.on('tap', function() {
					var hadClass = $(me).hasClass('active');
					$('.item').removeClass('active');
					if (!hadClass)
						$(me).addClass('active');
				});

				h.on('swipeleft', function() {
					var hadClass = $(me).hasClass('active');
					inventory.call.run('+equip', '_', [i, hadClass]);
				});

				h.on('swiperight', function() {
					var hadClass = $(me).hasClass('active');
					inventory.call.run('+unequip', '_', [i, hadClass]);
				});
				
				h.on('press', function() {
					console.log('pressed!');
				});


			});
		};

		_i.init();

		return _i;
	})();

	_a.attributes = (function() {
		var _attrs = {};

		_attrs.refreshMain = function() {
			main.ctrl.set('character.attributes', attributes.ctrl.get('list'));
		};

		return _attrs;
	})();

	return _a;
})();