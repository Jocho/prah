<?php
namespace k6rpg\database;
class Games extends \Blank\Database {
	public $id;
	public $title;
	public $description;
	public $ruleset;
	public $notes;
	public $open;
	public $created;
	public $tstamp;
	public $user_id;

	public function getByGM($id) {
		return $this->select('*')->where('user_id', $id)->run();
	}

	public function getLightByGM($id) {
		return $this->select('id, title, description, open')->where('user_id', $id)->run();
	}

	public function getById($id) {
		return $this->select('*')->where('id', $id)->run('object');
		
	}


	public function getByTitle($title) {
		return $this->getOpen($title);
	}

	public function addGame($data) {
		$insert = $this->arrayFiller($data->toArray());
		return $this->insert($insert)->run();
	}

	public function setGame($data) {
		$update = $this->arrayFiller($data->toArray());
		return $this->update($update)->where('id', $data->id)->run();
	}

	public function delGame($gameId, $userId) {
		$charList = $this->table('characters')->select('id')->where('game_id', $gameId)->run('array');
		$charIds  = array();
		foreach ($charList as $char)
			$charIds[] = $char->id;

		$results = array(
			$this->table('characters')->update(array('game_id' => 0))->where('game_id', $gameId)->run(),
			$this->table('characters_x_items')->delete(array('character_id' => $charIds))->run(),
			$this->table('items')->delete('game_id', $gameId)->run(),
			$this->table('games')->delete('id', $gameId)->run()
		);
		return count($results) == count(array_filter($results));
	}

	protected function arrayFiller($data) {
		$vars = array('title', 'description', 'ruleset', 'triumphs', 'notes', 'open', 'created', 'tstamp', 'user_id');
		$res  = array();
		foreach ($vars as $var) {
			if (array_key_exists($var, $data))
				$res[$var] = $data[$var];
		}
		if (!isset($data['tstamp']))
			$res['tstamp'] = time();

		return $res;
	}

	public function getOpen($title = NULL) {
		$where = array(
			'open' => 1
		);
		
		if (strlen ($title))
			$where['title %'] = '%' . $title . '%';

		return $this
			->select('games.*, email')
			->join('users', 'games.user_id = users.id')
			->where($where)
			->run();
	}

	public function hasPlayerGame($userId, $gameId) {
		$result = $this->select('id')->where(array('user_id' => $userId, 'id' => $gameId))->run();
		return count($result) ? TRUE : FALSE;
	}
}
?>