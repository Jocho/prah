<?php
namespace k6rpg\database;
class Npcs extends Skills {
	protected $columns = array('id', 'name', 'pow', 'dex', 'knw', 'san', 'vit', 'description', 'game_id');
	public $table = 'npcs';

	public function getByGroup($groupId, $gameId = NULL) {
		$npcs = $this->select('npcs.*, npc_groups.name AS group_name')
			->join('npc_groups', 'npcs.group_id = npc_groups.id')
			->where(array('npc.group_id' => $groupId, 'npcs.game_id' => $gameId, 'npc_groups.game_id' => $gameId))
			->order('npc_groups.name ASC, npcs.name ASC')
			->run();

		return $npcs;
	}

	public function getByGame($gameId) {
		$npcs = $this->select('*')->where('game_id', $gameId)->order('name ASC')->run('array');

		foreach ($npcs as $k => $npc) {
			$npcs[$k]->attributes = array(
				array('attr' => 'pow', 'value' => $npc->pow),
				array('attr' => 'dex', 'value' => $npc->dex),
				array('attr' => 'knw', 'value' => $npc->knw),
				array('attr' => 'san', 'value' => $npc->san),
				array('attr' => 'vit', 'value' => $npc->vit)
			);
			unset($npcs[$k]->pow, $npcs[$k]->dex, $npcs[$k]->knw, $npcs[$k]->san, $npcs[$k]->vit);
		}
		return $npcs;
	}

	public function gameHasNpc($gameId, $npcId) {
		$result = $this->select('id')->where(array(
			'game_id' => $gameId, 
			'id'      => $npcId
		))->run();

		return count($result)? TRUE : FALSE;
	}

	protected function arrayFiller($data) {
		$x = $data;
		foreach ($data['attributes'] as $k => $v) {
			$x[$v['attr']] = $v['value'];
		}
		unset($x['attributes']);

		return parent::arrayFiller($x);
	}
}
