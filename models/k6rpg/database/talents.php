<?
namespace k6rpg\database;
class Talents extends Skills {
	protected $columns = array('id', 'character_id', 'name', 'description', 'related_to', 'mastered');
	public $table      = 'talents';

	public function charHasTalent($characterId, $talentId) {
		$result = $this->table('talents')->select('*')->where(array('character_id' => $characterId, 'id' => $talentId))->run();
		return count($result) ? TRUE : FALSE;
	}
}
?>