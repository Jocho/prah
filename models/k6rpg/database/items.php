<?php
namespace k6rpg\database;
class Items extends Skills {
	protected $columns = array('id', 'name', 'pow', 'dex', 'knw', 'san', 'text_value', 'description', 'price', 'game_id', 'stackable', 'item_group');
	public $table = 'items';

	public function getById($itemId) {
		$result = $this->select('*')->where('id', $itemId)->run();
		$item   = reset($result);
		return $item;
	}

	public function getByCharacter($characterId, $gameId = NULL) {
		$items = $this->select('items.*, cxi.amount, cxi.equipped')
			->join('characters_x_items AS cxi', 'cxi.item_id = items.id')
			->where(array('cxi.character_id' => $characterId, 'items.game_id' => $gameId))
			->order('items.item_group ASC, items.name ASC, cxi.equipped DESC')
			->run();

		foreach ($items as $k => $item)
			$items[$k] = $this->groupAttributeValues($item);
		return $items;
	}

	public function getByGame($gameId) {
		$items = $this->select('*')->query('WHERE game_id = ' . $gameId)->order('item_group ASC, name ASC')->run();
		foreach ($items as $k => $item)
			$items[$k] = $this->groupAttributeValues($item);
		return $items;
	}

	private function groupAttributeValues($data) {
		$numVals = array('pow' => 0, 'dex' => 0, 'knw' => 0, 'san' => 0);
		foreach ($numVals as $key => $value) {
			$numVals[] = array(
				'attr'  => $key,
				'value' => $data->$key
			);
			unset($data->$key, $numVals[$key]);
		}
		$data->price      = intval($data->price);
		$data->attributes = $numVals;
		
		if (isset($data->equipped)) {
			$data->equipped = intval($data->equipped);
			$data->amount   = intval($data->amount);
		}

		return $data;
	}

	protected function arrayFiller($data) {
		$x = $data;
		foreach ($data['attributes'] as $k => $v) {
			$x[$v['attr']] = $v['value'];
		}
		$x['stackable'] = intval($x['stackable']);
		unset($x['attributes']);

		return parent::arrayFiller($x);
	}

	public function add($data) {
		$result = parent::add($data);

		if ($result && $data->character_id) {
			$this->table('characters_x_items')->insert(array(
				'item_id'      => $result,
				'character_id' => $data->character_id,
				'amount'       => $data->amount,
				'equipped'     => $data->equipped
			))->run();
		}
		return $result;
	}

	public function set($data) {
		$updates   = array();
		$update    = $this->arrayFiller($data->toArrayRecursive());
		$updates[] = (count($update)) ? $this->update($update)->where('id', $data->id)->run() : NULL;

		$xUpdate = array();
		if ($data->has('equipped')) {
			$xUpdate['equipped'] = intval($data->equipped);
		}

		if ($data->has('amount')) {
			$xUpdate['amount'] = intval($data->amount);
		}

		if ($data->character_id)
			$updates[] = (count($xUpdate)) ? $this->table('characters_x_items')->update($xUpdate)->where(array('character_id' => $data->character_id, 'item_id' => $data->id))->run() : NULL;
		
		return count($updates) == count(array_filter($updates));
	}

	public function del($itemId) {
		$this->table('items')->delete('id', $itemId)->run();
		$this->table('characters_x_items')->delete('item_id', $itemId)->run();

		return parent::del($itemId);
	}

	public function drop($characterId, $itemId) {
		if ($this->charHasItem($characterId, $itemId)) {
			return $this->table('characters_x_items')->delete(array('character_id' => $characterId, 'item_id' => $itemId))->run();
		}
		return NULL;
	}

	public function charHasItem($characterId, $itemId) {
		$result = $this->table('characters_x_items')->select('*')->where(array('character_id' => $characterId, 'item_id' => $itemId))->run();
		return count($result) ? TRUE : FALSE;
	}

	public function give($offer) {
		$hasItem = $this->charHasItem($offer->to, $offer->item_id);
		
		if ($offer->all) {
			if ($hasItem) {
				$this->table('characters_x_items')->update(array('amount +=' => $offer->amount))->where(array(
					'character_id' => $offer->to, 
					'item_id'      => $offer->item_id
				))->run();

				$this->table('characters_x_items')->delete(array('character_id' => $offer->from, 'item_id' => $offer->item_id))->run();
			}
			else {
				return $this->table('characters_x_items')->update(array('character_id' => $offer->to))->where(array(
					'character_id' => $offer->from,
					'item_id'      => $offer->item_id
				))->run();
			}
		}
		else {
			if ($hasItem) {
				$this->table('characters_x_items')->update(array('amount +=' => $offer->amount))->where(array(
					'character_id' => $offer->to, 
					'item_id'      => $offer->item_id
				))->run();
			}
			else {
				$this->table('characters_x_items')->insert(array(
					'character_id' => $offer->to,
					'item_id'      => $offer->item_id,
					'amount'       => $offer->amount,
					'equipped'     => 0
				))->run();
			}

			return $this->table('characters_x_items')->update(array('amount -=' => $offer->amount))->where(array(
				'character_id' => $offer->from, 
				'item_id'      => $offer->item_id
			))->run();
		}
	}
}
