<?
namespace k6rpg\database;
class Skills extends \Blank\Database {
	protected $columns = array('id', 'name', 'description', 'difficulty', 'character_id');
	public $table      = 'skills';

	public function add($data) {
		$insert = $this->arrayFiller($data->toArrayRecursive());
		return $this->insert($insert)->run('object');
	}

	public function getById($id) {
		return $this->select('*')->where('id', $id)->run('object');
	}

	public function getByCharacter($characterId) {
		return $this->select('*')->where('character_id', $characterId)->run();
	}

	public function set($data) {
		$update = $this->arrayFiller($data->toArrayRecursive());
		return $this->update($update)->where('id', $data->id)->run();
	}

	public function del($id) {
		return $this->delete('id', $id)->run();
	}

	public function delAll($characterId) {
		return $this->delete('character_id', $characterId)->run();
	}

	protected function arrayFiller($data) {
		$res = array();
		foreach ($this->columns as $var) {
			if (array_key_exists($var, $data))
				$res[$var] = $data[$var];
		}
		return $res;
	}

	public function charHasSkill($characterId, $skillId) {
		$result = $this->table('skills')->select('*')->where(array('character_id' => $characterId, 'id' => $skillId))->run();
		return count($result) ? TRUE : FALSE;
	}
}
?>