<?php
namespace k6rpg\database;
class NpcGroups extends Skills {
	protected $columns = array('id', 'title', 'description', 'game_id');
	public $table = 'npc_groups';

	public function add($data) {
		$dr = $data->toArrayRecursive();
		$insert = $this->arrayFiller($data->toArrayRecursive());
		$result = $this->insert($insert)->run();

		if (!$result)
			return 0;

		$this->setNpcs($dr['npcs'], $result);
		return $result;
	}

	private function setNpcs($npcs, $npcGroupId) {
		foreach ($npcs as $npc) {
			$this->table('npcs_x_npc_groups')->insert(array(
				'npc_group_id' => $npcGroupId,
				'npc_id'       => $npc['id'],
				'vit'          => $npc['attributes'][4]['value']
			))->run();
		}
	}

	private function getNpcs($npcGroupId) {
		$npcs = $this->table('npcs_x_npc_groups as x')->select('npcs.*, x.vit as max')->join('npcs', 'x.npc_id = npcs.id')->where('npc_group_id', $npcGroupId)->run();
		
		if (!count($npcs))
			return array();

		foreach ($npcs as $k => $npc) {
			$npc->index = $k;
			$npc->attributes = array(
				array('attr' => 'pow', 'value' => ($npc->max > $npc->pow) ? $npc->pow : $npc->max, 'max' => $npc->pow),
				array('attr' => 'dex', 'value' => ($npc->max > $npc->dex) ? $npc->dex : $npc->max, 'max' => $npc->dex),
				array('attr' => 'knw', 'value' => ($npc->max > $npc->knw) ? $npc->knw : $npc->max, 'max' => $npc->knw),
				array('attr' => 'san', 'value' => ($npc->max > $npc->san) ? $npc->san : $npc->max, 'max' => $npc->san),
				array('attr' => 'vit', 'value' => $npc->max, 'max' => $npc->vit)
			);
			unset($npc->pow, $npc->dex, $npc->knw, $npc->san, $npc->vit, $npc->max);
			$npcs[$k] = $npc;
		}
		return $npcs;
	}

	public function set($data) {
		$dr = $data->toArrayRecursive();

		$this->table('npcs_x_npc_groups')->delete(array('npc_group_id' => $dr['id']))->run();
		$this->setNpcs($dr['npcs'], $dr['id']);

		$update = $this->arrayFiller($data->toArrayRecursive());
		return $this->update($update)->where('id', $data->id)->run();
	}

	public function del($npcGroupId) {
		$this->table('npcs_x_npc_groups')->delete('npc_group_id', $npcGroupId)->run();
		return $this->delete('id', $npcGroupId)->run();
	}

	public function getById($npcGroupId) {
		$result = $this->select('*')->where('id', $npcGroupId)->order('title ASC')->run();
		
		if (count($result)) {
			$result[0]->npcs = $this->getNpcs($result[0]->id);
			return $result[0];
		}
		return array();
	}

	public function getByGame($gameId) {
		$result = $this->select('*')->where('game_id', $gameId)->order('title ASC')->run();
		
		if (count($result)) {
			foreach ($result as $k => $row) {
				$result[$k]->npcs = $this->getNpcs($row->id);
			}
			return $result;
		}
		return array();
	}

	public function gameHasNpcGroup($gameId, $npcId) {
		$result = $this->select('id')->where(array(
			'game_id' => $gameId, 
			'id'      => $npcId
		))->run();

		return count($result)? TRUE : FALSE;
	}
}
