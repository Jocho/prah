<?
namespace k6rpg\database;
class Characters extends \Blank\Database {
	public $id;
	public $name;
	public $description;
	public $goal;
	public $fear;
	public $notes;

	public $user_id;
	public $pow; // power
	public $dex; // dexterity
	public $knw; // knowledge
	public $san; // sanity
	public $vit; // vitality

	private $startingXp = 10;

	public $table = 'characters';

	public function addCharacter($data) {
		$erej = $data->toArrayRecursive();
		$data = $this->arrayFiller($erej);
		return $this->insert($data)->run();
	}

	public function saveCharacter($data) {
		$update = $this->arrayFiller($data->toArrayRecursive());
		return $this->update($update)->where('id', $data->id)->run();
	}

	public function del($id, $userId) {
		$r = $this->table('skills')->delete('character_id', $id)->run();
		if ($r)
			$r = $this->table('characters_x_items')->delete('character_id', $id)->run();
		if ($r)
			$r = $this->table('talents')->delete('character_id', $id)->run();
		if ($r)
			$r = $this->table('characters')->delete(array('id' => $id, 'user_id' => $userId))->run();
		return $r;
	}

	public function getByPlayer($userId) {
		return $this->select('*')->where('user_id', $userId)->run();
	}

	public function getLightByPlayer($userId) {
		return $this->table('characters as ch')->select('ch.id, ch.name, g.title')->join('left', 'games as g', 'g.id = ch.game_id')->where('ch.user_id', $userId)->run();
	}

	public function getByGame($gameId, $notMe = FALSE) {
		$where = array('game_id' => $gameId);
		if ($notMe !== FALSE)
			$where['id !='] = $notMe;

		$list       = $this->select('id, name, description, fear, goal')->where($where)->run();
		$characters = array();

		foreach ($list as $character)
			$characters[$character->id] = $character; //->name;
		return $characters;
	}

	public function getFullByGame($gameId, $notMe = FALSE) {
		$where = array('game_id' => $gameId);
		if ($notMe !== FALSE)
			$where['id !='] = $notMe;

		$list       = $this->select('*')->where($where)->run();
		$characters = array();

		foreach ($list as $character) {
			$character->bonuses = $this->calculateBonuses($character->id);
			$characters[$character->id] = $character; //->name;
		}
		return $characters;
	}

	public function getWithSpaceInInventory($gameId, $meId) {
		$where = array(
			'characters.id !='   => $meId,
			'characters.game_id' => $gameId,
		);
		return $this->select('id, name, COUNT(x.item_id) AS cnt')->join('left', 'characters_x_items AS x', 'x.character_id = characters.id')->where($where)->group('characters.id HAVING cnt < 10')->run('array');
	}

	private function calculateBonuses($charId) {
		$characterItems = $this->table('items AS i')
			->select('i.*')
			->join('left', 'characters_x_items as cxi', 'i.id = cxi.item_id')
			->where(array('character_id' => $charId, 'cxi.equipped' => '1'))
			// ->showQuery()
			->run();

		$bonuses = array('pow' => 0, 'dex' => 0, 'knw' => 0, 'san' => 0);
		foreach ($characterItems as $item) {
			$bonuses['pow'] += $item->pow;
			$bonuses['dex'] += $item->dex;
			$bonuses['knw'] += $item->knw;
			$bonuses['san'] += $item->san;
		}
		return $bonuses;
	}

	public function getExtendedByGame($gameId) {
		$where      = array('game_id' => $gameId);
		$list       = $this->select('*')->where($where)->run();
		$characters = array();
		
		foreach ($list as $character)
			$characters[$character->id] = $character; //->name;
		return $characters;
	}

	public function getFree() {
		$where = array('game_id' => 0);
		return $this->select('*')->where($where)->run();
	}

	public function getById($id) {
		return $character = $this->select('*')->where('id', $id)->run('object');
	}

	public function hasPlayerCharacter($userId, $characterId) {
		$result = $this->select('id')->where(array('user_id' => $userId, 'id' => $characterId))->run();
		return count($result) ? TRUE : FALSE;
	}

	protected function arrayFiller($data) {
		$vars = array('name', 'description', 'goal', 'fear', 'notes', 'xp', 'user_id', 'game_id');
		$res  = array();
		foreach ($vars as $var) {
			if (array_key_exists($var, $data))
				$res[$var] = $data[$var];
		}

		$attributes = array('pow', 'dex', 'knw', 'san', 'vit');
		foreach ($attributes as $k => $attribute) {
			$res[$attribute]          = $data['attributes'][$k]['value'];
			$res['max_' . $attribute] = $data['attributes'][$k]['max'];
		}
		return $res;
	}
}
?>